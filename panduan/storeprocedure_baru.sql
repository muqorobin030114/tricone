USE [DB_TRICONE]
GO
/****** Object:  StoredProcedure [dbo].[daftarkendaraanbytanggal]    Script Date: 25/01/2021 10.59.32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[daftarkendaraanbytanggal] 
	-- Add the parameters for the stored procedure here
	@startdate datetime,
	@enddate datetime,
	@id_kota int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	select kend.ID_KENDARAAN, jenis.NAMA_KENDARAAN,kend.JUMLAH_SEAT,kend.fasilitas,kend.pathFoto,harga.id_harga,
	COALESCE(harga.harga,0) hargakota,COALESCE(harga.durasi,0) durasi,
	kend.JUMLAH_KENDARAAN-COALESCE(SUM(pesan.quantity_kendaraan),0) terpakai
	from ms_kendaraan kend
	left join (
		select * from v_detail_pesan 
		where 
		(tgl_pergi >= @startdate and tgl_pergi <= @enddate) or 
		(tgl_pulang <= @enddate and tgl_pulang >= @startdate)
	)pesan on kend.id_kendaraan = pesan.id_kendaraan
	inner join ms_jenis_kendaraan jenis on jenis.ID_JENIS_KENDARAAN = kend.ID_JENIS_KENDARAAN
	left join TBL_HARGA harga on harga.ID_KENDARAAN=kend.ID_KENDARAAN and harga.ID_KOTA=@id_kota
	group by kend.ID_KENDARAAN, jenis.NAMA_KENDARAAN,kend.JUMLAH_SEAT,kend.fasilitas,kend.pathFoto,kend.JUMLAH_KENDARAAN,
	harga.HARGA,harga.durasi,harga.id_harga

    -- Insert statements for procedure here
END
GO
