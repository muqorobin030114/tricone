USE [DB_TRICONE]
GO
/****** Object:  View [dbo].[v_kota]    Script Date: 01/02/2021 00.40.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_kota] AS
SELECT k.ID_KOTA,k.NAMA_KOTA,k.ID_PROVINSI,p.NAMA_PROVINSI,k.CREATED_DATE as CREATED_DATE_KOTA,
K.MODIFIED_DATE as MODIFIED_DATE_KOTA,P.CREATED_DATE AS CREATED_DATE_PROV, P.MODIFIED_DATE AS MODIFIED_DATE_PROV
FROM MS_KOTA k
inner join MS_PROVINSI p on k.ID_PROVINSI=p.ID_PROVINSI
GO
/****** Object:  View [dbo].[v_transaksi]    Script Date: 01/02/2021 00.40.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[v_transaksi] as
select trans.ID_TRANSAKSI,trans.TOTAL_TRANSAKSI,promo.KODE_PROMO,promo.PERSEN,trans.ID_PROMO,trans.JUMLAH_PROMO,
trans.FOTO_PEMBAYARAN,trans.TOTAL_DURASI,trans.STATUS_UBAH_JADWAL,trans.APPROVAL,trans.KETERANGANUSER,trans.ID_REFUND,
refu.ID_BANK,refu.NOMOR_REKENING,refu.TOTAL_REFUND,refu.NAMA_REKENING,refu.STATUS as STATUS_REFUND,refu.FOTO_REFUND,
trans.ID_PEMBAYARAN,trans.STATUS as STATUS_TRANSAKSI,trans.KETERANGANADMIN
,trans.ID_MEMBER, member.NAMA_MEMBER,trans.TGL_PERGI,trans.TGL_PULANG,trans.ID_KOTA,kt.NAMA_KOTA,kt.NAMA_PROVINSI,trans.CATATAN,trans.CREATED_DATE,
trans.MODIFIED_DATE
from TBL_TRANSAKSI trans
left join TBL_PROMO promo on trans.ID_PROMO=promo.ID_PROMO
left join TBL_REFUND refu on trans.ID_REFUND=refu.ID_REFUND
inner join v_kota kt on trans.ID_KOTA=kt.ID_KOTA
inner join TBL_MEMBER member on trans.ID_MEMBER = member.ID_MEMBER
GO
/****** Object:  View [dbo].[v_refund]    Script Date: 01/02/2021 00.40.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[v_refund] as
select refu.ID_REFUND,refu.ID_TRANSAKSI,trans.TGL_PERGI,trans.ID_KOTA,trans.NAMA_KOTA,trans.NAMA_PROVINSI,trans.ID_MEMBER,
mem.NAMA_MEMBER,trans.TOTAL_TRANSAKSI,refu.FOTO_REFUND,refu.ID_BANK,bank.NAMA_BANK,refu.NOMOR_REKENING,refu.NAMA_REKENING,refu.TOTAL_REFUND,
refu.APPROVAL,refu.KETERANGANUSER,refu.KETERANGANADMIN,refu.STATUS,refu.CREATED_DATE,refu.MODIFIED_DATE
from TBL_REFUND refu
inner join v_transaksi trans on refu.ID_TRANSAKSI = trans.ID_TRANSAKSI
inner join TBL_MEMBER mem on trans.ID_MEMBER = mem.ID_MEMBER
inner join MS_BANK bank on refu.ID_BANK = bank.ID_BANK
GO
/****** Object:  View [dbo].[v_reschedule]    Script Date: 01/02/2021 00.40.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[v_reschedule] as
select trans.ID_TRANSAKSI,trans.TOTAL_TRANSAKSI,promo.KODE_PROMO,promo.PERSEN,trans.ID_PROMO,trans.JUMLAH_PROMO,
trans.FOTO_PEMBAYARAN,trans.TOTAL_DURASI,
(SELECT TOTAL_TRANSAKSI-TRANS.TOTAL_TRANSAKSI FROM TBL_TRANSAKSI WHERE STATUS_UBAH_JADWAL=trans.ID_TRANSAKSI) as SISA_BAYAR,
trans.STATUS_UBAH_JADWAL,trans.APPROVAL,trans.KETERANGANUSER,trans.ID_REFUND,
refu.ID_BANK,refu.NOMOR_REKENING,refu.TOTAL_REFUND,refu.NAMA_REKENING,
trans.ID_PEMBAYARAN,trans.STATUS as STATUS_TRANSAKSI,trans.KETERANGANADMIN
,trans.ID_MEMBER,mem.NAMA_MEMBER,trans.TGL_PERGI,trans.TGL_PULANG,trans.ID_KOTA,kt.NAMA_KOTA,kt.NAMA_PROVINSI,trans.CATATAN,trans.CREATED_DATE,
trans.MODIFIED_DATE
from TBL_TRANSAKSI trans
left join TBL_PROMO promo on trans.ID_PROMO=promo.ID_PROMO
left join TBL_REFUND refu on trans.ID_REFUND=refu.ID_BANK
inner join v_kota kt on trans.ID_KOTA=kt.ID_KOTA
inner join TBL_MEMBER mem on trans.ID_MEMBER = mem.ID_MEMBER
where trans.ID_TRANSAKSI in(select STATUS_UBAH_JADWAL from TBL_TRANSAKSI where STATUS_UBAH_JADWAL IS NOT NULL)
GO
/****** Object:  View [dbo].[v_Notreschedule]    Script Date: 01/02/2021 00.40.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[v_Notreschedule] as
select trans.ID_TRANSAKSI,trans.TOTAL_TRANSAKSI,promo.KODE_PROMO,promo.PERSEN,trans.ID_PROMO,trans.JUMLAH_PROMO,
trans.FOTO_PEMBAYARAN,trans.TOTAL_DURASI,
(SELECT TOTAL_TRANSAKSI-TRANS.TOTAL_TRANSAKSI FROM TBL_TRANSAKSI WHERE STATUS_UBAH_JADWAL=trans.ID_TRANSAKSI) as SISA_BAYAR,
trans.STATUS_UBAH_JADWAL,trans.APPROVAL,trans.KETERANGANUSER,trans.ID_REFUND,
refu.ID_BANK,refu.NOMOR_REKENING,refu.TOTAL_REFUND,refu.NAMA_REKENING,
trans.ID_PEMBAYARAN,trans.STATUS as STATUS_TRANSAKSI,trans.KETERANGANADMIN
,trans.ID_MEMBER,mem.NAMA_MEMBER,trans.TGL_PERGI,trans.TGL_PULANG,trans.ID_KOTA,kt.NAMA_KOTA,kt.NAMA_PROVINSI,trans.CATATAN,trans.CREATED_DATE,
trans.MODIFIED_DATE
from TBL_TRANSAKSI trans
left join TBL_PROMO promo on trans.ID_PROMO=promo.ID_PROMO
left join TBL_REFUND refu on trans.ID_REFUND=refu.ID_BANK
inner join v_kota kt on trans.ID_KOTA=kt.ID_KOTA
inner join TBL_MEMBER mem on trans.ID_MEMBER = mem.ID_MEMBER
where trans.ID_TRANSAKSI not in(select STATUS_UBAH_JADWAL from TBL_TRANSAKSI where STATUS_UBAH_JADWAL IS NOT NULL)
GO
/****** Object:  View [dbo].[v_detail_pesan]    Script Date: 01/02/2021 00.40.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[v_detail_pesan] as
select trans.id_transaksi,trans.APPROVAL,trans.STATUS as STATUS_TRANSAKSI,pesan.id_pemesanan, pesan.id_member, mmr.nama_member,mmr.no_telepon as telpon_member,mmr.email as email_member,
pesan.id_harga, harga.id_kendaraan ,
kend.id_jenis_kendaraan,jkend.nama_kendaraan,kend.jumlah_seat,kend.jumlah_kendaraan,kend.status as status_kendaraan,kend.pathFoto,
kend.fasilitas,
harga.durasi as durasi_jam,harga.harga,pesan.id_harilibur,pesan.HARGA_LIBUR,libur.nama_harilibur,libur.startdate as startdate_libur,
libur.enddate as enddate_libur,libur.persen_libur,
pesan.tgl_pergi,pesan.tgl_pulang, pesan.id_kota,kota.nama_kota,kota.id_provinsi,prov.nama_provinsi,pesan.quantity_kendaraan,
pesan.total_pembayaran,pesan.status as status_detail_pesan,pesan.id_supir,supir.nama_supir,supir.no_telepon as telpon_supir,
pesan.created_date as CREATED_DATE_PESAN,
pesan.modified_date as MODIFIED_DATE_PESAN
from TR_DETAIL_PEMESANAN pesan
inner join TBL_TRANSAKSI trans on trans.ID_TRANSAKSI = pesan.id_TRANSAKSI
inner join tbl_member mmr on mmr.id_member = pesan.id_member
inner join TBL_HARGA harga on pesan.id_harga = harga.id_harga and pesan.id_kota = harga.id_kota
inner join ms_kendaraan kend on kend.id_kendaraan = harga.id_kendaraan
inner join ms_jenis_kendaraan jkend on jkend.id_jenis_kendaraan = kend.id_jenis_kendaraan
left join ms_harilibur libur on libur.id_harilibur = pesan.id_harilibur
inner join ms_kota kota on kota.id_kota=pesan.id_kota
inner join ms_provinsi prov on prov.id_provinsi=kota.id_provinsi
left join ms_supir supir on supir.id_supir = pesan.id_supir
GO
/****** Object:  View [dbo].[v_kendaraan]    Script Date: 01/02/2021 00.40.36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[v_kendaraan] AS
SELECT k.ID_KENDARAAN, k.ID_JENIS_KENDARAAN, j.NAMA_KENDARAAN, k.JUMLAH_KENDARAAN, k.JUMLAH_SEAT, k.pathFoto,k.STATUS, k.DESKRIPSI, k.FASILITAS,
k.CREATED_DATE as CREATED_DATE_KENDARAAN, k.MODIFIED_DATE AS MODIFIED_DATE_KENDARAAAN,j.CREATED_DATE as CREATED_DATE_JENISK,
j.MODIFIED_DATE AS MODIFIED_DATE_JENISK
FROM MS_JENIS_KENDARAAN j
inner join MS_KENDARAAN k on j.ID_JENIS_KENDARAAN=k.ID_JENIS_KENDARAAN
GO
