﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Tricone.Models
{
    [MetadataType(typeof(SupirMetadata))]
    public partial class MS_SUPIR
    {

    }

    public class SupirMetadata
    {
        [Display(Name = "Driver ID")]
        public int ID_SUPIR { get; set; }

        [Display(Name = "Driver Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Name Required")]
        public string NAMA_SUPIR { get; set; }

        [Display(Name = "Address")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Address Required")]
        public string ALAMAT { get; set; }

        [Display(Name = "Telephone")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Telephone Required")]
        public string NO_TELEPON { get; set; }
    }
}