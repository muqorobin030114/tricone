﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Tricone.Models
{
    public class kontak
    {
        [Display(Name = "Email From")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Email Required")]
        public string From { get; set; }

        [Display(Name = "Email To")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Email Required")]
        public string To { get; set; }

        [Display(Name = "Subject")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Subject Required")]
        public string Subject { get; set; }

        [Display(Name = "Message")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Message Required")]
        public string Message { get; set; }
    }
}