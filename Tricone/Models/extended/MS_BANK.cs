﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Tricone.Models
{
    [MetadataType(typeof(bankMetadata))]
    public partial class MS_BANK
    {
        //public string CONFIRMPASSWORD { get; set; }
    }

    public class bankMetadata
    {
        /*[Display(Name = "ID")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "ID Required")]
        public int ID_BANK { get; set; }*/

        [Display(Name = "Bank Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Name Required")]
        public string NAMA_BANK { get; set; }
    }
}