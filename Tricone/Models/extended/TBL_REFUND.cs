﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Tricone.Models
{
    [MetadataType(typeof(refundMetadata))]
    public partial class TBL_REFUND
    {
        [NotMapped]
        public List<v_transaksi> transcollection{ get; set; }
        
        [NotMapped]
        public List<MS_BANK> bankcollection { get; set; }
    }

    public class refundMetadata
    {
        [Display(Name = "Your Transaction")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Your Transaction Required")]
        public int ID_TRANSAKSI { get; set; }

        [Required(ErrorMessage = "Bank Required")]
        [Display(Name = "Bank Name")]
        public int ID_BANK { get; set; }

        [Display(Name = "Account Number")]
        [Required(ErrorMessage = "Account Number Required")]
        [MaxLength(12)]
        [MinLength(1)]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Account Number must be numeric")]
        public string NOMOR_REKENING { get; set; }

        [Display(Name = "Reason")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Reason Required")]
        public string KETERANGANUSER { get; set; }
        
        [Required(ErrorMessage = "Account Name is required.")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Use Alphabetic only please")]
        public string NAMA_REKENING { get; set; }

        [Display(Name = "Reason Pict")]
        public string FOTO_REFUND { get; set; }
    }
}