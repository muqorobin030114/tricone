﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Tricone.Models
{

    [MetadataType(typeof(hariliburMetadata))]
    public partial class MS_HARILIBUR
    {
        //public string CONFIRMPASSWORD { get; set; }
    }

    public class hariliburMetadata
    {
        /*[Display(Name = "ID")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "ID Required")]
        public int ID_HARILIBUR { get; set; }*/
        
        [Display(Name = "Day Off Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Name Required")]
        public string NAMA_HARILIBUR { get; set; }
        
        [Display(Name = "Start Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Date Required")]
        public DateTime STARTDATE { get; set; }
        
        [Display(Name = "End Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Date Required")]
        public DateTime ENDDATE { get; set; }
        
        [Display(Name = "Percent(%)")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Name Required")]
        public int PERSEN_LIBUR { get; set; }
    }
}