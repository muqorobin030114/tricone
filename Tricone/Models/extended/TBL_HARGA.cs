﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Tricone.Models
{
    [MetadataType(typeof(hargaMetadata))]
    public partial class TBL_HARGA
    {
        [NotMapped]
        public List<v_kota> kotacollection { get; set; }
        [NotMapped]
        public List<v_kendaraan> kendcollection { get; set; }
    }

    public class hargaMetadata
    {
        [Display(Name = "Id Harga")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Id Required")]
        public int ID_HARGA { get; set; }

        [Display(Name = "Vehicle Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Vehicle Name Required")]
        public int ID_KENDARAAN { get; set; }

        [Display(Name = "City Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "City Name Required")]
        public int ID_KOTA { get; set; }

        [Display(Name = "Duration")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Name Required")]
        public int DURASI { get; set; }

        [Display(Name = "Price")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Name Required")]
        public int HARGA { get; set; }
    }
}