﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Tricone.Models
{
    [MetadataType(typeof(bankMetadata))]
    public partial class TestimoniAdmin
    {
        //public string CONFIRMPASSWORD { get; set; }
    }

    public class testimoniMetadata
    {
        [Display(Name = "ID")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "ID Required")]
        public int ID_TESTIMONI { get; set; }

        [Display(Name = "Email From")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Email Required")]
        public string EMAIL_FROM { get; set; }

        [Display(Name = "Email To")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Email Required")]
        public string EMAIL_TO { get; set; }

        [Display(Name = "Subject")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Subject Required")]
        public string Subject { get; set; }

        [Display(Name = "Message")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Message Required")]
        public string MESSAGE { get; set; }

        [Display(Name = "Subject")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Subject Required")]
        public string SUBJECT { get; set; }

        [Display(Name = "Status")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Status Required")]
        public Nullable<int> STATUS { get; set; }
    }
}