﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Tricone.Models
{
    [MetadataType(typeof(promoMetadata))]
    public partial class TBL_PROMO
    {
        //public string CONFIRMPASSWORD { get; set; }
    }

    public class promoMetadata
    {
        /*[Display(Name = "ID")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "ID Required")]
        public int ID_PROMO { get; set; }*/

        [Display(Name = "Discount Code")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Code Required")]
        public string KODE_PROMO { get; set; }

        [Display(Name = "Percent%")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Number Required")]
        public int PERSEN { get; set; }
    }
}