﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace Tricone.Models
{
    [MetadataType(typeof(wisataMetadata))]
    public partial class TBL_WISATA
    {
        [NotMapped]
        public List<MS_KOTA> kotacollection { get; set; }
        [NotMapped]
        public List<MS_PROVINSI> provcollection { get; set; }
    }

    public class wisataMetadata
    {
        [Display(Name = "Id Wisata")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Id Wisata")]
        public int ID_WISATA { get; set; }

        [Display(Name = "Name of Tourist Destination")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Name Required")]
        public string NAMA_WISATA { get; set; }

        [Display(Name = "Description")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Description Required")]
        public string DESKRIPSI { get; set; }

        [Display(Name = "Image")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Image Required")]
        public string FOTO_DERSKRIPSI { get; set; }

        [Display(Name = "Province Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Province Name Required")]
        public Nullable<int> ID_PROVINSI { get; set; }

        [Display(Name = "City Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "City Name Required")]
        public Nullable<int> ID_KOTA { get; set; }
    }
}