﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Tricone.Models
{

    [MetadataType(typeof(pembayaranMetadata))]
    public partial class TBL_PEMBAYARAN
    {
        [NotMapped]
        public List<MS_BANK> bankcollection { get; set; }
    }

    public class pembayaranMetadata
    {
        [Display(Name = "Id Bank")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Bank Required")]
        public Nullable<int> ID_BANK { get; set; }

        [Required(ErrorMessage = "Account Name is required.")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Use Alphabetic only please")]
        public string NAMA_REKENING { get; set; }

        [Display(Name = "Paid Amount")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Paid Amount Required")]
        public Nullable<int> JUMLAH_BAYAR { get; set; }

        [Display(Name = "Account Number")]
        [Required(ErrorMessage = "Account Number Required")]
        [MaxLength(12)]
        [MinLength(1)]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Account Number must be numeric")]
        public string NOMOR_REKENING { get; set; }
    }
}