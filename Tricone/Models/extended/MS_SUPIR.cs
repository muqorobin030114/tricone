﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Tricone.Models
{
    [MetadataType(typeof(supirMetadata))]
    public partial class MS_SUPIR
    {
       
    }

    public class supirMetadata
    {
        /*[Display(Name = "ID")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Name Required")]
        public string ID_SUPIR { get; set; }*/

        [Display(Name = "Driver Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Name Required")]
        public string NAMA_SUPIR { get; set; }

        [Display(Name = "Address")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Address Required")]
        public string ALAMAT { get; set; }

        [Display(Name = "Number Phone")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Number Phone Required")]
        [MinLength(11, ErrorMessage = "Minimum 11 characters Required")]
        [MaxLength(13, ErrorMessage = "Maximum 13 characters Required")]
        public string NO_TELEPON { get; set; }
    }
}