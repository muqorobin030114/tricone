﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tricone.Models
{
    public class JoinKendaraanClass
    {
        public MS_JENIS_KENDARAAN jkendaraanList { get; set; }
        public MS_KENDARAAN kendaraanList { get; set; }
    }
}