﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Tricone.Models
{

    [MetadataType(typeof(kendaraanMetadata))]
    public partial class MS_KENDARAAN
    {
        [NotMapped]
        public List<MS_JENIS_KENDARAAN> kendcollection { get; set; }
    }

    public class kendaraanMetadata
    {
        [Display(Name = "Vehicle Type")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Vehicle Type Required")]
        public int ID_JENIS_KENDARAAN { get; set; }

        [Display(Name = "Total Seat")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Total Seat Required")]
        public int JUMLAH_SEAT { get; set; }

        [Display(Name = "Total Vehicle")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Total Vehicle Required")]
        public int JUMLAH_KENDARAAN { get; set; }

        /*[Display(Name = "Image")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Image Required")]
        public string pathFoto { get; set; }*/

        [Display(Name = "Status")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Status Required")]
        public int STATUS { get; set; }

        [AllowHtml]
        [Display(Name = "Description")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Description Required")]
        public string DESKRIPSI { get; set; }
        [AllowHtml]
        [Display(Name = "Facility")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Facility Required")]
        public string FASILITAS { get; set; }
    }
}