﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Tricone.Models
{
    [MetadataType(typeof(provinsiMetadata))]
    public partial class MS_PROVINSI
    {
        //public string CONFIRMPASSWORD { get; set; }
    }

    public class provinsiMetadata
    {
        /*[Display(Name = "ID")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "ID Required")]
        public int ID_PROVINSI { get; set; }*/

        [Display(Name = "Province Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Name Required")]
        public string NAMA_PROVINSI { get; set; }
    }
}