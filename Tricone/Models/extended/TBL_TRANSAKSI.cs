﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Tricone.Models
{
    [MetadataType(typeof(transMetadata))]
    public partial class TBL_TRANSAKSI
    {
        [NotMapped]
        public List<v_transaksi> transcollection { get; set; }
    }

    public class transMetadata
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Transaction Required")]
        public int ID_TRANSAKSI { get; set; }
    }
}