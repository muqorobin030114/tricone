﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Tricone.Models
{

    [MetadataType(typeof(jeniskendaraanMetadata))]
    public partial class MS_JENIS_KENDARAAN
    {
        //public string CONFIRMPASSWORD { get; set; }
    }

    public class jeniskendaraanMetadata
    {
        /*[Display(Name = "ID")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Name Required")]
        public int ID_JENIS_KENDARAAN { get; set; }*/

        [Display(Name = "Vehicle Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Name Required")]
        public string NAMA_KENDARAAN { get; set; }
    }
}