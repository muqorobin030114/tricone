﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Tricone.Models
{
    [MetadataType(typeof(pesanMetadata))]
    public partial class TR_DETAIL_PEMESANAN
    {
        [NotMapped]
        public List<MS_KOTA> kotacollection { get; set; }
        [NotMapped]
        public List<MS_SUPIR> supircollection { get; set; }
        [NotMapped]
        public List<TBL_HARGA> hargacollection { get; set; }
    }

    public class pesanMetadata
    {
        public Nullable<int> ID_TRANSAKSI { get; set; }

        public Nullable<int> ID_KOTA { get; set; }

        [Display(Name = "Booking Id")]
        public int ID_PEMESANAN { get; set; }

        [Display(Name = "Member Id")]
        public int ID_MEMBER { get; set; }

        [Display(Name = "Price Id")]
        public int ID_HARGA { get; set; }

        [Display(Name = "Day Off Id")]
        public int ID_HARILIBUR { get; set; }

        [Display(Name = "Start Date")]
        public int TGL_PERGI { get; set; }

        [Display(Name = "End Date")]
        public int TGL_PULANG { get; set; }

        [Display(Name = "Vehicle Quantity")]
        public int QUANTITY_KENDARAAN { get; set; }

        [Display(Name = "Total Payment")]
        public int TOTAL_PEMBAYARAN { get; set; }

        [Display(Name = "Status")]
        public int STATUS { get; set; }

        [Display(Name = "Driver Id")]
        public int ID_SUPIR { get; set; }
    }
}