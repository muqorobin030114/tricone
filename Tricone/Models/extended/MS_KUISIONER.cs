﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Tricone.Models
{
    [MetadataType(typeof(kuisionerMetadata))]
    public partial class MS_KUISIONER
    {
        //public string CONFIRMPASSWORD { get; set; }
    }

    public class kuisionerMetadata
    {
        /*[Display(Name = "ID")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "ID Required")]
        public int ID_KUISIONER { get; set; }*/

        [Display(Name = "Kuisioner")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Kuisioner Required")]
        public string KUISIONER { get; set; }
    }
}