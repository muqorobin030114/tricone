﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Tricone.Models
{
    public partial class User
    {

    }

    public class UserMetadata2
    {
        [Display(Name ="Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage ="Name Required")]
        public string NAMA_MEMBER { get; set; }

        [Display(Name = "Address")]

        [Required(AllowEmptyStrings = false, ErrorMessage = "Address Required")]
        public string ALAMAT { get; set; }

        [Display(Name = "Telephone")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Number Phone Required")]
        [DataType(DataType.PhoneNumber)]
        [MinLength(11, ErrorMessage = "Minimum 11 characters Required")]
        [MaxLength(13, ErrorMessage = "Maximum 13 characters Required")]
        public string NO_TELEPON { get; set; }

        [Display(Name = "Email")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Email Required")]
        [DataType(DataType.EmailAddress)]
        public string EMAIL { get; set; }

        [Display(Name = "Username")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Username Required")]
        public string USERNAME { get; set; }
        
        [Required(AllowEmptyStrings = false, ErrorMessage = "Password Required")]
        [DataType(DataType.Password)]
        [MinLength(6,ErrorMessage ="Minimum 6 characters Required")]
        public string PASSWORD { get; set; }

        [Display(Name = "Confirm Password")]
        [DataType(DataType.Password)]
        [Compare("PASSWORD",ErrorMessage ="Confirm password and password do not match")]
        public string CONFIRMPASSWORD { get; set; }
    }
}