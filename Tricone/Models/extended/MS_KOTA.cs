﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Tricone.Models
{
    [MetadataType(typeof(kotaMetadata))]
    public partial class MS_KOTA
    {
        [NotMapped]
        public List<MS_PROVINSI> provcollection { get; set; }
        //public string CONFIRMPASSWORD { get; set; }
    }

    public class kotaMetadata
    {
        /*[Display(Name = "ID")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Name Required")]
        public int ID_KOTA { get; set; }*/

        [Display(Name = "City Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Name Required")]
        public string NAMA_KOTA { get; set; }

        
        [Display(Name = "ID PROVINCE")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Province Required")]
        public int ID_PROVINSI { get; set; }
    }
}