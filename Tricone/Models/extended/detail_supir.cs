﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Tricone.Models
{
    [MetadataType(typeof(detailsupirMetadata))]
    public partial class detail_supir
    {
        [NotMapped]
        public List<v_detail_pesan> detailcollection { get; set; }

        [NotMapped]
        public List<MS_SUPIR> supircollection { get; set; }
    }

    public class detailsupirMetadata
    {

    }
}