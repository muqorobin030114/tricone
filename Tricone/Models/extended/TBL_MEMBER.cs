﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Tricone.Models
{
    [MetadataType(typeof(UserMetadata))]
    public partial class TBL_MEMBER
    {
        public string CONFIRMPASSWORD { get; set; }
    }

    public class UserMetadata
    {
        [Display(Name = "Id")]
        public int ID_MEMBER { get; set; }

        [Display(Name = "Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Name Required")]
        public string NAMA_MEMBER { get; set; }

        [Display(Name = "Address")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Address Required")]
        public string ALAMAT { get; set; }

        [Display(Name = "Telephone")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Telephone Required")]
        [MinLength(11, ErrorMessage = "Minimum 11 characters Required")]
        public string NO_TELEPON { get; set; }

        [Display(Name = "Email")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Email Required")]
        [DataType(DataType.EmailAddress)]
        public string EMAIL { get; set; }

        [Display(Name = "Username")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Username Required")]
        public string USERNAME { get; set; }

        [Display(Name = "Password")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Password is Required")]
        [DataType(DataType.Password)]
        [MinLength(6, ErrorMessage = "Minimum 6 characters Required")]
        public string PASSWORD { get; set; }

        [Display(Name = "Confirm Password")]
        [DataType(DataType.Password)]
        [Compare("PASSWORD", ErrorMessage = "Confirm password and password do not match")]
        public string CONFIRMPASSWORD { get; set; }

        [Display(Name = "Is Email Verified")]
        public int ISEMAILVERIFIED { get; set; }

        [Display(Name = "Role")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "Role Required")]
        public int ROLEMEMBER { get; set; }

        [Display(Name = "Status")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "Status Required")]
        public int STATUS { get; set; }
    }
}