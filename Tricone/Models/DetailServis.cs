﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Tricone.Models
{

    [MetadataType(typeof(servisMetadata))]
    public partial class DetailServis
    {
    }

    public class servisMetadata
    {
        //[Display(Name = "Id Detail Servis")]
        //[Required(AllowEmptyStrings = false, ErrorMessage = "Id Detail Servis")]
        //public int ID_DETAILSERVIS { get; set; }

        [Display(Name = "Service Name")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Service Name Required")]
        public string NAMA_SERVIS { get; set; }

        [Display(Name = "Description")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Description Required")]
        public string DESKRIPSI { get; set; }

        [Display(Name = "Image")]
        [Required(AllowEmptyStrings = false, ErrorMessage = "Image Required")]
        public string FOTO_DESKRIPSI { get; set; }
    }
}