﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Tricone.Models.Kendaraan
{
    [MetadataType(typeof(CascadingKendaraan))]
    public partial class MS_JENIS_KENDARAAN
    {

    }

    public class CascadingKendaraan
    {
        public int ID_KENDARAAN { get; set;}
        public int ID_JENIS_KENDARAAN { get; set;}
        
    }
}