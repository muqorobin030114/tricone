//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Tricone.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class TBL_PEMBAYARAN
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TBL_PEMBAYARAN()
        {
            this.TBL_TRANSAKSI = new HashSet<TBL_TRANSAKSI>();
        }
    
        public int ID_PEMBAYARAN { get; set; }
        public Nullable<int> ID_BANK { get; set; }
        public string NAMA_REKENING { get; set; }
        public Nullable<int> JUMLAH_BAYAR { get; set; }
        public string NOMOR_REKENING { get; set; }
        public Nullable<System.DateTime> CREATED_DATE { get; set; }
        public Nullable<System.DateTime> MODIFIED_DATE { get; set; }
        public Nullable<int> ID_TRANSAKSI { get; set; }
    
        public virtual MS_BANK MS_BANK { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TBL_TRANSAKSI> TBL_TRANSAKSI { get; set; }
        public virtual TBL_TRANSAKSI TBL_TRANSAKSI1 { get; set; }
    }
}
