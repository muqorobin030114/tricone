﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tricone.Models
{
    public class Resultdetail
    {
        public Nullable<int> ID_TRANSAKSI { get; set; }
        public int ID_PEMESANAN { get; set; }
        public Nullable<int> ID_KENDARAAN { get; set; }
        public string pathFoto { get; set; }
        public string NAMA_KENDARAAN { get; set; }
        public string fasilitas { get; set; }
        public Nullable<int> HARGA { get; set; }
        public string CATATAN { get; set; }
        public Nullable<int> QUANTITY_KENDARAAN { get; set; }
        public Nullable<int> TOTAL_PEMBAYARAN { get; set; }
        public Nullable<int> ID_PROMO { get; set; }
        public Nullable<int> JUMLAH_PROMO { get; set; }
    }

    public class transaksiMemberOutput
    {
        public int ID_TRANSAKSI { get; set; }
        public Nullable<System.DateTime> TGL_PERGI { get; set; }
        public Nullable<System.DateTime> TGL_PULANG { get; set; }
        public string NAMA_KOTA { get; set; }
        public string FOTO_PEMBAYARAN { get; set; }
        public Nullable<int> STATUS { get; set; }
        public Nullable<int> APPROVAL { get; set; }
        public Nullable<int> SISA_BAYAR { get; set; }
        public string KETERANGANADMIN { get; set; }
    }

    public class refundOutput
    {
        public int ID_REFUND { get; set; }
        public int ID_TRANSAKSI { get; set; }
        public Nullable<System.DateTime> TGL_PERGI { get; set; }
        public string NAMA_KOTA { get; set; }
        public string FOTO_REFUND { get; set; }
        public Nullable<int> STATUS { get; set; }
        public Nullable<int> APPROVAL { get; set; }
    }

    public class refundadminOutput
    {
        public int ID_REFUND { get; set; }
        public Nullable<System.DateTime> TGL_PERGI { get; set; }
        public string NAMA_KOTA { get; set; }
        public string NAMA_MEMBER { get; set; }
        public string FOTO_REFUND { get; set; }
        public string NOMOR_REKENING { get; set; }
        public string NAMA_REKENING { get; set; }
        public Nullable<int> TOTAL_REFUND { get; set; }
        public Nullable<int> STATUS { get; set; }
        public string KETERANGANUSER { get; set; }
        public Nullable<int> APPROVAL { get; set; }
    }

    public class transaksiOutput
    {
        public int ID_TRANSAKSI { get; set; }
        public Nullable<int> TOTAL_TRANSAKSI { get; set; }
        public Nullable<int> ID_PROMO { get; set; }
        public string KODE_PROMO { get; set; }
        public string FOTO_PEMBAYARAN { get; set; }
        public Nullable<int> TOTAL_DURASI { get; set; }
        public Nullable<int> STATUS_UBAH_JADWAL { get; set; }
        public Nullable<int> APPROVAL { get; set; }
        public string KETERANGANUSER { get; set; }
        public Nullable<int> ID_REFUND { get; set; }
        public Nullable<int> ID_PEMBAYARAN { get; set; }
        public Nullable<int> STATUS { get; set; }
        public string KETERANGANADMIN { get; set; }
        public Nullable<int> ID_MEMBER { get; set; }
        public string NAMA_MEMBER { get; set; }
        public Nullable<System.DateTime> TGL_PERGI { get; set; }
        public Nullable<System.DateTime> TGL_PULANG { get; set; }
        public Nullable<int> SISA_BAYAR { get; set; }
    }

    public class detailtransaksiOutput
    {
        public Nullable<int> ID_TRANSAKSI { get; set; }
        public int id_pemesanan { get; set; }
        public Nullable<int> id_member { get; set; }
        public string nama_member { get; set; }
        public string telpon_member { get; set; }
        public string email_member { get; set; }
        public Nullable<int> id_harga { get; set; }
        public Nullable<int> id_kendaraan { get; set; }
        public Nullable<int> id_jenis_kendaraan { get; set; }
        public string nama_kendaraan { get; set; }
        public Nullable<int> jumlah_seat { get; set; }
        public Nullable<int> jumlah_kendaraan { get; set; }
        public Nullable<int> status_kendaraan { get; set; }
        public Nullable<int> durasi_jam { get; set; }
        public Nullable<int> harga { get; set; }
        public Nullable<int> id_harilibur { get; set; }
        public string nama_harilibur { get; set; }
        public Nullable<System.DateTime> startdate_libur { get; set; }
        public Nullable<System.DateTime> enddate_libur { get; set; }
        public Nullable<int> persen_libur { get; set; }
        public Nullable<System.DateTime> tgl_pergi { get; set; }
        public Nullable<System.DateTime> tgl_pulang { get; set; }
        public Nullable<int> id_kota { get; set; }
        public string nama_kota { get; set; }
        public Nullable<int> id_provinsi { get; set; }
        public string nama_provinsi { get; set; }
        public Nullable<int> quantity_kendaraan { get; set; }
        public Nullable<int> total_pembayaran { get; set; }
        public Nullable<int> status_detail_pesan { get; set; }
        public Nullable<int> id_supir { get; set; }
        public string nama_supir { get; set; }
        public string telpon_supir { get; set; }
        public Nullable<int> APPROVAL { get; set; }
        public Nullable<int> STATUS_TRANSAKSI { get; set; }
    }

    public class testimoni 
    {
        public int ID_TESTIMONI { get; set; }
        public string EMAIL_FROM { get; set; }
        public string NAMA { get; set; }
        public string EMAIL_TO { get; set; }
        public string SUBJECT { get; set; }
        public string MESSAGE { get; set; }
        public Nullable<int> STATUS { get; set; }
    }

    public class auditOutput
    {
        public int ID_AUDIT { get; set; }
        public string NAMA_TABEL { get; set; }
        public string AKSI { get; set; }
        public string NAMA_FIELD { get; set; }
        public string VALUE_LAMA { get; set; }
        public string VALUE_BARU { get; set; }
        public string userid { get; set; }
        public Nullable<System.DateTime> CREATED_DATE { get; set; }
    }

    public class errorOutput
    {
        public int ID_LOG { get; set; }
        public string ID_USER { get; set; }
        public Nullable<System.DateTime> EROR_TIME { get; set; }
        public string pathfileerror { get; set; }
        public string EROR_LINE { get; set; }
        public string EROR_DESCRIPTION { get; set; }
    }
}