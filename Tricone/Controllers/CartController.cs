﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class CartController : Controller
    {
        DB_TRICONEEntitie dc = new DB_TRICONEEntitie();
        // GET: Cart
        public ActionResult Index()
        {
            int id = Convert.ToInt32(Session["ID_MEMBER"]);
            var qtrx = (from trans in dc.TBL_TRANSAKSI
                        join kota in dc.v_kota on trans.ID_KOTA equals kota.ID_KOTA
                        where trans.ID_MEMBER == id && trans.APPROVAL==0
                        select new transaksi
                        {
                            TGL_PERGI = trans.TGL_PERGI,
                            TGL_PULANG = trans.TGL_PULANG,
                            NAMA_KOTA = kota.NAMA_KOTA,
                            NAMA_PROVINSI = kota.NAMA_PROVINSI,
                            TOTAL_DURASI = trans.TOTAL_DURASI,
                            TOTAL_TRANSAKSI = trans.TOTAL_TRANSAKSI,
                            ID_TRANSAKSI = trans.ID_TRANSAKSI,
                        }).ToList();
            return View(qtrx);
            //return View(dc.TR_DETAIL_PEMESANAN.ToList());
            //return View();
        }

        [HttpGet]
        public ActionResult Cart(int id)
        {
            try
            {
                var id_member = Convert.ToInt32(Session["ID_MEMBER"]);
                //var tampilkan = dc.v_detail_pesan.Where(a => a.id_transaksi == id && a.id_member== id_member).FirstOrDefault();
                var qtrx = (from dtl in dc.v_detail_pesan
                            where dtl.id_transaksi == id && dtl.id_member == id_member
                            group dtl by new { dtl.id_transaksi, dtl.id_kendaraan, dtl.pathFoto, dtl.nama_kendaraan, dtl.fasilitas, dtl.harga, dtl.id_pemesanan } into grp
                            select new Resultdetail
                            {
                                ID_TRANSAKSI = grp.Key.id_transaksi,
                                ID_KENDARAAN = grp.Key.id_kendaraan,
                                pathFoto = grp.Key.pathFoto,
                                NAMA_KENDARAAN = grp.Key.nama_kendaraan,
                                fasilitas = grp.Key.fasilitas,
                                HARGA = grp.Key.harga,
                                ID_PEMESANAN = grp.Key.id_pemesanan,
                                QUANTITY_KENDARAAN = grp.Sum(t => t.quantity_kendaraan)
                            }).ToList();

                //List<v_detail_pesan> listpesan = dc.v_detail_pesan.Where(a => a.id_transaksi == id && a.id_member == id_member).ToList();
                ViewData["listpsn"] = qtrx;
                ViewBag.Catatan = dc.TBL_TRANSAKSI.Where(a => a.ID_TRANSAKSI == id).Select(uf => uf.CATATAN).FirstOrDefault();
                ViewBag.Total = dc.TBL_TRANSAKSI.Where(a => a.ID_TRANSAKSI == id).Select(uf => uf.TOTAL_TRANSAKSI).FirstOrDefault();

                //total durasi
                TBL_TRANSAKSI trx = dc.TBL_TRANSAKSI.Where(x => x.ID_TRANSAKSI == id).FirstOrDefault();
                DateTime dt1 = DateTime.Parse(Convert.ToDateTime(trx.TGL_PULANG).AddDays(35).ToString());
                DateTime dt2 = DateTime.Parse(Convert.ToDateTime(trx.TGL_PERGI).AddDays(35).ToString());
                TimeSpan ts = dt1.Subtract(dt2);
                var duras = ts.Days + 1;
                ViewBag.Durasi = duras;

                //sub total durasi
                var jumlahtotal = dc.TR_DETAIL_PEMESANAN
                                .Join(dc.TBL_HARGA,
                                  post => post.ID_HARGA,
                                  meta => meta.ID_HARGA,
                                  (post, meta) => new { Post = post, Meta = meta })
                                .Where(uf => uf.Post.ID_TRANSAKSI == id)
                                .Select(uf => uf.Post.QUANTITY_KENDARAAN * uf.Meta.HARGA)
                                //.DefaultIfEmpty()
                                .Sum();
                ViewBag.subtot = jumlahtotal;
                ViewBag.id_trans = id;
                ViewBag.Discountnya = trx.JUMLAH_PROMO;

                //total fee holiday
                var feeholiday = dc.TR_DETAIL_PEMESANAN
                                .Where(uf => uf.ID_TRANSAKSI == id)
                                .Select(uf => uf.HARGA_LIBUR)
                                .Sum();
                ViewBag.feeholiday = feeholiday;

            List<TBL_PROMO> promolist = dc.TBL_PROMO.ToList();
            ViewData["promolist"] = promolist;
            //cari kode
            TBL_PROMO prms = dc.TBL_PROMO.Where(x => x.ID_PROMO == trx.ID_PROMO).FirstOrDefault();
            var countpromo = dc.TBL_PROMO.Where(a => a.ID_PROMO == trx.ID_PROMO).Count();
            if (countpromo > 0)
            {
                ViewBag.kdpromo = prms.KODE_PROMO;
            }
            else
            {
                ViewBag.kdpromo = "";
            }

                return View(qtrx);
            }
            catch (Exception msgErr)
            {
                //error log
                DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                if (Session["ID_MEMBER"].ToString() != null)
                {
                    logerror.ID_USER = Session["ID_MEMBER"].ToString();
                }
                logerror.EROR_TIME = DateTime.Now;
                logerror.EROR_DESCRIPTION = msgErr.Message;
                logerror.CREATED_DATE = DateTime.Now;
                logerror.MODIFIED_DATE = DateTime.Now;
                logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                dc2.TBL_EROR_LOG.Add(logerror);
                dc2.SaveChanges();
                ViewBag.Error = msgErr.Message;
            }
            return null;
        }

        [HttpPost]
        public ActionResult postCart(List<Resultdetail> model,FormCollection pesan)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    var id_member = Convert.ToInt32(Session["ID_MEMBER"]);
                    int id_transaksi = 0;
                    int qty_kendaraan = 0;
                    int id_kendaraan = 0;
                    int id_pemesanan = 0;
                    string catatan = pesan["message"];
                    foreach (var aa in model)
                    {
                        id_transaksi = Convert.ToInt32(aa.ID_TRANSAKSI);
                    }
                    TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                    var oke = dc.TBL_TRANSAKSI.AsNoTracking().Where(x => x.ID_TRANSAKSI == id_transaksi).FirstOrDefault();
                    var alas = "";
                    id_transaksi = 0;
                    foreach (var cba in model)
                    {
                        id_transaksi = Convert.ToInt32(cba.ID_TRANSAKSI);
                        id_pemesanan = Convert.ToInt32(cba.ID_PEMESANAN);
                        id_kendaraan = Convert.ToInt32(cba.ID_KENDARAAN);
                        qty_kendaraan = Convert.ToInt32(cba.QUANTITY_KENDARAAN);

                        var qtrx = (from dtl in dc.v_detail_pesan
                                    join trs in dc.TBL_TRANSAKSI on dtl.id_transaksi equals trs.ID_TRANSAKSI
                                    where dtl.id_transaksi == id_transaksi && dtl.id_member == id_member && dtl.id_kendaraan == id_kendaraan && dtl.id_pemesanan == id_pemesanan
                                    group dtl by new { dtl.id_transaksi, dtl.id_kendaraan, dtl.pathFoto, dtl.nama_kendaraan, dtl.fasilitas, dtl.harga, dtl.id_pemesanan, trs.CATATAN } into grp
                                    select new Resultdetail
                                    {
                                        ID_TRANSAKSI = grp.Key.id_transaksi,
                                        ID_KENDARAAN = grp.Key.id_kendaraan,
                                        pathFoto = grp.Key.pathFoto,
                                        NAMA_KENDARAAN = grp.Key.nama_kendaraan,
                                        fasilitas = grp.Key.fasilitas,
                                        HARGA = grp.Key.harga,
                                        ID_PEMESANAN = grp.Key.id_pemesanan,
                                        CATATAN = grp.Key.CATATAN,
                                        QUANTITY_KENDARAAN = grp.Sum(t => t.quantity_kendaraan)
                                    }).FirstOrDefault();

                        //apabila angka database kurang dari cart. artinya kendaraan ditambah
                        if (Convert.ToInt32(qtrx.QUANTITY_KENDARAAN) < qty_kendaraan)
                        {
                            audit.AKSI = "INCREASE CAR";
                            int jumlah = qty_kendaraan - Convert.ToInt32(qtrx.QUANTITY_KENDARAAN);
                            var tambahrec = dc.v_detail_pesan.Where(a => a.id_transaksi == id_transaksi && a.id_kendaraan == id_kendaraan && a.id_pemesanan == id_pemesanan).FirstOrDefault();

                            TR_DETAIL_PEMESANAN tambahmobil = dc.TR_DETAIL_PEMESANAN.Where(x => x.ID_PEMESANAN == tambahrec.id_pemesanan).FirstOrDefault();

                            //total durasi
                            DateTime dt1 = DateTime.Parse(Convert.ToDateTime(tambahrec.tgl_pulang).AddDays(35).ToString());
                            DateTime dt2 = DateTime.Parse(Convert.ToDateTime(tambahrec.tgl_pergi).AddDays(35).ToString());
                            TimeSpan ts = dt1.Subtract(dt2);
                            int jumlah_durasi = ts.Days + 1;

                            //kalkulasi harilibur
                            var harganya = dc.TBL_HARGA.Where(x => x.ID_HARGA == tambahrec.id_harga).FirstOrDefault();
                            DateTime datepergi = Convert.ToDateTime(tambahrec.tgl_pergi);
                            DateTime datepulang = Convert.ToDateTime(tambahrec.tgl_pulang);
                            Decimal total_harga = 0;
                            Decimal total_persen = 0;
                            for (DateTime date = datepergi; date.Date <= datepulang.Date; date = date.AddDays(1))
                            {
                                var cekharganaik = dc.MS_HARILIBUR.Where(x => EntityFunctions.TruncateTime(x.STARTDATE) <= date && EntityFunctions.TruncateTime(x.ENDDATE) >= date).FirstOrDefault();
                                Decimal jumlahpersen = 0;
                                if (cekharganaik != null)
                                {
                                    Decimal persen = Convert.ToInt32(cekharganaik.PERSEN_LIBUR);
                                    jumlahpersen = Convert.ToDecimal(harganya.HARGA) * (persen / 100);
                                    total_persen += jumlahpersen;
                                    tambahmobil.ID_HARILIBUR = cekharganaik.ID_HARILIBUR;
                                }
                                total_harga += Convert.ToDecimal(harganya.HARGA) + jumlahpersen;
                            }
                            tambahmobil.HARGA_LIBUR = (Convert.ToInt32(total_persen) * jumlah) + tambahrec.HARGA_LIBUR;
                            tambahmobil.TOTAL_PEMBAYARAN = (Convert.ToInt32(total_harga) * jumlah) + tambahrec.total_pembayaran;

                            tambahmobil.QUANTITY_KENDARAAN = tambahrec.quantity_kendaraan + jumlah;
                            //tambahmobil.TOTAL_PEMBAYARAN = ((tambahrec.harga * jumlah_durasi) * jumlah) + tambahrec.total_pembayaran;
                            tambahmobil.MODIFIED_DATE = DateTime.Now;

                            dc.Entry(tambahmobil).State = EntityState.Modified;
                            dc.Entry(tambahmobil).Property("HARGA_LIBUR").IsModified = true;
                            dc.Entry(tambahmobil).Property("QUANTITY_KENDARAAN").IsModified = true;
                            dc.Entry(tambahmobil).Property("TOTAL_PEMBAYARAN").IsModified = true;
                            dc.Entry(tambahmobil).Property("MODIFIED_DATE").IsModified = true;
                            dc.SaveChanges();


                            //ubah table transaksi
                            var jumlahtotal = dc.TR_DETAIL_PEMESANAN
                            .Where(uf => uf.ID_TRANSAKSI == tambahrec.id_transaksi)
                            .Select(uf => uf.TOTAL_PEMBAYARAN)
                            //.DefaultIfEmpty()
                            .Sum();

                            TBL_TRANSAKSI trx = dc.TBL_TRANSAKSI.Where(x => x.ID_TRANSAKSI == tambahrec.id_transaksi).FirstOrDefault();
                            trx.CATATAN = catatan;
                            alas = catatan;
                            trx.TOTAL_TRANSAKSI = jumlahtotal;
                            trx.MODIFIED_DATE = DateTime.Now;

                            dc.Entry(trx).State = EntityState.Modified;
                            dc.Entry(trx).Property("TOTAL_TRANSAKSI").IsModified = true;
                            dc.Entry(trx).Property("CATATAN").IsModified = true;
                            dc.Entry(trx).Property("MODIFIED_DATE").IsModified = true;
                            dc.SaveChanges();
                        }
                        else if (Convert.ToInt32(qtrx.QUANTITY_KENDARAAN) == qty_kendaraan)
                        {
                            audit.AKSI = "UPDATED";
                            var tambahrec = dc.v_detail_pesan.Where(a => a.id_transaksi == id_transaksi && a.id_kendaraan == id_kendaraan && a.id_pemesanan == id_pemesanan).FirstOrDefault();
                            TBL_TRANSAKSI trx = dc.TBL_TRANSAKSI.Where(x => x.ID_TRANSAKSI == tambahrec.id_transaksi).FirstOrDefault();
                            trx.CATATAN = catatan;
                            trx.MODIFIED_DATE = DateTime.Now;
                            alas = catatan;
                            dc.Entry(trx).State = EntityState.Modified;
                            dc.Entry(trx).Property("CATATAN").IsModified = true;
                            dc.Entry(trx).Property("MODIFIED_DATE").IsModified = true;
                            dc.SaveChanges();
                        }
                        else
                        {
                            audit.AKSI = "DECREASE CAR";
                            int jumlah = Convert.ToInt32(qtrx.QUANTITY_KENDARAAN) - qty_kendaraan;
                            var kurangrec = dc.v_detail_pesan.Where(a => a.id_transaksi == id_transaksi && a.id_kendaraan == id_kendaraan && a.id_pemesanan == id_pemesanan).FirstOrDefault();

                            TR_DETAIL_PEMESANAN kurangmobil = dc.TR_DETAIL_PEMESANAN.Where(x => x.ID_PEMESANAN == kurangrec.id_pemesanan).FirstOrDefault();

                            //total durasi
                            DateTime dt1 = DateTime.Parse(Convert.ToDateTime(kurangrec.tgl_pulang).AddDays(35).ToString());
                            DateTime dt2 = DateTime.Parse(Convert.ToDateTime(kurangrec.tgl_pergi).AddDays(35).ToString());
                            TimeSpan ts = dt1.Subtract(dt2);
                            int jumlah_durasi = ts.Days + 1;

                            //kalkulasi harilibur
                            var harganya = dc.TBL_HARGA.Where(x => x.ID_HARGA == kurangrec.id_harga).FirstOrDefault();
                            DateTime datepergi = Convert.ToDateTime(kurangrec.tgl_pergi);
                            DateTime datepulang = Convert.ToDateTime(kurangrec.tgl_pulang);
                            Decimal total_harga = 0;
                            Decimal total_persen = 0;
                            for (DateTime date = datepergi; date.Date <= datepulang.Date; date = date.AddDays(1))
                            {
                                var cekharganaik = dc.MS_HARILIBUR.Where(x => EntityFunctions.TruncateTime(x.STARTDATE) <= date && EntityFunctions.TruncateTime(x.ENDDATE) >= date).FirstOrDefault();
                                Decimal jumlahpersen = 0;
                                if (cekharganaik != null)
                                {
                                    Decimal persen = Convert.ToInt32(cekharganaik.PERSEN_LIBUR);
                                    jumlahpersen = Convert.ToDecimal(harganya.HARGA) * (persen / 100);
                                    total_persen += jumlahpersen;
                                    kurangmobil.ID_HARILIBUR = cekharganaik.ID_HARILIBUR;
                                }
                                total_harga += Convert.ToDecimal(harganya.HARGA) + jumlahpersen;
                            }
                            kurangmobil.HARGA_LIBUR = kurangrec.HARGA_LIBUR - (Convert.ToInt32(total_persen) * jumlah);
                            kurangmobil.TOTAL_PEMBAYARAN = kurangrec.total_pembayaran - (Convert.ToInt32(total_harga) * jumlah);

                            kurangmobil.QUANTITY_KENDARAAN = kurangrec.quantity_kendaraan - jumlah;
                            //kurangmobil.TOTAL_PEMBAYARAN = kurangrec.total_pembayaran - ((kurangrec.harga * jumlah_durasi) * jumlah);
                            kurangmobil.MODIFIED_DATE = DateTime.Now;

                            dc.Entry(kurangmobil).State = EntityState.Modified;
                            dc.Entry(kurangmobil).Property("HARGA_LIBUR").IsModified = true;
                            dc.Entry(kurangmobil).Property("QUANTITY_KENDARAAN").IsModified = true;
                            dc.Entry(kurangmobil).Property("TOTAL_PEMBAYARAN").IsModified = true;
                            dc.Entry(kurangmobil).Property("MODIFIED_DATE").IsModified = true;
                            dc.SaveChanges();


                            //ubah table transaksi
                            var jumlahtotal = dc.TR_DETAIL_PEMESANAN
                            .Where(uf => uf.ID_TRANSAKSI == kurangrec.id_transaksi)
                            .Select(uf => uf.TOTAL_PEMBAYARAN)
                            //.DefaultIfEmpty()
                            .Sum();

                            TBL_TRANSAKSI trx = dc.TBL_TRANSAKSI.Where(x => x.ID_TRANSAKSI == kurangrec.id_transaksi).FirstOrDefault();
                            trx.CATATAN = catatan;
                            alas = catatan;
                            trx.TOTAL_TRANSAKSI = jumlahtotal;
                            trx.MODIFIED_DATE = DateTime.Now;

                            dc.Entry(trx).State = EntityState.Modified;
                            dc.Entry(trx).Property("TOTAL_TRANSAKSI").IsModified = true;
                            dc.Entry(trx).Property("CATATAN").IsModified = true;
                            dc.Entry(trx).Property("MODIFIED_DATE").IsModified = true;
                            dc.SaveChanges();
                        }
                    }

                    //kalkulasi promo ulang apabila ada
                    TBL_TRANSAKSI trsx = dc.TBL_TRANSAKSI.Where(x => x.ID_TRANSAKSI == id_transaksi).FirstOrDefault();
                    TBL_PROMO prmo = dc.TBL_PROMO.Where(a => a.ID_PROMO == trsx.ID_PROMO).FirstOrDefault();
                    var countpromo = dc.TBL_PROMO.Where(a => a.ID_PROMO == trsx.ID_PROMO).Count();

                    if (countpromo > 0)
                    {
                        //jumlah total tanpa promo
                        var jumlahtotal = dc.TR_DETAIL_PEMESANAN
                        .Where(uf => uf.ID_TRANSAKSI == id_transaksi)
                        .Select(uf => uf.TOTAL_PEMBAYARAN)
                        .Sum();

                        Decimal persen = Convert.ToDecimal(prmo.PERSEN);
                        Decimal pi = persen / 100;
                        Decimal jumlahprm = Math.Round(Convert.ToDecimal(jumlahtotal) * pi);
                        trsx.JUMLAH_PROMO = Convert.ToInt32(jumlahprm);
                        trsx.ID_PROMO = prmo.ID_PROMO;
                        trsx.TOTAL_TRANSAKSI = Convert.ToInt32(Convert.ToDecimal(jumlahtotal) - jumlahprm);
                        trsx.MODIFIED_DATE = DateTime.Now;

                        //update tabel utama
                        dc.Entry(trsx).State = EntityState.Modified;
                        dc.Entry(trsx).Property("JUMLAH_PROMO").IsModified = true;
                        dc.Entry(trsx).Property("ID_PROMO").IsModified = true;
                        dc.Entry(trsx).Property("TOTAL_TRANSAKSI").IsModified = true;
                        dc.Entry(trsx).Property("MODIFIED_DATE").IsModified = true;
                        dc.SaveChanges();
                    }
                    
                    //audit trail edit
                    
                    audit.NAMA_TABEL = "TBL_TRANSAKSI,TR_DETAIL_PEMESANAN";
                    audit.userid = Session["ID_MEMBER"].ToString();
                    audit.NAMA_FIELD = "";
                    audit.VALUE_LAMA = oke.ID_TRANSAKSI + ", " + oke.TOTAL_TRANSAKSI + ", " + catatan;
                    audit.VALUE_BARU = oke.ID_TRANSAKSI + ", " + trsx.TOTAL_TRANSAKSI +", " + catatan;
                    audit.CREATED_DATE = DateTime.Now;
                    audit.MODIFIED_DATE = DateTime.Now;
                    dc.TBL_AUDIT_TRAIL.Add(audit);
                    dc.SaveChanges();

                    transactions.Commit();
                    return RedirectToAction("Cart/" + id_transaksi + "", "Cart");
                    //return RedirectToAction("Index");
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    ViewBag.Error = msgErr.Message;
                }
            }
            return null;
        }

        public ActionResult Delete1Cart(int id)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    var carihapus = dc.TR_DETAIL_PEMESANAN.Where(a => a.ID_PEMESANAN == id).FirstOrDefault();
                    var caritotaldihapus = dc.TBL_TRANSAKSI.Where(a => a.ID_TRANSAKSI == carihapus.ID_TRANSAKSI).FirstOrDefault();
                    var idreturn = caritotaldihapus.ID_TRANSAKSI;
                    int dikurang = Convert.ToInt32(caritotaldihapus.TOTAL_TRANSAKSI) - Convert.ToInt32(carihapus.TOTAL_PEMBAYARAN);

                    //kurangi tabel utama
                    TBL_TRANSAKSI trx = dc.TBL_TRANSAKSI.Where(x => x.ID_TRANSAKSI == carihapus.ID_TRANSAKSI).FirstOrDefault();
                    trx.TOTAL_TRANSAKSI = dikurang;
                    trx.MODIFIED_DATE = DateTime.Now;

                    dc.Entry(trx).State = EntityState.Modified;
                    dc.Entry(trx).Property("TOTAL_TRANSAKSI").IsModified = true;
                    dc.Entry(trx).Property("MODIFIED_DATE").IsModified = true;
                    dc.SaveChanges();

                    //hapus 1 detail
                    var cekrecord = dc.TR_DETAIL_PEMESANAN.Where(a => a.ID_TRANSAKSI == idreturn).Count();
                    if (cekrecord > 1)
                    {
                        dc.TR_DETAIL_PEMESANAN.Remove(carihapus);
                        dc.SaveChanges();


                        //kalkulasi promo ulang apabila ada
                        TBL_TRANSAKSI trsx = dc.TBL_TRANSAKSI.Where(x => x.ID_TRANSAKSI == idreturn).FirstOrDefault();
                        TBL_PROMO prmo = dc.TBL_PROMO.Where(a => a.ID_PROMO == trsx.ID_PROMO).FirstOrDefault();
                        var countpromo = dc.TBL_PROMO.Where(a => a.ID_PROMO == trsx.ID_PROMO).Count();

                        if (countpromo > 0)
                        {
                            //jumlah total tanpa promo
                            var jumlahtotal = dc.TR_DETAIL_PEMESANAN
                            .Where(uf => uf.ID_TRANSAKSI == idreturn)
                            .Select(uf => uf.TOTAL_PEMBAYARAN)
                            .Sum();

                            Decimal persen = Convert.ToDecimal(prmo.PERSEN);
                            Decimal pi = persen / 100;
                            Decimal jumlahprm = Math.Round(Convert.ToDecimal(jumlahtotal) * pi);
                            trsx.JUMLAH_PROMO = Convert.ToInt32(jumlahprm);
                            trsx.ID_PROMO = prmo.ID_PROMO;
                            trsx.TOTAL_TRANSAKSI = Convert.ToInt32(Convert.ToDecimal(jumlahtotal) - jumlahprm);
                            trsx.MODIFIED_DATE = DateTime.Now;

                            //update tabel utama
                            dc.Entry(trsx).State = EntityState.Modified;
                            dc.Entry(trsx).Property("JUMLAH_PROMO").IsModified = true;
                            dc.Entry(trsx).Property("ID_PROMO").IsModified = true;
                            dc.Entry(trsx).Property("TOTAL_TRANSAKSI").IsModified = true;
                            dc.Entry(trsx).Property("MODIFIED_DATE").IsModified = true;
                            dc.SaveChanges();
                        }

                        //audit trail edit
                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                        audit.NAMA_TABEL = "TBL_TRANSAKSI,TR_DETAIL_PEMESANAN";
                        audit.AKSI = "DECREASE CAR ON CART";
                        audit.userid = Session["ID_MEMBER"].ToString();
                        audit.NAMA_FIELD = "";
                        audit.VALUE_LAMA = "";
                        audit.VALUE_BARU = trx.ID_TRANSAKSI.ToString() +", "+trx.TOTAL_TRANSAKSI;
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();

                        transactions.Commit();
                        return RedirectToAction("Cart/" + idreturn + "", "Cart");
                    }
                    else
                    {
                        dc.TR_DETAIL_PEMESANAN.Remove(carihapus);
                        dc.TBL_TRANSAKSI.Remove(caritotaldihapus);
                        dc.SaveChanges();

                        //audit trail edit
                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                        audit.NAMA_TABEL = "TBL_TRANSAKSI,TR_DETAIL_PEMESANAN";
                        audit.AKSI = "DELETE CART";
                        audit.userid = Session["ID_MEMBER"].ToString();
                        audit.NAMA_FIELD = "";
                        audit.VALUE_LAMA = "";
                        audit.VALUE_BARU = id.ToString();
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();

                        transactions.Commit();
                        return RedirectToAction("Index", "Cart");
                    }
                }
                catch (Exception msgErr)
                {
                    transactions.Rollback();
                    ViewBag.Error = msgErr.Message;
                }
            }
            return null;
        }

        public ActionResult DeleteAllCart(int id)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    //haus detail
                    dc.TR_DETAIL_PEMESANAN.RemoveRange(dc.TR_DETAIL_PEMESANAN.Where(c => c.ID_TRANSAKSI == id));

                    //hapus table utama
                    dc.TBL_TRANSAKSI.RemoveRange(dc.TBL_TRANSAKSI.Where(c => c.ID_TRANSAKSI == id));
                    dc.SaveChanges();

                    //audit trail edit
                    TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                    audit.NAMA_TABEL = "TBL_TRANSAKSI,TR_DETAIL_PEMESANAN";
                    audit.AKSI = "DELETE CART";
                    audit.userid = Session["ID_MEMBER"].ToString();
                    audit.NAMA_FIELD = "";
                    audit.VALUE_LAMA = "";
                    audit.VALUE_BARU = id.ToString();
                    audit.CREATED_DATE = DateTime.Now;
                    audit.MODIFIED_DATE = DateTime.Now;
                    dc.TBL_AUDIT_TRAIL.Add(audit);
                    dc.SaveChanges();

                    transactions.Commit();
                    return RedirectToAction("Index", "Cart");
                }
                catch (Exception msgErr)
                {
                    transactions.Rollback();
                    ViewBag.Error = msgErr.Message;
                }
            }
            return null;
        }

        public ActionResult Discount(FormCollection nihf)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    string promo = nihf["discount"];
                    int id_trsk = Convert.ToInt32(nihf["id_trans"]);
                    var oke = dc.TBL_TRANSAKSI.AsNoTracking().Where(x => x.ID_TRANSAKSI == id_trsk).FirstOrDefault();
                    TBL_TRANSAKSI trx = dc.TBL_TRANSAKSI.Where(x => x.ID_TRANSAKSI == id_trsk).FirstOrDefault();
                    TBL_PROMO prmo = dc.TBL_PROMO.Where(a => a.KODE_PROMO == promo).FirstOrDefault();
                    var countpromo = dc.TBL_PROMO.Where(a => a.KODE_PROMO == promo).Count();

                    if (countpromo > 0)
                    {
                        //jumlah total tanpa promo
                        var jumlahtotal = dc.TR_DETAIL_PEMESANAN
                        .Where(uf => uf.ID_TRANSAKSI == id_trsk)
                        .Select(uf => uf.TOTAL_PEMBAYARAN)
                        .Sum();

                        Decimal persen = Convert.ToDecimal(prmo.PERSEN);
                        Decimal pi = persen / 100;
                        Decimal jumlahprm = Math.Round(Convert.ToDecimal(jumlahtotal) * pi);
                        trx.JUMLAH_PROMO = Convert.ToInt32(jumlahprm);
                        trx.ID_PROMO = prmo.ID_PROMO;
                        trx.TOTAL_TRANSAKSI = Convert.ToInt32(Convert.ToDecimal(jumlahtotal) - jumlahprm);
                        trx.MODIFIED_DATE = DateTime.Now;

                        //update tabel utama
                        dc.Entry(trx).State = EntityState.Modified;
                        dc.Entry(trx).Property("JUMLAH_PROMO").IsModified = true;
                        dc.Entry(trx).Property("ID_PROMO").IsModified = true;
                        dc.Entry(trx).Property("TOTAL_TRANSAKSI").IsModified = true;
                        dc.Entry(trx).Property("MODIFIED_DATE").IsModified = true;
                        dc.SaveChanges();

                        //audit trail edit
                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                        audit.NAMA_TABEL = "TBL_TRANSAKSI";
                        audit.AKSI = "ADD PROMO";
                        audit.userid = Session["ID_MEMBER"].ToString();
                        audit.NAMA_FIELD = "NAMA_BANK";
                        audit.VALUE_LAMA = oke.ID_TRANSAKSI + ", " + oke.JUMLAH_PROMO;
                        audit.VALUE_BARU = id_trsk + ", " + trx.JUMLAH_PROMO;
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();
                    }
                    transactions.Commit();
                    return RedirectToAction("Cart/" + id_trsk + "", "Cart");
                }
                catch (Exception msgErr)
                {
                    transactions.Rollback();
                    ViewBag.Error = msgErr.Message;
                }
            }
            return null;
        }

        public ActionResult DeletePromo(int id)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    var oke = dc.TBL_TRANSAKSI.AsNoTracking().Where(x => x.ID_TRANSAKSI == id).FirstOrDefault();
                    TBL_TRANSAKSI trx = dc.TBL_TRANSAKSI.Where(x => x.ID_TRANSAKSI == id).FirstOrDefault();

                    //jumlah total tanpa promo
                    var jumlahtotal = dc.TR_DETAIL_PEMESANAN
                    .Where(uf => uf.ID_TRANSAKSI == id)
                    .Select(uf => uf.TOTAL_PEMBAYARAN)
                    .Sum();

                    trx.JUMLAH_PROMO = null;
                    trx.ID_PROMO = null;
                    trx.MODIFIED_DATE = DateTime.Now;
                    trx.TOTAL_TRANSAKSI = jumlahtotal;
                    //update tabel utama
                    dc.Entry(trx).State = EntityState.Modified;
                    dc.Entry(trx).Property("JUMLAH_PROMO").IsModified = true;
                    dc.Entry(trx).Property("ID_PROMO").IsModified = true;
                    dc.Entry(trx).Property("TOTAL_TRANSAKSI").IsModified = true;
                    dc.Entry(trx).Property("MODIFIED_DATE").IsModified = true;
                    dc.SaveChanges();

                    //audit trail edit
                    TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                    audit.NAMA_TABEL = "TBL_TRANSAKSI";
                    audit.AKSI = "EDIT,DELETE PROMO";
                    audit.userid = Session["ID_MEMBER"].ToString();
                    audit.NAMA_FIELD = "";
                    audit.VALUE_LAMA = id + ", " + oke.JUMLAH_PROMO + ", " + oke.ID_PROMO + ", " + oke.TOTAL_TRANSAKSI;
                    audit.VALUE_BARU = id + ", " + trx.JUMLAH_PROMO + ", " + trx.ID_PROMO + ", " + trx.TOTAL_TRANSAKSI;
                    audit.CREATED_DATE = DateTime.Now;
                    audit.MODIFIED_DATE = DateTime.Now;
                    dc.TBL_AUDIT_TRAIL.Add(audit);
                    dc.SaveChanges();
                    transactions.Commit();
                    return RedirectToAction("Cart/" + id + "", "Cart");
                }
                catch (Exception msgErr)
                {
                    transactions.Rollback();
                    ViewBag.Error = msgErr.Message;
                }
            }
            return null;
        }

        public ActionResult Checkout(int id)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    var oke = dc.TBL_TRANSAKSI.AsNoTracking().Where(x => x.ID_TRANSAKSI == id).FirstOrDefault();
                    TBL_TRANSAKSI trx = dc.TBL_TRANSAKSI.Where(x => x.ID_TRANSAKSI == id).FirstOrDefault();

                    trx.APPROVAL = 1;
                    trx.STATUS = 1; //status 1 artinya sudah checkout, tinggal menunggu konfirmasi pembayaran
                    trx.MODIFIED_DATE = DateTime.Now;

                    //update tabel utama
                    dc.Entry(trx).State = EntityState.Modified;
                    dc.Entry(trx).Property("STATUS").IsModified = true;
                    dc.Entry(trx).Property("MODIFIED_DATE").IsModified = true;
                    dc.SaveChanges();

                    //audit trail edit
                    TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                    audit.NAMA_TABEL = "TBL_TRANSAKSI";
                    audit.AKSI = "EDIT,CHECKOUT";
                    audit.userid = Session["ID_MEMBER"].ToString();
                    audit.NAMA_FIELD = "";
                    audit.VALUE_LAMA = id + ", " + oke.APPROVAL + ", " + oke.STATUS;
                    audit.VALUE_BARU = id + ", " + trx.APPROVAL + ", " + trx.STATUS;
                    audit.CREATED_DATE = DateTime.Now;
                    audit.MODIFIED_DATE = DateTime.Now;
                    dc.TBL_AUDIT_TRAIL.Add(audit);
                    dc.SaveChanges();

                    transactions.Commit();
                    return RedirectToAction("Rekening/" + id + "", "ScheduleUser");
                }
                catch (Exception msgErr)
                {
                    transactions.Rollback();
                    ViewBag.Error = msgErr.Message;
                }
            }
            return null;
        }
    }

    public class transaksi
    {
        public Nullable<System.DateTime> TGL_PERGI { get; set; }
        public Nullable<System.DateTime> TGL_PULANG { get; set; }
        public String NAMA_KOTA { set; get; }
        public String NAMA_PROVINSI { set; get; }
        public Nullable<int> TOTAL_DURASI { get; set; }
        public Nullable<int> TOTAL_TRANSAKSI { get; set; }
        public int ID_TRANSAKSI { get; set; }
        public Nullable<int> ID_KENDARAAN { get; set; }
    }

    
}