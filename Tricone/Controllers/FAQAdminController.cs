﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class FAQAdminController : Controller
    {
        // GET: FAQ
        DB_TRICONEEntitie dc = new DB_TRICONEEntitie();

        public ActionResult Index()
        {
            return View(dc.TBL_FAQ.ToList());
        }

        public ActionResult Getdata()
        {
            List<TBL_FAQ> FAQList = dc.TBL_FAQ.ToList<TBL_FAQ>();
            List<FAQOutput> result = new List<FAQOutput>();
            foreach (var item in FAQList)
            {
                result.Add(new FAQOutput
                {
                    ID_FAQ = item.ID_FAQ,
                    PERTANYAAN = item.PERTANYAAN,
                    JAWABAN = item.JAWABAN
                });
            }

            //var json = Newtonsoft.Json.JsonConvert.SerializeObject(result);
            return Json(new { data = result.OrderByDescending(e => e.ID_FAQ) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddorEdit(int id = 0)
        {
            if (id == 0)
            {
                return View(new TBL_FAQ());
            }
            else
            {
                return View(dc.TBL_FAQ.Where(x => x.ID_FAQ == id).FirstOrDefault<TBL_FAQ>());
            }
        }

        [HttpPost]
        public ActionResult AddorEdit(TBL_FAQ faq)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    faq.MODIFIED_DATE = DateTime.Now;
                    if (faq.ID_FAQ == 0)
                    {
                        faq.CREATED_DATE = DateTime.Now;
                        dc.TBL_FAQ.Add(faq);
                        dc.SaveChanges();

                        //audit trail
                        var fa = dc.TBL_FAQ.OrderByDescending(e => e.ID_FAQ).FirstOrDefault();
                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                        audit.NAMA_TABEL = "TBL_FAQ";
                        audit.AKSI = "ADD";
                        audit.NAMA_FIELD = "All Field";
                        audit.VALUE_LAMA = "";
                        audit.userid = Session["ID_MEMBER"].ToString();
                        audit.VALUE_BARU = fa.ID_FAQ + ", " + fa.PERTANYAAN + ", " + fa.JAWABAN ;
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();

                        transactions.Commit();
                        return Json(new { success = true, message = "Saved Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var oke = dc.TBL_FAQ.AsNoTracking().Where(x => x.ID_FAQ == faq.ID_FAQ).FirstOrDefault();
                        dc.Entry(faq).State = EntityState.Modified;
                        dc.Entry(faq).Property("CREATED_DATE").IsModified = false;
                        dc.SaveChanges();

                        //audit trail

                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                        audit.NAMA_TABEL = "TBL_FAQ";
                        audit.AKSI = "EDIT";
                        audit.userid = Session["ID_MEMBER"].ToString();
                        audit.NAMA_FIELD = "ID_FAQ" + ", " + "PERTANYAAN" + ", " + "JAWABAN";
                        audit.VALUE_LAMA = faq.ID_FAQ + ", " + oke.PERTANYAAN + ", " + oke.JAWABAN;
                        audit.VALUE_BARU = faq.ID_FAQ + ", " + faq.PERTANYAAN + ", " + faq.JAWABAN;
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();

                        transactions.Commit();
                        return Json(new { success = true, message = "Updated Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    return Json(new { success = true, message = msgErr.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        // GET: Member/Details/5
        public ActionResult Details(int id)
        {
            using (DB_TRICONEEntitie dc = new DB_TRICONEEntitie())
            {
                return View(dc.TBL_FAQ.Where(x => x.ID_FAQ == id).FirstOrDefault());

            }

        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            TBL_FAQ faq = dc.TBL_FAQ.Where(x => x.ID_FAQ == id).FirstOrDefault<TBL_FAQ>();
            dc.TBL_FAQ.Remove(faq);
            dc.SaveChanges();
            return Json(new { success = true, message = "Deleted Successfully" }, JsonRequestBehavior.AllowGet);
        }
    }
    public class FAQOutput
    {
        public int ID_FAQ { get; set; }
        public string PERTANYAAN { get; set; }
        public string JAWABAN { get; set; }
    }
}