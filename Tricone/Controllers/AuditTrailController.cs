﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class AuditTrailController : Controller
    {
        // GET: AuditTrail
        DB_TRICONEEntitie dc = new DB_TRICONEEntitie();
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult Getdata()
        {
            List<TBL_AUDIT_TRAIL> auditlist = dc.TBL_AUDIT_TRAIL.ToList<TBL_AUDIT_TRAIL>();
            List<auditOutput> result = new List<auditOutput>();
            foreach (var item in auditlist)
            {
                result.Add(new auditOutput
                {
                    ID_AUDIT = item.ID_AUDIT,
                    NAMA_TABEL = item.NAMA_TABEL,
                    AKSI = item.AKSI,
                    NAMA_FIELD = item.NAMA_FIELD,
                    VALUE_LAMA = item.VALUE_LAMA,
                    VALUE_BARU = item.VALUE_BARU,
                    userid = item.userid,
                    CREATED_DATE = item.CREATED_DATE
                });
            }
            return Json(new { data = result.OrderByDescending(e => e.ID_AUDIT) }, JsonRequestBehavior.AllowGet);
        }
    }
}