﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;


namespace Tricone.Controllers
{
    public class ReportController : Controller
    {
        // GET: Report
        public ActionResult Member()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);

            /* ini kalau pake passing parameter dari vs */
            //ReportParameter[] reportParameter = new ReportParameter[1];
            //reportParameter[0] = new ReportParameter("FirstName", "Ken");
            //report.ServerReport.ReportPath = "/Report-TriconeTravel";
            //report.ServerReport.SetParameters(reportParameter);
            //report.ServerReport.Refresh();
            /*ini kalo panggil langsung*/
            report.ServerReport.ReportPath = "/DataMember-TriconeTravel";
            ViewBag.ReportViewer = report;
            return View();

        }

        public ActionResult Kota()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            report.ServerReport.ReportPath = "/DataKota-TriconeTravel";
            ViewBag.ReportViewer = report;
            return View();
        }

        public ActionResult DetailPemesanan()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            report.ServerReport.ReportPath = "/DataDetailPemesanan-TriconeTravel";
            ViewBag.ReportViewer = report;
            return View();
        }

        public ActionResult Refund()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            report.ServerReport.ReportPath = "/DataRefund-TriconeTravel";
            ViewBag.ReportViewer = report;
            return View();
        }

        public ActionResult Transaksi()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            report.ServerReport.ReportPath = "/DataTransaksi-TriconeTravel";
            ViewBag.ReportViewer = report;
            return View();
        }

        public ActionResult Kendaraan()
        {
            string ssrsUrl = ConfigurationManager.AppSettings["SSRSReportsUrl"].ToString();
            ReportViewer report = new ReportViewer();
            report.ProcessingMode = ProcessingMode.Remote;
            report.Width = Unit.Pixel(1100);
            report.ServerReport.ReportServerUrl = new Uri(ssrsUrl);
            report.ServerReport.ReportPath = "/DataKendaraan-TriconeTravel";
            ViewBag.ReportViewer = report;
            return View();
        }
    }
}