﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class RefundController : Controller
    {
        DB_TRICONEEntitie dc = new DB_TRICONEEntitie();
        // GET: Refund
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult Getdata()
        {
            List<v_refund> refundList = dc.v_refund.ToList<v_refund>();
            List<refundadminOutput> result = new List<refundadminOutput>();
            foreach (var item in refundList)
            {
                result.Add(new refundadminOutput
                {
                   
                    ID_REFUND = item.ID_REFUND,
                    TGL_PERGI = item.TGL_PERGI,
                    NAMA_KOTA = item.NAMA_KOTA,
                    NAMA_MEMBER = item.NAMA_MEMBER,
                    FOTO_REFUND = item.FOTO_REFUND,
                    KETERANGANUSER = item.KETERANGANUSER,
                    NAMA_REKENING = item.NAMA_REKENING,
                    TOTAL_REFUND = Convert.ToInt32(item.TOTAL_REFUND),
                    STATUS = Convert.ToInt32(item.STATUS),
                    APPROVAL = Convert.ToInt32(item.APPROVAL)
                });
            }
            return Json(new { data = result.OrderByDescending(e => e.ID_REFUND) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Approve(int id)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    var oke = dc.TBL_TRANSAKSI.AsNoTracking().Where(x => x.ID_REFUND == id).FirstOrDefault();
                    //table refund
                    var trx = dc.TBL_REFUND.Where(x => x.ID_REFUND == id).FirstOrDefault();
                    trx.APPROVAL = 4;
                    trx.STATUS = 1;
                    trx.MODIFIED_DATE = DateTime.Now;

                    dc.Entry(trx).State = EntityState.Modified;
                    dc.Entry(trx).Property("STATUS").IsModified = true;
                    dc.Entry(trx).Property("APPROVAL").IsModified = true;
                    dc.Entry(trx).Property("MODIFIED_DATE").IsModified = true;
                    dc.SaveChanges();

                    //table transaksi di nonaktif
                    var trxtrans = dc.TBL_TRANSAKSI.Where(x => x.ID_TRANSAKSI == trx.ID_TRANSAKSI).FirstOrDefault();
                    trxtrans.STATUS = 0;
                    trxtrans.MODIFIED_DATE = DateTime.Now;
                    dc.Entry(trxtrans).State = EntityState.Modified;
                    dc.Entry(trxtrans).Property("STATUS").IsModified = true;
                    dc.Entry(trxtrans).Property("MODIFIED_DATE").IsModified = true;
                    dc.SaveChanges();

                    //table detail dinonaktifkan
                    var trxdetail = dc.TR_DETAIL_PEMESANAN.Where(x => x.ID_TRANSAKSI == trx.ID_TRANSAKSI).FirstOrDefault();
                    trxdetail.STATUS = 0;
                    trxdetail.MODIFIED_DATE = DateTime.Now;
                    dc.Entry(trxdetail).State = EntityState.Modified;
                    dc.Entry(trxdetail).Property("STATUS").IsModified = true;
                    dc.Entry(trxdetail).Property("MODIFIED_DATE").IsModified = true;
                    dc.SaveChanges();

                    //audit trail edit
                    TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                    audit.NAMA_TABEL = "TBL_REFUND,TBL_TRANSAKSI";
                    audit.AKSI = "EDIT, APPROVED";
                    audit.userid = Session["ID_MEMBER"].ToString();
                    audit.NAMA_FIELD = "";
                    audit.VALUE_LAMA = oke.ID_TRANSAKSI + ", " + oke.ID_REFUND + ", " + oke.APPROVAL;
                    audit.VALUE_BARU = trx.ID_TRANSAKSI + ", " + trx.ID_REFUND + ", " + trx.APPROVAL;
                    audit.CREATED_DATE = DateTime.Now;
                    audit.MODIFIED_DATE = DateTime.Now;
                    dc.TBL_AUDIT_TRAIL.Add(audit);
                    dc.SaveChanges();

                    //buat notifikasi
                    var notit = dc.v_refund.Where(x => x.ID_REFUND == id).FirstOrDefault();
                    notifikasi noti = new notifikasi();
                    noti.NAMA_NOTIF = "refund";
                    noti.NOTIF_FOR = notit.ID_MEMBER.ToString();
                    dc.Entry(noti).State = EntityState.Added;
                    dc.SaveChanges();
                    transactions.Commit();
                    return RedirectToAction("Index", "Refund");
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    ViewBag.Error = msgErr.Message;
                }
            }return null;
        }

        public ActionResult popreject(int id)
        {
            ViewBag.id_trans = id;
            return View();
        }

        public ActionResult Reject(FormCollection formnya)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    int id = Convert.ToInt32(formnya["id_trans"]);
                    var keterangan = formnya["KETERANGANADMIN"];
                    var oke = dc.TBL_REFUND.AsNoTracking().Where(x => x.ID_REFUND == id).FirstOrDefault();
                    var trx = dc.TBL_REFUND.Where(x => x.ID_REFUND == id).FirstOrDefault();
                    trx.STATUS = 0;
                    trx.APPROVAL = 3;
                    trx.KETERANGANADMIN = keterangan;
                    trx.MODIFIED_DATE = DateTime.Now;

                    dc.Entry(trx).State = EntityState.Modified;
                    dc.Entry(trx).Property("KETERANGANADMIN").IsModified = true;
                    dc.Entry(trx).Property("STATUS").IsModified = true;
                    dc.Entry(trx).Property("APPROVAL").IsModified = true;
                    dc.Entry(trx).Property("MODIFIED_DATE").IsModified = true;
                    dc.SaveChanges();

                    //audit trail edit
                    TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                    audit.NAMA_TABEL = "TBL_REFUND";
                    audit.AKSI = "EDIT, REJECTED";
                    audit.userid = Session["ID_MEMBER"].ToString();
                    audit.NAMA_FIELD = "";
                    audit.VALUE_LAMA = oke.ID_REFUND + ", " + oke.STATUS + ", " + oke.APPROVAL;
                    audit.VALUE_BARU = trx.ID_REFUND + ", " + trx.STATUS + ", " + trx.APPROVAL;
                    audit.CREATED_DATE = DateTime.Now;
                    audit.MODIFIED_DATE = DateTime.Now;
                    dc.TBL_AUDIT_TRAIL.Add(audit);
                    dc.SaveChanges();

                    //buat notifikasi
                    var notit = dc.v_refund.Where(x => x.ID_REFUND == id).FirstOrDefault();
                    notifikasi noti = new notifikasi();
                    noti.NAMA_NOTIF = "refund";
                    noti.NOTIF_FOR = notit.ID_MEMBER.ToString();
                    dc.Entry(noti).State = EntityState.Added;
                    dc.SaveChanges();
                    transactions.Commit();
                    return Json(new { success = true, message = "Reject Successfully" }, JsonRequestBehavior.AllowGet);
                    //return RedirectToAction("Index", "Refund");
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    ViewBag.Error = msgErr.Message;
                }
            }
            return null;
        }

        [HttpGet]
        public ActionResult UploadPht(int id)
        {
            TBL_REFUND trxmodel = new TBL_REFUND();

            var trxfoto = dc.TBL_REFUND.Find(id);
            Session["imgPath"] = trxmodel.FOTO_REFUND;
            if (trxfoto == null)
            {
                return HttpNotFound();
            }
            var trxs = dc.TBL_REFUND.Where(x => x.ID_REFUND == id).FirstOrDefault();
            ViewBag.foto = trxs.FOTO_REFUND;
            ViewBag.id_trans = id;
            return View(trxmodel);
        }

        
        [HttpPost]
        public ActionResult Uploadp(FormCollection trx, HttpPostedFileBase filebase)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    int id_trns = Convert.ToInt32(trx["id_trans"]);
                    var fotolama = trx["fotolama"];
                    var trxs = dc.TBL_REFUND.Where(x => x.ID_REFUND == id_trns).FirstOrDefault();
                    var oke = dc.TBL_REFUND.AsNoTracking().Where(x => x.ID_REFUND == id_trns).FirstOrDefault();
                    if (filebase != null)
                    {
                        //delete foto lama
                        string fullPath = Request.MapPath("~/images/" + fotolama);
                        if (System.IO.File.Exists(fullPath))
                        {
                            System.IO.File.Delete(fullPath);
                        }

                        //penambahan foto baru

                        string fileName = Path.GetFileNameWithoutExtension(filebase.FileName);
                        string extension = Path.GetExtension(filebase.FileName);
                        fileName = fileName + DateTime.Now.ToString("yyyyMMddHHmmssfff") + extension;
                        //if (kendaraan.pathFoto==null) { kendaraan.pathFoto = fileName; }
                        trxs.FOTO_REFUND = fileName;
                        fileName = Path.Combine(Server.MapPath("~/images/"), fileName);
                        filebase.SaveAs(fileName);
                        trxs.MODIFIED_DATE = DateTime.Now;
                        trxs.STATUS = 0;
                        trxs.APPROVAL = 5;
                        dc.Entry(trxs).State = EntityState.Modified;
                        dc.Entry(trxs).Property("FOTO_REFUND").IsModified = true;
                        dc.Entry(trxs).Property("MODIFIED_DATE").IsModified = true;
                        dc.Entry(trxs).Property("STATUS").IsModified = true;
                        dc.Entry(trxs).Property("APPROVAL").IsModified = true;
                        dc.SaveChanges();

                        //audit trail edit
                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                        audit.NAMA_TABEL = "TBL_REFUND";
                        audit.AKSI = "UPLOAD PAYMENT REFUND";
                        audit.userid = Session["ID_MEMBER"].ToString();
                        audit.NAMA_FIELD = "";
                        audit.VALUE_LAMA = oke.ID_REFUND + ", " + oke.FOTO_REFUND;
                        audit.VALUE_BARU = trxs.ID_REFUND + ", " + trxs.FOTO_REFUND;
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();

                        //buat notifikasi
                        var notit = dc.v_refund.Where(x => x.ID_REFUND == id_trns).FirstOrDefault();
                        notifikasi noti = new notifikasi();
                        noti.NAMA_NOTIF = "refund";
                        noti.NOTIF_FOR = notit.ID_MEMBER.ToString();
                        dc.Entry(noti).State = EntityState.Added;
                        dc.SaveChanges();
                        transactions.Commit();
                        return Json(new { success = true, message = "Updated Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        transactions.Commit();
                        return Json(new { success = true, message = "Please Try Again" }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    ViewBag.Error = msgErr.Message;
                }
            }
            return null;
        }
        
        [HttpGet]
        public ActionResult Details(int id)
        {
            ViewBag.id_trans = id;
            return View();
        }

        [HttpGet]
        public JsonResult GetdataDetail(int id_trans)
        {
            var refu = dc.TBL_REFUND.Where(g => g.ID_REFUND == id_trans).FirstOrDefault();
            List<v_detail_pesan> detailtransaksiList = dc.v_detail_pesan.ToList<v_detail_pesan>();
            List<detailtransaksiOutput> result = new List<detailtransaksiOutput>();
            foreach (var item in detailtransaksiList)
            {
                if (refu.ID_TRANSAKSI == item.id_transaksi)
                {
                    result.Add(new detailtransaksiOutput
                    {
                        ID_TRANSAKSI = Convert.ToInt32(item.id_transaksi),
                        id_pemesanan = Convert.ToInt32(item.id_pemesanan),
                        id_member = Convert.ToInt32(item.id_member),
                        nama_member = item.nama_member,
                        telpon_member = item.telpon_member,
                        email_member = item.email_member,
                        id_harga = Convert.ToInt32(item.id_harga),
                        harga = Convert.ToInt32(item.harga),
                        id_kendaraan = Convert.ToInt32(item.id_kendaraan),
                        id_jenis_kendaraan = Convert.ToInt32(item.id_jenis_kendaraan),
                        nama_kendaraan = item.nama_kendaraan,
                        jumlah_seat = Convert.ToInt32(item.jumlah_seat),
                        jumlah_kendaraan = Convert.ToInt32(item.jumlah_kendaraan),
                        status_kendaraan = Convert.ToInt32(item.status_kendaraan),
                        durasi_jam = Convert.ToInt32(item.durasi_jam),
                        id_harilibur = Convert.ToInt32(item.id_harilibur),
                        nama_harilibur = item.nama_harilibur,
                        startdate_libur = Convert.ToDateTime(item.startdate_libur),
                        enddate_libur = Convert.ToDateTime(item.enddate_libur),
                        persen_libur = Convert.ToInt32(item.persen_libur),
                        tgl_pergi = Convert.ToDateTime(item.tgl_pergi),
                        tgl_pulang = Convert.ToDateTime(item.tgl_pulang),
                        id_kota = Convert.ToInt32(item.id_kota),
                        nama_kota = item.nama_kota,
                        id_provinsi = Convert.ToInt32(item.id_provinsi),
                        nama_provinsi = item.nama_provinsi,
                        quantity_kendaraan = Convert.ToInt32(item.quantity_kendaraan),
                        total_pembayaran = Convert.ToInt32(item.total_pembayaran),
                        status_detail_pesan = Convert.ToInt32(item.status_detail_pesan),
                        id_supir = Convert.ToInt32(item.id_supir),
                        nama_supir = item.nama_supir,
                        telpon_supir = item.telpon_supir

                    });
                }
            }
            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }
    }
}