﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class HargaController : Controller
    {
        // GET: Harga
        DB_TRICONEEntitie dc = new DB_TRICONEEntitie();

        public ActionResult Index()
        {
            return View(dc.TBL_HARGA.ToList());
        }

        [HttpGet]
        public JsonResult Getdata()
        {
            List<TBL_HARGA> hargalist = dc.TBL_HARGA.ToList<TBL_HARGA>();
            List<HargaOutput> result = new List<HargaOutput>();
            foreach (var item in hargalist)
            {
                result.Add(new HargaOutput
                {
                    ID_HARGA = item.ID_HARGA,
                    ID_KENDARAAN = item.MS_KENDARAAN.ID_KENDARAAN,
                    NAMA_KENDARAAN = item.MS_KENDARAAN.MS_JENIS_KENDARAAN.NAMA_KENDARAAN,
                    ID_PROVINSI = item.MS_KOTA.MS_PROVINSI.ID_PROVINSI,
                    NAMA_PROVINSI = item.MS_KOTA.MS_PROVINSI.NAMA_PROVINSI,
                    ID_KOTA = item.MS_KOTA.ID_KOTA,
                    NAMA_KOTA = item.MS_KOTA.NAMA_KOTA,
                    DURASI = Convert.ToInt32(item.DURASI),
                    HARGA = Convert.ToInt32(item.HARGA)
                });
            }

            //var json = Newtonsoft.Json.JsonConvert.SerializeObject(result);
            return Json(new { data = result.OrderByDescending(e => e.ID_HARGA) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddorEdit(int id = 0)
        {
            TBL_HARGA kotamodel = new TBL_HARGA();
            kotamodel.kotacollection = dc.v_kota.ToList<v_kota>();
            kotamodel.kendcollection = dc.v_kendaraan.ToList<v_kendaraan>();
            if (id == 0)
            {
                return View(kotamodel);
            }
            else
            {
                kotamodel = dc.TBL_HARGA.Where(x => x.ID_HARGA == id).FirstOrDefault();
                kotamodel.kotacollection = dc.v_kota.ToList<v_kota>();
                kotamodel.kendcollection = dc.v_kendaraan.ToList<v_kendaraan>();
                return View(kotamodel);
            }
        }

        [HttpPost]
        public ActionResult AddorEdit(TBL_HARGA hrg)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    hrg.MODIFIED_DATE = DateTime.Now;
                    if (hrg.ID_HARGA == 0)
                    {
                        hrg.CREATED_DATE = DateTime.Now;
                        dc.TBL_HARGA.Add(hrg);
                        dc.SaveChanges();

                        //audit trail
                        var harga = dc.TBL_HARGA.OrderByDescending(e => e.ID_HARGA).FirstOrDefault();
                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                        audit.NAMA_TABEL = "TBL_HARGA";
                        audit.AKSI = "ADD";
                        audit.NAMA_FIELD = "All Field";
                        audit.VALUE_LAMA = "";
                        audit.userid = Session["ID_MEMBER"].ToString();
                        audit.VALUE_BARU = harga.ID_HARGA + ", " + harga.ID_KOTA + ", " + harga.ID_KENDARAAN + ", " + harga.DURASI+ ", " + harga.HARGA;
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();

                        transactions.Commit();
                        return Json(new { success = true, message = "Saved Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var oke = dc.TBL_HARGA.AsNoTracking().Where(x => x.ID_HARGA == hrg.ID_HARGA).FirstOrDefault();
                        dc.Entry(hrg).State = EntityState.Modified;
                        dc.Entry(hrg).Property("CREATED_DATE").IsModified = false;
                        dc.SaveChanges();

                        //audit trail edit

                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                        audit.NAMA_TABEL = "TBL_HARGA";
                        audit.AKSI = "EDIT";
                        audit.userid = Session["ID_MEMBER"].ToString();
                        audit.NAMA_FIELD = "ID_KENDARAAN" + ", " + "ID_KENDARAAN" + ", " + "DURASI" + ", " + "HARGA";
                        audit.VALUE_LAMA = hrg.ID_HARGA + ", " + oke.ID_KOTA + ", " + oke.ID_KENDARAAN + ", " + oke.DURASI + ", " + oke.HARGA;
                        audit.VALUE_BARU = hrg.ID_HARGA + ", " + hrg.ID_KOTA + ", " + hrg.ID_KENDARAAN + ", " + hrg.DURASI + ", " + hrg.HARGA;
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();

                        transactions.Commit();
                        return Json(new { success = true, message = "Updated Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    return Json(new { success = true, message = msgErr.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            TBL_HARGA harga = dc.TBL_HARGA.Where(x => x.ID_HARGA == id).FirstOrDefault<TBL_HARGA>();
            dc.TBL_HARGA.Remove(harga);
            dc.SaveChanges();
            return Json(new { success = true, message = "Deleted Successfully" }, JsonRequestBehavior.AllowGet);
        }
    }

    public class HargaOutput
    {
        public int ID_HARGA { get; set; }
        public int ID_KENDARAAN { get; set; }
        public string NAMA_KENDARAAN { get; set; }
        public int ID_KOTA { get; set; }
        public string NAMA_KOTA { get; set; }
        public int ID_PROVINSI { get; set; }
        public string NAMA_PROVINSI{ get; set; }
        public int DURASI { get; set; }
        public int HARGA { get; set; }
    }
}
