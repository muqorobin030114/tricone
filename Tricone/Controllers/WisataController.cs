﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class wisataController : Controller
    {
        // GET: wisata
        DB_TRICONEEntitie dc = new DB_TRICONEEntitie();

        public ActionResult Index()
        {
            return View(dc.TBL_WISATA.ToList());
        }


        [HttpGet]
        public JsonResult Getdata()
        {
            List<TBL_WISATA> wisatalist = dc.TBL_WISATA.ToList<TBL_WISATA>();
            List<wisataOutput> result = new List<wisataOutput>();
            foreach (var item in wisatalist)
            {
                result.Add(new wisataOutput
                {
                    ID_PROVINSI = item.ID_PROVINSI,
                    NAMA_PROVINSI = item.MS_PROVINSI.NAMA_PROVINSI,
                    ID_KOTA = item.ID_KOTA,
                    NAMA_KOTA = item.MS_KOTA.NAMA_KOTA,
                    ID_WISATA = item.ID_WISATA,
                    NAMA_WISATA = item.NAMA_WISATA,
                    DESKRIPSI = item.DESKRIPSI,
                    FOTO_DERSKRIPSI = item.FOTO_DERSKRIPSI
                });
            }

            //var json = Newtonsoft.Json.JsonConvert.SerializeObject(result);
            return Json(new { data = result.OrderByDescending(e => e.ID_KOTA) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddorEdit(int id = 0)
        {
            TBL_WISATA wisatamodel = new TBL_WISATA();
            wisatamodel.provcollection = dc.MS_PROVINSI.ToList<MS_PROVINSI>();
            wisatamodel.kotacollection = dc.MS_KOTA.ToList<MS_KOTA>();
            if (id == 0)
            {
                //return View(new MS_KOTA());
                return View(wisatamodel);
            }
            else
            {
                wisatamodel = dc.TBL_WISATA.Where(x => x.ID_WISATA == id).FirstOrDefault();
                wisatamodel.provcollection = dc.MS_PROVINSI.ToList<MS_PROVINSI>();
                wisatamodel.kotacollection = dc.MS_KOTA.ToList<MS_KOTA>();
                //return View(dc.MS_KOTA.Where(x => x.ID_KOTA == id).FirstOrDefault<MS_KOTA>());
                return View(wisatamodel);
            }
        }

        [HttpPost]
        public ActionResult AddorEdit(TBL_WISATA wisata, HttpPostedFileBase filebase)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    wisata.UPDATE_DATE = DateTime.Now;
                    if (wisata.ID_WISATA == 0)
                    {
                        string fileName = Path.GetFileNameWithoutExtension(filebase.FileName);
                        string extension = Path.GetExtension(filebase.FileName);
                        fileName = fileName + DateTime.Now.ToString("yyyyMMddHHmmssfff") + extension;
                        wisata.FOTO_DERSKRIPSI = fileName;
                        fileName = Path.Combine(Server.MapPath("~/images/"), fileName);
                        filebase.SaveAs(fileName);
                        wisata.CREATED_DATE = DateTime.Now;
                        if (filebase.ContentLength <= 1000000)
                        {
                            dc.TBL_WISATA.Add(wisata);
                            if (dc.SaveChanges() > 0)
                            {
                                filebase.SaveAs(fileName);

                                //audit trail
                                var wisat = dc.TBL_WISATA.OrderByDescending(e => e.ID_WISATA).FirstOrDefault();
                                TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                                audit.NAMA_TABEL = "TBL_WISATA";
                                audit.AKSI = "ADD";
                                audit.NAMA_FIELD = "All Field";
                                audit.VALUE_LAMA = "";
                                audit.userid = Session["ID_MEMBER"].ToString();
                                audit.VALUE_BARU = wisat.ID_PROVINSI + ", " + wisat.ID_KOTA + ", " + wisat.ID_WISATA + ", " + wisat.NAMA_WISATA + ", " + wisat.DESKRIPSI + ", " + wisat.FOTO_DERSKRIPSI;
                                audit.CREATED_DATE = DateTime.Now;
                                audit.MODIFIED_DATE = DateTime.Now;
                                dc.TBL_AUDIT_TRAIL.Add(audit);
                                dc.SaveChanges();

                                transactions.Commit();
                                return Json(new { success = true, message = "Saved Successfully" }, JsonRequestBehavior.AllowGet);
                            }
                        
                            else
                            {
                                return Json(new { success = true, message = "File Size must be Equal or less than 1mb" }, JsonRequestBehavior.AllowGet);
                                //ViewBag.msg = "File Size must be Equal or less than 1mb";
                            }
                        }
                        else
                        {
                            return Json(new { success = true, message = "Please Try Again With Short Filename" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        var qaudit = dc.TBL_WISATA.AsNoTracking().Where(x => x.ID_WISATA == wisata.ID_WISATA).FirstOrDefault();
                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();

                        if (filebase != null)
                        {
                            //delete foto lama
                            string fullPath = Request.MapPath("~/images/" + wisata.FOTO_DERSKRIPSI);
                            if (System.IO.File.Exists(fullPath))
                            {
                                System.IO.File.Delete(fullPath);
                            }

                            //penambahan foto baru
                            string fileName = Path.GetFileNameWithoutExtension(filebase.FileName);
                            string extension = Path.GetExtension(filebase.FileName);
                            fileName = fileName + DateTime.Now.ToString("yyyyMMddHHmmssfff") + extension;
                            //if (kendaraan.FOTO_PROMO==null) { kendaraan.FOTO_PROMO = fileName; }
                            wisata.FOTO_DERSKRIPSI = fileName;
                            fileName = Path.Combine(Server.MapPath("~/images/"), fileName);
                            filebase.SaveAs(fileName);

                            audit.VALUE_LAMA = qaudit.ID_PROVINSI + ", " + qaudit.ID_KOTA + ", " + qaudit.ID_WISATA + ", " + qaudit.NAMA_WISATA + ", " + qaudit.DESKRIPSI + ", " + qaudit.FOTO_DERSKRIPSI;
                            audit.VALUE_BARU = wisata.ID_PROVINSI + ", " + wisata.ID_KOTA + ", " + wisata.ID_WISATA + ", " + wisata.NAMA_WISATA + ", " + wisata.DESKRIPSI + ", " + wisata.FOTO_DERSKRIPSI;
                        }

                        dc.Entry(wisata).State = EntityState.Modified;
                        if (filebase == null)
                        {
                            dc.Entry(wisata).Property("FOTO_DERSKRIPSI").IsModified = false;

                            audit.VALUE_LAMA = qaudit.ID_PROVINSI + ", " + qaudit.ID_KOTA + ", " + qaudit.ID_WISATA + ", " + qaudit.NAMA_WISATA + ", " + qaudit.DESKRIPSI + ", " + qaudit.FOTO_DERSKRIPSI;
                            audit.VALUE_BARU = wisata.ID_PROVINSI + ", " + wisata.ID_KOTA + ", " + wisata.ID_WISATA + ", " + wisata.NAMA_WISATA + ", " + wisata.DESKRIPSI + ", " + wisata.FOTO_DERSKRIPSI;
                        }
                        dc.Entry(wisata).Property("CREATED_DATE").IsModified = false;
                        dc.SaveChanges();

                        //audit trail edit
                        audit.NAMA_TABEL = "TBL_PROMO";
                        audit.AKSI = "EDIT";
                        audit.userid = Session["ID_MEMBER"].ToString();
                        audit.NAMA_FIELD = "";
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();

                        transactions.Commit();
                        return Json(new { success = true, message = "Updated Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Convert.ToInt32(Session["ID_MEMBER"])> 0)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    return Json(new { success = true, message = msgErr.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            TBL_WISATA wisata = dc.TBL_WISATA.Where(x => x.ID_WISATA == id).FirstOrDefault<TBL_WISATA>();
            dc.TBL_WISATA.Remove(wisata);
            dc.SaveChanges();
            return Json(new { success = true, message = "Deleted Successfully" }, JsonRequestBehavior.AllowGet);
        }
    }

    public class wisataOutput
    {
        public int ID_WISATA { get; set; }
        public string NAMA_WISATA { get; set; }
        [AllowHtml]
        public string DESKRIPSI { get; set; }
        public string FOTO_DERSKRIPSI { get; set; }
        public Nullable<int> ID_PROVINSI { get; set; }
        public string NAMA_PROVINSI { get; set; }
        public Nullable<int> ID_KOTA { get; set; }
        public string NAMA_KOTA { get; set; }
        public Nullable<System.DateTime> CREATED_DATE { get; set; }
        public Nullable<System.DateTime> UPDATE_DATE { get; set; }
    }
}
