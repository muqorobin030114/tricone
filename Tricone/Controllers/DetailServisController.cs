﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class DetailServisController : Controller
    {
        // GET: DetailServis
        DB_TRICONEEntitie db = new DB_TRICONEEntitie();
        public ActionResult Mudik()
        {
            List<TBL_DETAILSERVIS> servisList = db.TBL_DETAILSERVIS.Where(x => x.ID_DETAILSERVIS == 1).ToList();
            ViewData["detailservis"] = servisList;
            return View();
        }
        public ActionResult Travel()
        {
            List<TBL_DETAILSERVIS> servisList = db.TBL_DETAILSERVIS.Where(x => x.ID_DETAILSERVIS == 2).ToList();
            ViewData["detailservis"] = servisList;
            return View();
        }
        public ActionResult Bandara()
        {
            List<TBL_DETAILSERVIS> servisList = db.TBL_DETAILSERVIS.Where(x => x.ID_DETAILSERVIS == 3).ToList();
            ViewData["detailservis"] = servisList;
            return View();
        }
        public ActionResult AntarJemput()
        {
            List<TBL_DETAILSERVIS> servisList = db.TBL_DETAILSERVIS.Where(x => x.ID_DETAILSERVIS == 4).ToList();
            ViewData["detailservis"] = servisList;
            return View();
        }
        public ActionResult Mice()
        {
            List<TBL_DETAILSERVIS> servisList = db.TBL_DETAILSERVIS.Where(x => x.ID_DETAILSERVIS == 5).ToList();
            ViewData["detailservis"] = servisList;
            return View();
        }
        public ActionResult Pengajian()
        {
            List<TBL_DETAILSERVIS> servisList = db.TBL_DETAILSERVIS.Where(x => x.ID_DETAILSERVIS == 6).ToList();
            ViewData["detailservis"] = servisList;
            return View();
        }
    }
}