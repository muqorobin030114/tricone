﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class FAQUserController : Controller
    {
        // GET: FAQUser
        DB_TRICONEEntitie db = new DB_TRICONEEntitie();
        public ActionResult Index()
        {
            List<TBL_FAQ> FAQList = db.TBL_FAQ.ToList();
            ViewData["detailFAQ"] = FAQList;
            return View();
        }
    }
}