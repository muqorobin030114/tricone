﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class KuisionerController : Controller
    {
        // GET: Kuisioner
        DB_TRICONEEntitie dc = new DB_TRICONEEntitie();

        public ActionResult Index()
        {
            return View(dc.MS_KUISIONER.ToList());
        }

        [HttpGet]
        public JsonResult Getdata()
        {
            List<MS_KUISIONER> kuisList = dc.MS_KUISIONER.ToList<MS_KUISIONER>();
            List<kuisionerOutput> result = new List<kuisionerOutput>();
            foreach (var item in kuisList)
            {
                result.Add(new kuisionerOutput
                {
                    ID_KUISIONER = item.ID_KUISIONER,
                    KUISIONER = item.KUISIONER
                });
            }

            //var json = Newtonsoft.Json.JsonConvert.SerializeObject(result);
            return Json(new { data = result.OrderByDescending(e => e.ID_KUISIONER) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddorEdit(int id = 0)
        {
            if (id == 0)
            {
                return View(new MS_KUISIONER());
            }
            else
            {
                return View(dc.MS_KUISIONER.Where(x => x.ID_KUISIONER == id).FirstOrDefault<MS_KUISIONER>());
            }
        }

        [HttpPost]
        public ActionResult AddorEdit(MS_KUISIONER kuis)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    kuis.MODIFIED_DATE = DateTime.Now;
                    if(kuis.ID_KUISIONER==0)
                    {
                        kuis.CREATED_DATE = DateTime.Now;
                        dc.MS_KUISIONER.Add(kuis);
                        dc.SaveChanges();

                        //audit trail
                        var kuisi = dc.MS_KUISIONER.OrderByDescending(e => e.ID_KUISIONER).FirstOrDefault();
                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                        audit.NAMA_TABEL = "MS_KUISIONER";
                        audit.AKSI = "ADD";
                        audit.NAMA_FIELD = "All Field";
                        audit.VALUE_LAMA = "";
                        audit.userid = Session["ID_KUISIONER"].ToString();
                        audit.VALUE_BARU = kuisi.ID_KUISIONER + ", " + kuisi.KUISIONER;
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();

                        transactions.Commit();
                        return Json(new { success = true, message = "Saved Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var oke = dc.MS_KUISIONER.AsNoTracking().Where(x => x.ID_KUISIONER == kuis.ID_KUISIONER).FirstOrDefault();
                        dc.Entry(kuis).State = EntityState.Modified;
                        dc.Entry(kuis).Property("CREATED_DATE").IsModified = false;
                        dc.SaveChanges();

                        //audit trail edit

                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                        audit.NAMA_TABEL = "MS_KUISIONER";
                        audit.AKSI = "EDIT";
                        audit.userid = Session["ID_MEMBER"].ToString();
                        audit.NAMA_FIELD = "KUISIONER";
                        audit.VALUE_LAMA = kuis.ID_KUISIONER + ", " + oke.KUISIONER;
                        audit.VALUE_BARU = kuis.ID_KUISIONER + ", " + kuis.KUISIONER;
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();

                        transactions.Commit();
                        return Json(new { success = true, message = "Updated Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    return Json(new { success = true, message = msgErr.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            MS_KUISIONER kuisioner = dc.MS_KUISIONER.Where(x => x.ID_KUISIONER == id).FirstOrDefault<MS_KUISIONER>();
            dc.MS_KUISIONER.Remove(kuisioner);
            dc.SaveChanges();
            return Json(new { success = true, message = "Deleted Successfully" }, JsonRequestBehavior.AllowGet);
        }
    }
    public class kuisionerOutput
    {
        public int ID_KUISIONER { get; set; }
        public string KUISIONER { get; set; }
    }
}
