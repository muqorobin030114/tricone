﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class ScheduleUserController : Controller
    {
        DB_TRICONEEntitie dc = new DB_TRICONEEntitie();

        // GET: ScheduleUser
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult Getdata()
        {
            int id = Convert.ToInt32(Session["ID_MEMBER"]);

            List<v_transaksi> transaksiList = dc.v_transaksi.ToList<v_transaksi>();
            List<transaksiMemberOutput> result = new List<transaksiMemberOutput>();
            foreach (var item in transaksiList)
            {
                if (item.ID_MEMBER== id && Convert.ToInt32(item.APPROVAL) > 0) { 
                    result.Add(new transaksiMemberOutput
                    {
                        ID_TRANSAKSI = item.ID_TRANSAKSI,
                        TGL_PERGI = Convert.ToDateTime(item.TGL_PERGI),
                        TGL_PULANG = Convert.ToDateTime(item.TGL_PULANG),
                        NAMA_KOTA = item.NAMA_KOTA,
                        FOTO_PEMBAYARAN = item.FOTO_PEMBAYARAN,
                        STATUS = Convert.ToInt32(item.STATUS_TRANSAKSI),
                        APPROVAL = Convert.ToInt32(item.APPROVAL)
                    });
                }
            }
            return Json(new { data = result.OrderByDescending(e => e.ID_TRANSAKSI) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Rekening(int id)
        {
            ViewBag.id_trans = id;
            var trx = dc.TBL_TRANSAKSI.Where(x => x.ID_TRANSAKSI == id).FirstOrDefault();
            ViewBag.total = trx.TOTAL_TRANSAKSI;
            TBL_PEMBAYARAN bayarmodel = new TBL_PEMBAYARAN();
            var cekrecord = dc.TBL_PEMBAYARAN.Where(x => x.ID_TRANSAKSI == id).Count();
            bayarmodel.bankcollection = dc.MS_BANK.ToList<MS_BANK>();
            if (cekrecord == 0)
            {
                bayarmodel.JUMLAH_BAYAR = trx.TOTAL_TRANSAKSI;
                return View(bayarmodel);
            }
            else
            {
                bayarmodel = dc.TBL_PEMBAYARAN.Where(x => x.ID_TRANSAKSI == id).FirstOrDefault();
                bayarmodel.bankcollection = dc.MS_BANK.ToList<MS_BANK>();
                bayarmodel.JUMLAH_BAYAR = trx.TOTAL_TRANSAKSI;
                return View(bayarmodel);
            }
        }

        [HttpPost]
        public ActionResult Payment(TBL_PEMBAYARAN bayar, FormCollection fcol)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    var id_transaksi = Convert.ToInt32(fcol["id_trx"]);
                    var countpembayaran = dc.TBL_PEMBAYARAN.Where(a => a.ID_TRANSAKSI == id_transaksi).Count();
                    if (countpembayaran > 0)
                    {
                        var oke = dc.TBL_PEMBAYARAN.AsNoTracking().Where(x => x.ID_TRANSAKSI == id_transaksi).FirstOrDefault();
                        var pembayaran = dc.TBL_PEMBAYARAN.Where(a => a.ID_TRANSAKSI == id_transaksi).FirstOrDefault();
                        pembayaran.ID_BANK = bayar.ID_BANK;
                        pembayaran.NAMA_REKENING = bayar.NAMA_REKENING;
                        pembayaran.JUMLAH_BAYAR = bayar.JUMLAH_BAYAR;
                        pembayaran.NOMOR_REKENING = bayar.NOMOR_REKENING;
                        pembayaran.MODIFIED_DATE = DateTime.Now;

                        dc.Entry(pembayaran).State = EntityState.Modified;
                        dc.Entry(pembayaran).Property("ID_TRANSAKSI").IsModified = false;
                        dc.Entry(pembayaran).Property("CREATED_DATE").IsModified = false;
                        dc.SaveChanges();

                        //audit trail edit
                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                        audit.NAMA_TABEL = "TBL_PEMBAYARAN";
                        audit.AKSI = "EDIT PAYMENT METHOD";
                        audit.userid = Session["ID_MEMBER"].ToString();
                        audit.NAMA_FIELD = "";
                        audit.VALUE_LAMA = oke.ID_PEMBAYARAN + ", " + oke.ID_TRANSAKSI + ", " + oke.ID_BANK + ", " + oke.JUMLAH_BAYAR;
                        audit.VALUE_BARU = oke.ID_PEMBAYARAN + ", " + oke.ID_TRANSAKSI + ", " + pembayaran.ID_BANK + ", " + pembayaran.JUMLAH_BAYAR;
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();
                    }
                    else
                    {
                        TBL_PEMBAYARAN bayarmodel = new TBL_PEMBAYARAN();
                        bayarmodel.ID_BANK = bayar.ID_BANK;
                        bayarmodel.NAMA_REKENING = bayar.NAMA_REKENING;
                        bayarmodel.JUMLAH_BAYAR = bayar.JUMLAH_BAYAR;
                        bayarmodel.NOMOR_REKENING = bayar.NOMOR_REKENING;
                        bayarmodel.CREATED_DATE = DateTime.Now;
                        bayarmodel.MODIFIED_DATE = DateTime.Now;
                        bayarmodel.ID_TRANSAKSI = id_transaksi;

                        dc.Entry(bayarmodel).State = EntityState.Added;
                        dc.SaveChanges();

                        //audit trail
                        var qaudit = dc.TBL_PEMBAYARAN.Where(a => a.ID_TRANSAKSI == id_transaksi).OrderByDescending(e => e.ID_PEMBAYARAN).FirstOrDefault();
                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                        audit.NAMA_TABEL = "TBL_PEMBAYARAN";
                        audit.AKSI = "ADD PAYMENT METHOD";
                        audit.NAMA_FIELD = "All Field";
                        audit.VALUE_LAMA = "";
                        audit.userid = Session["ID_MEMBER"].ToString();
                        audit.VALUE_BARU = qaudit.ID_PEMBAYARAN + ", " + bayarmodel.ID_TRANSAKSI + ", " + bayarmodel.ID_BANK + ", " + bayarmodel.JUMLAH_BAYAR;
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();
                    }
                    transactions.Commit();
                    return RedirectToAction("Index", "ScheduleUser");
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    ViewBag.Error = msgErr.Message;
                }
            }
            return null;
        }

        [HttpGet]
        public ActionResult Pembayaran(int id)
        {
            var id_trx = id;
            TBL_TRANSAKSI trxmodel = new TBL_TRANSAKSI();
            
            var trxfoto = dc.TBL_TRANSAKSI.Find(id);
            Session["imgPath"] = trxmodel.FOTO_PEMBAYARAN;
            if (trxfoto == null)
            {
                return HttpNotFound();
            }
            var trxs = dc.TBL_TRANSAKSI.Where(x => x.ID_TRANSAKSI == id).FirstOrDefault();
            ViewBag.foto = trxs.FOTO_PEMBAYARAN;
            ViewBag.id_trans = id;
            return View(trxmodel);
        }

        [HttpPost]
        public ActionResult Uploadphoto(FormCollection trx, HttpPostedFileBase filebase)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    int id_trns = Convert.ToInt32(trx["id_trans"]);
                    var fotolama = trx["fotolama"];
                    var trxs = dc.TBL_TRANSAKSI.Where(x => x.ID_TRANSAKSI == id_trns).FirstOrDefault();
                    var oke = dc.TBL_TRANSAKSI.AsNoTracking().Where(x => x.ID_TRANSAKSI == id_trns).FirstOrDefault();
                    if (filebase != null)
                    {
                        //delete foto lama
                        string fullPath = Request.MapPath("~/images/" + fotolama);
                        if (System.IO.File.Exists(fullPath))
                        {
                            System.IO.File.Delete(fullPath);
                        }

                        //penambahan foto baru

                        string fileName = Path.GetFileNameWithoutExtension(filebase.FileName);
                        string extension = Path.GetExtension(filebase.FileName);
                        fileName = fileName + DateTime.Now.ToString("yyyyMMddHHmmssfff") + extension;
                        //if (kendaraan.pathFoto==null) { kendaraan.pathFoto = fileName; }
                        trxs.FOTO_PEMBAYARAN = fileName;
                        fileName = Path.Combine(Server.MapPath("~/images/"), fileName);
                        filebase.SaveAs(fileName);
                        trxs.MODIFIED_DATE = DateTime.Now;
                        trxs.APPROVAL = 2;
                        dc.Entry(trxs).State = EntityState.Modified;
                        dc.Entry(trxs).Property("FOTO_PEMBAYARAN").IsModified = true;
                        dc.Entry(trxs).Property("MODIFIED_DATE").IsModified = true;
                        dc.Entry(trxs).Property("APPROVAL").IsModified = true;
                        dc.SaveChanges();

                        //audit trail edit
                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                        audit.NAMA_TABEL = "TBL_TRANSAKSI";
                        audit.AKSI = "UPLOAD PAYMENT TRANSACTION";
                        audit.userid = Session["ID_MEMBER"].ToString();
                        audit.NAMA_FIELD = "";
                        audit.VALUE_LAMA = oke.ID_TRANSAKSI + ", " + oke.FOTO_PEMBAYARAN;
                        audit.VALUE_BARU = trxs.ID_TRANSAKSI + ", " + trxs.FOTO_PEMBAYARAN;
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();

                        //buat notifikasi
                        notifikasi noti = new notifikasi();
                        noti.NAMA_NOTIF = "transaksi";
                        noti.NOTIF_FOR = "admin";
                        dc.Entry(noti).State = EntityState.Added;
                        dc.SaveChanges();
                        transactions.Commit();
                        return Json(new { success = true, message = "Updated Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        transactions.Commit();
                        return Json(new { success = true, message = "Please Try Again" }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception msgErr)
                {
                    transactions.Rollback();
                    ViewBag.Error = msgErr.Message;
                }
            }
            return null;
        }
        
        [HttpGet]
        public ActionResult detailbayar(int id)
        {
            ViewBag.id_trans = id;
            return View();
        }

        [HttpGet]
        public JsonResult GetdataDetail(int id_trans)
        {
            var a = id_trans;
            List<v_detail_pesan> detailtransaksiList = dc.v_detail_pesan.ToList<v_detail_pesan>();
            List<detailtransaksiOutput> result = new List<detailtransaksiOutput>();
            foreach (var item in detailtransaksiList)
            {
                if (id_trans == item.id_transaksi)
                {
                    result.Add(new detailtransaksiOutput
                    {
                        ID_TRANSAKSI = Convert.ToInt32(item.id_transaksi),
                        id_pemesanan = Convert.ToInt32(item.id_pemesanan),
                        id_member = Convert.ToInt32(item.id_member),
                        nama_member = item.nama_member,
                        telpon_member = item.telpon_member,
                        email_member = item.email_member,
                        id_harga = Convert.ToInt32(item.id_harga),
                        harga = Convert.ToInt32(item.harga),
                        id_kendaraan = Convert.ToInt32(item.id_kendaraan),
                        id_jenis_kendaraan = Convert.ToInt32(item.id_jenis_kendaraan),
                        nama_kendaraan = item.nama_kendaraan,
                        jumlah_seat = Convert.ToInt32(item.jumlah_seat),
                        jumlah_kendaraan = Convert.ToInt32(item.jumlah_kendaraan),
                        status_kendaraan = Convert.ToInt32(item.status_kendaraan),
                        durasi_jam = Convert.ToInt32(item.durasi_jam),
                        id_harilibur = Convert.ToInt32(item.id_harilibur),
                        nama_harilibur = item.nama_harilibur,
                        startdate_libur = Convert.ToDateTime(item.startdate_libur),
                        enddate_libur = Convert.ToDateTime(item.enddate_libur),
                        persen_libur = Convert.ToInt32(item.persen_libur),
                        tgl_pergi = Convert.ToDateTime(item.tgl_pergi),
                        tgl_pulang = Convert.ToDateTime(item.tgl_pulang),
                        id_kota = Convert.ToInt32(item.id_kota),
                        nama_kota = item.nama_kota,
                        id_provinsi = Convert.ToInt32(item.id_provinsi),
                        nama_provinsi = item.nama_provinsi,
                        quantity_kendaraan = Convert.ToInt32(item.quantity_kendaraan),
                        total_pembayaran = Convert.ToInt32(item.total_pembayaran),
                        status_detail_pesan = Convert.ToInt32(item.status_detail_pesan),
                        id_supir = Convert.ToInt32(item.id_supir),
                        nama_supir = item.nama_supir,
                        telpon_supir = item.telpon_supir,
                        APPROVAL = item.APPROVAL,
                        STATUS_TRANSAKSI = item.STATUS_TRANSAKSI
                    });
                }
            }

            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }

        //[HttpGet]
        //public ActionResult printtiket(int id)
        //{
        //    ViewBag.id_trans = id;
        //    return View();
        //}
    }
}