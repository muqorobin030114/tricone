﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class PemesananController : Controller
    {
        DB_TRICONEEntitie db = new DB_TRICONEEntitie();
        // GET: Pemesanan
        public ActionResult Index()
        {
            ViewBag.Message = "pesan";
            /*List<MS_KENDARAAN> kendaraanList = db.MS_KENDARAAN.ToList();
            List<MS_JENIS_KENDARAAN> jkendaraanList = db.MS_JENIS_KENDARAAN.ToList();

            ViewData["joinkendaraan"] = from j in jkendaraanList
                                        join k in kendaraanList on j.ID_JENIS_KENDARAAN equals k.ID_JENIS_KENDARAAN into table1
                                        from k in table1.DefaultIfEmpty()
                                        select new JoinKendaraanClass { jkendaraanList = j, kendaraanList = k };
            return View(ViewData["joinkendaraan"]);*/
            var startdate = DateTime.Now;
            var enddate = DateTime.Now;
            var id_kota = 1;
            TR_DETAIL_PEMESANAN pesanmodel = new TR_DETAIL_PEMESANAN();
            pesanmodel.kotacollection = db.MS_KOTA.ToList<MS_KOTA>();
            List<daftarkendaraanbytanggal_Result> daftar = db.daftarkendaraanbytanggal(startdate, enddate, id_kota).ToList();
            List<v_kendaraan> kendaraanList = db.v_kendaraan.ToList();
            ViewBag.startdate = "";
            ViewBag.enddate = "";
            ViewData["kndraan"] = daftar;
            return View(pesanmodel);
        }

        [HttpPost]
        public ActionResult Index(DateTime startdate,DateTime enddate, int id_kota)
        {
            List<TR_DETAIL_PEMESANAN> pesan = db.TR_DETAIL_PEMESANAN.ToList();
            List<TBL_HARGA> harga = db.TBL_HARGA.ToList();
            List<v_kendaraan> kend2 = db.v_kendaraan.ToList();
            List<v_kendaraan> kendaraanList = db.v_kendaraan.ToList();
            TR_DETAIL_PEMESANAN pesanmodel = new TR_DETAIL_PEMESANAN();
            pesanmodel.kotacollection = db.MS_KOTA.ToList<MS_KOTA>();
            List<daftarkendaraanbytanggal_Result> daftar = db.daftarkendaraanbytanggal(startdate, enddate, id_kota).ToList();
            ViewBag.startdate = startdate;
            ViewBag.enddate = enddate;
            ViewBag.id_kota = id_kota;
            ViewData["kndraan"] = daftar;
            return View(pesanmodel);
        }
        [HttpGet]
        public ActionResult DetailProduk(int id)
        {
            /*List<v_kendaraan> kendaraanList = db.v_kendaraan.ToList();
            List<v_kota> kotaList = db.v_kota.ToList();
            List<TBL_HARGA> hargaList = db.v_kota.ToList();

            ViewData["joinharga"] = from j in kendaraanList
                                    join k in kendaraanList on j.ID_JENIS_KENDARAAN equals k.ID_JENIS_KENDARAAN into table1
                                        from k in table1.DefaultIfEmpty()
                                        select new JoinKendaraanClass { jkendaraanList = j, kendaraanList = k };
            return View(ViewData["joinkendaraan"]);*/
            return View(db.v_kendaraan.Where(x => x.ID_KENDARAAN == id).FirstOrDefault());
        }

        [HttpGet]
        public ActionResult checkout(int id)
        {

            return View();
        }

        [HttpPost]
        public ActionResult AddtoCart(TR_DETAIL_PEMESANAN pesani, DateTime startdate, DateTime enddate, int id_member, int id_harga, int id_kota, int id_kendaraan)
        {
            using (var transactions = db.Database.BeginTransaction())
            {
                try
                {
                if (id_member != 0)
                    {
                        var cektransaksi = db.TBL_TRANSAKSI.Where(x => x.STATUS == 1 && x.APPROVAL== 0 && x.ID_MEMBER == id_member && EntityFunctions.TruncateTime(x.TGL_PERGI) == startdate.Date && EntityFunctions.TruncateTime(x.TGL_PULANG) == enddate.Date && x.ID_KOTA == id_kota).FirstOrDefault();
                        var harganya = db.TBL_HARGA.Where(x => x.ID_HARGA == id_harga).FirstOrDefault();
                        if (cektransaksi == null)
                        {
                            var cekadatransaksi = db.TBL_TRANSAKSI.Where(x => x.STATUS == 1 && x.ID_MEMBER == id_member && EntityFunctions.TruncateTime(x.TGL_PERGI) == startdate.Date && EntityFunctions.TruncateTime(x.TGL_PULANG) == enddate.Date && x.ID_KOTA == id_kota).Count();
                            if (cekadatransaksi > 0)
                            {
                                transactions.Commit();
                                return RedirectToAction("Index");
                            }
                            else { 
                                //table transaksi
                                TBL_TRANSAKSI trx = new TBL_TRANSAKSI();

                                //total durasi
                                DateTime dt1 = DateTime.Parse(Convert.ToDateTime(enddate).AddDays(35).ToString());
                                DateTime dt2 = DateTime.Parse(Convert.ToDateTime(startdate).AddDays(35).ToString());
                                TimeSpan ts = dt1.Subtract(dt2);
                                trx.TOTAL_DURASI = ts.Days + 1;

                                //kalkulasi harilibur
                                Decimal total_harga = 0;
                                Decimal total_persen = 0;
                                for (DateTime date = startdate; date.Date <= enddate.Date; date = date.AddDays(1))
                                {
                                    var cekharganaik = db.MS_HARILIBUR.Where(x => EntityFunctions.TruncateTime(x.STARTDATE) <= date && EntityFunctions.TruncateTime(x.ENDDATE) >= date).FirstOrDefault();
                                    Decimal jumlahpersen = 0;
                                    if (cekharganaik != null)
                                    {
                                        Decimal persen = Convert.ToInt32(cekharganaik.PERSEN_LIBUR);
                                        jumlahpersen = Convert.ToDecimal(harganya.HARGA) * (persen/100);
                                        total_persen += jumlahpersen;
                                        pesani.ID_HARILIBUR = cekharganaik.ID_HARILIBUR;
                                    }
                                    total_harga += Convert.ToDecimal(harganya.HARGA) + jumlahpersen;
                                }
                                pesani.HARGA_LIBUR = Convert.ToInt32(total_persen);

                                trx.TOTAL_TRANSAKSI = Convert.ToInt32(total_harga);
                                trx.TGL_PERGI = startdate;
                                trx.TGL_PULANG = enddate;
                                //trx.TOTAL_TRANSAKSI = harganya.HARGA * trx.TOTAL_DURASI;
                                trx.STATUS = 1;
                                trx.APPROVAL = 0;
                                trx.ID_MEMBER = id_member;
                                trx.MODIFIED_DATE = DateTime.Now;
                                trx.CREATED_DATE = DateTime.Now;
                                trx.ID_KOTA = id_kota;
                                db.TBL_TRANSAKSI.Add(trx);
                                db.SaveChanges();

                                //table detail transaksi
                                //cari id transaksi
                                var cari_idtransaksi = db.TBL_TRANSAKSI.Where(x => x.STATUS == 1 && x.ID_MEMBER == id_member && EntityFunctions.TruncateTime(x.TGL_PERGI) == startdate.Date && EntityFunctions.TruncateTime(x.TGL_PULANG) == enddate.Date && x.ID_KOTA == id_kota).FirstOrDefault();
                                pesani.ID_TRANSAKSI = cari_idtransaksi.ID_TRANSAKSI;

                                pesani.MODIFIED_DATE = DateTime.Now;
                                pesani.CREATED_DATE = DateTime.Now;
                                pesani.STATUS = 1;
                                pesani.ID_KOTA = id_kota;
                                pesani.QUANTITY_KENDARAAN = 1;
                                pesani.TOTAL_PEMBAYARAN = trx.TOTAL_TRANSAKSI;
                                pesani.ID_MEMBER = id_member;
                                pesani.TGL_PERGI = startdate;
                                pesani.TGL_PULANG = enddate;
                                pesani.ID_HARGA = id_harga;
                                db.TR_DETAIL_PEMESANAN.Add(pesani);
                                db.SaveChanges();

                                //audit trail edit
                                TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                                audit.NAMA_TABEL = "TBL_TRANSAKSI,TR_DETAIL_PEMESANAN";
                                audit.AKSI = "ADD BOOKING";
                                audit.userid = Session["ID_MEMBER"].ToString();
                                audit.NAMA_FIELD = "";
                                audit.VALUE_LAMA = "";
                                audit.VALUE_BARU = pesani.ID_TRANSAKSI.ToString() + ", " + trx.TOTAL_TRANSAKSI;
                                audit.CREATED_DATE = DateTime.Now;
                                audit.MODIFIED_DATE = DateTime.Now;
                                db.TBL_AUDIT_TRAIL.Add(audit);
                                db.SaveChanges();
                            }
                        }
                        else
                        {
                            var cektadadetail = db.v_detail_pesan.Where(x => x.id_transaksi== cektransaksi.ID_TRANSAKSI && x.status_detail_pesan == 1 && x.id_member == id_member && x.id_kendaraan == id_kendaraan && EntityFunctions.TruncateTime(x.tgl_pergi) == startdate.Date && EntityFunctions.TruncateTime(x.tgl_pulang) == enddate.Date && x.id_kota == id_kota).FirstOrDefault();
                            if (cektadadetail == null)
                            {
                                pesani.ID_TRANSAKSI = cektransaksi.ID_TRANSAKSI;
                                pesani.MODIFIED_DATE = DateTime.Now;
                                pesani.CREATED_DATE = DateTime.Now;
                                pesani.ID_KOTA = id_kota;
                                pesani.STATUS = 1;
                                pesani.QUANTITY_KENDARAAN = 1;

                                //total durasi
                                DateTime dt1 = DateTime.Parse(Convert.ToDateTime(enddate).AddDays(35).ToString());
                                DateTime dt2 = DateTime.Parse(Convert.ToDateTime(startdate).AddDays(35).ToString());
                                TimeSpan ts = dt1.Subtract(dt2);
                                int jumlah_durasi = ts.Days + 1;

                                //kalkulasi harilibur
                                Decimal total_harga = 0;
                                Decimal total_persen = 0;
                                for (DateTime date = startdate; date.Date <= enddate.Date; date = date.AddDays(1))
                                {
                                    var cekharganaik = db.MS_HARILIBUR.Where(x => EntityFunctions.TruncateTime(x.STARTDATE) <= date && EntityFunctions.TruncateTime(x.ENDDATE) >= date).FirstOrDefault();
                                    Decimal jumlahpersen = 0;
                                    if (cekharganaik != null)
                                    {
                                        Decimal persen = Convert.ToInt32(cekharganaik.PERSEN_LIBUR);
                                        jumlahpersen = Convert.ToDecimal(harganya.HARGA) * (persen / 100);
                                        total_persen += jumlahpersen;
                                        pesani.ID_HARILIBUR = cekharganaik.ID_HARILIBUR;
                                    }
                                    total_harga += Convert.ToDecimal(harganya.HARGA) + jumlahpersen;
                                }
                                pesani.HARGA_LIBUR = Convert.ToInt32(total_persen);
                                pesani.TOTAL_PEMBAYARAN = Convert.ToInt32(total_harga);

                                //pesani.TOTAL_PEMBAYARAN = harganya.HARGA * jumlah_durasi;
                                pesani.ID_MEMBER = id_member;
                                pesani.TGL_PERGI = startdate;
                                pesani.TGL_PULANG = enddate;
                                pesani.ID_HARGA = id_harga;
                                db.TR_DETAIL_PEMESANAN.Add(pesani);
                                db.SaveChanges();

                                //audit trail edit
                                TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                                audit.NAMA_TABEL = "TBL_TRANSAKSI,TR_DETAIL_PEMESANAN";
                                audit.AKSI = "ADD OTHER CAR";
                                audit.userid = Session["ID_MEMBER"].ToString();
                                audit.NAMA_FIELD = "";
                                audit.VALUE_LAMA = "";
                                audit.VALUE_BARU = pesani.ID_TRANSAKSI.ToString() + ", " + total_harga;
                                audit.CREATED_DATE = DateTime.Now;
                                audit.MODIFIED_DATE = DateTime.Now;
                                db.TBL_AUDIT_TRAIL.Add(audit);
                                db.SaveChanges();
                            }
                            else
                            {
                                TR_DETAIL_PEMESANAN tambahmobil = db.TR_DETAIL_PEMESANAN.Where(x => x.ID_PEMESANAN == cektadadetail.id_pemesanan).FirstOrDefault();

                                //total durasi
                                DateTime dt1 = DateTime.Parse(Convert.ToDateTime(enddate).AddDays(35).ToString());
                                DateTime dt2 = DateTime.Parse(Convert.ToDateTime(startdate).AddDays(35).ToString());
                                TimeSpan ts = dt1.Subtract(dt2);
                                int jumlah_durasi = ts.Days + 1;

                                //kalkulasi harilibur
                                Decimal total_harga = 0;
                                Decimal total_persen = 0;
                                for (DateTime date = startdate; date.Date <= enddate.Date; date = date.AddDays(1))
                                {
                                    var cekharganaik = db.MS_HARILIBUR.Where(x => EntityFunctions.TruncateTime(x.STARTDATE) <= date && EntityFunctions.TruncateTime(x.ENDDATE) >= date).FirstOrDefault();
                                    Decimal jumlahpersen = 0;
                                    if (cekharganaik != null)
                                    {
                                        Decimal persen = Convert.ToInt32(cekharganaik.PERSEN_LIBUR);
                                        jumlahpersen = Convert.ToDecimal(harganya.HARGA) * (persen / 100);
                                        total_persen += jumlahpersen;
                                        tambahmobil.ID_HARILIBUR = cekharganaik.ID_HARILIBUR;
                                    }
                                    total_harga += Convert.ToDecimal(harganya.HARGA) + jumlahpersen;
                                }
                                tambahmobil.HARGA_LIBUR = Convert.ToInt32(total_persen) + cektadadetail.HARGA_LIBUR;
                                tambahmobil.TOTAL_PEMBAYARAN = Convert.ToInt32(total_harga) + cektadadetail.total_pembayaran;

                                tambahmobil.QUANTITY_KENDARAAN = cektadadetail.quantity_kendaraan + 1;
                                //tambahmobil.TOTAL_PEMBAYARAN = (harganya.HARGA * jumlah_durasi) + cektadadetail.total_pembayaran;
                                tambahmobil.MODIFIED_DATE = DateTime.Now;

                                db.Entry(tambahmobil).State = EntityState.Modified;
                                db.Entry(tambahmobil).Property("QUANTITY_KENDARAAN").IsModified = true;
                                db.Entry(tambahmobil).Property("HARGA_LIBUR").IsModified = true;
                                db.Entry(tambahmobil).Property("TOTAL_PEMBAYARAN").IsModified = true;
                                db.Entry(tambahmobil).Property("MODIFIED_DATE").IsModified = true;
                                db.SaveChanges();

                                //audit trail edit
                                TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                                audit.NAMA_TABEL = "TBL_TRANSAKSI,TR_DETAIL_PEMESANAN";
                                audit.AKSI = "ADD INCREASE CAR";
                                audit.userid = Session["ID_MEMBER"].ToString();
                                audit.NAMA_FIELD = "";
                                audit.VALUE_LAMA = "";
                                audit.VALUE_BARU = cektadadetail.id_pemesanan.ToString() + ", " + tambahmobil.TOTAL_PEMBAYARAN;
                                audit.CREATED_DATE = DateTime.Now;
                                audit.MODIFIED_DATE = DateTime.Now;
                                db.TBL_AUDIT_TRAIL.Add(audit);
                                db.SaveChanges();
                            }
                            //total jumlah
                            //table transaksi
                            //var jumlahtotal = db.TR_DETAIL_PEMESANAN
                            //        .Where(x => x.ID_TRANSAKSI == cektransaksi.ID_TRANSAKSI)
                            //        .Select(x => x.TOTAL_PEMBAYARAN).Sum();

                            var jumlahtotal = db.TR_DETAIL_PEMESANAN
                            .Where(uf => uf.ID_TRANSAKSI == cektransaksi.ID_TRANSAKSI)
                            .Select(uf => uf.TOTAL_PEMBAYARAN)
                            //.DefaultIfEmpty()
                            .Sum();

                            TBL_TRANSAKSI trx = db.TBL_TRANSAKSI.Where(x => x.ID_TRANSAKSI == cektransaksi.ID_TRANSAKSI).FirstOrDefault();

                            trx.TOTAL_TRANSAKSI = jumlahtotal;
                            trx.MODIFIED_DATE = DateTime.Now;

                            db.Entry(trx).State = EntityState.Modified;
                            db.Entry(trx).Property("TOTAL_TRANSAKSI").IsModified = true;
                            db.Entry(trx).Property("MODIFIED_DATE").IsModified = true;
                            db.SaveChanges();
                        }

                        //TR_DETAIL_PEMESANAN pesanmodel = new TR_DETAIL_PEMESANAN();
                        transactions.Commit();
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ViewBag.error = "Please Login/Regist first";
                        return RedirectToAction("Index");
                    }
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    ViewBag.Error = msgErr.Message;
                }
            }
            return null;
        }
    }
}