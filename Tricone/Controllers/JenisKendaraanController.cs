﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class JenisKendaraanController : Controller
    {
        // GET: Kendaraan
        DB_TRICONEEntitie dc = new DB_TRICONEEntitie();

        public ActionResult Index()
        {
            return View(dc.MS_JENIS_KENDARAAN.ToList());
        }

        [HttpGet]
        public JsonResult Getdata()
        {
            List<MS_JENIS_KENDARAAN> kendList = dc.MS_JENIS_KENDARAAN.ToList<MS_JENIS_KENDARAAN>();
            List<jeniskdrOutput> result = new List<jeniskdrOutput>();
            foreach (var item in kendList)
            {
                result.Add(new jeniskdrOutput
                {
                    ID_JENIS_KENDARAAN = item.ID_JENIS_KENDARAAN,
                    NAMA_KENDARAAN = item.NAMA_KENDARAAN
                });
            }

            //var json = Newtonsoft.Json.JsonConvert.SerializeObject(result);
            return Json(new { data = result.OrderByDescending(e => e.ID_JENIS_KENDARAAN) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddorEdit(int id = 0)
        {
            if (id == 0)
            {
                return View(new MS_JENIS_KENDARAAN());
            }
            else
            {
                return View(dc.MS_JENIS_KENDARAAN.Where(x => x.ID_JENIS_KENDARAAN == id).FirstOrDefault<MS_JENIS_KENDARAAN>());
            }
        }

        [HttpPost]
        public ActionResult AddorEdit(MS_JENIS_KENDARAAN jeniskendaraan)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    jeniskendaraan.MODIFIED_DATE = DateTime.Now;
                    if (jeniskendaraan.ID_JENIS_KENDARAAN == 0)
                    {
                        jeniskendaraan.CREATED_DATE = DateTime.Now;
                        dc.MS_JENIS_KENDARAAN.Add(jeniskendaraan);
                        dc.SaveChanges();

                        //audit trail
                        var jeken = dc.MS_JENIS_KENDARAAN.OrderByDescending(e => e.ID_JENIS_KENDARAAN).FirstOrDefault();
                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                        audit.NAMA_TABEL = "MS_JENIS_KENDARAAN";
                        audit.AKSI = "ADD";
                        audit.NAMA_FIELD = "All Field";
                        audit.VALUE_LAMA = "";
                        audit.userid = Session["ID_JENIS_KENDARAAN"].ToString();
                        audit.VALUE_BARU = jeken.ID_JENIS_KENDARAAN + ", " + jeken.NAMA_KENDARAAN;
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();

                        transactions.Commit();
                        return Json(new { success = true, message = "Saved Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var oke = dc.MS_JENIS_KENDARAAN.AsNoTracking().Where(x => x.ID_JENIS_KENDARAAN == jeniskendaraan.ID_JENIS_KENDARAAN).FirstOrDefault();
                        dc.Entry(jeniskendaraan).State = EntityState.Modified;
                        dc.Entry(jeniskendaraan).Property("CREATED_DATE").IsModified = false;
                        dc.SaveChanges();

                        //audit trail edit

                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                        audit.NAMA_TABEL = "MS_JENIS_KENDARAAN";
                        audit.AKSI = "EDIT";
                        audit.userid = Session["ID_MEMBER"].ToString();
                        audit.NAMA_FIELD = "NAMA_KENDARAAN";
                        audit.VALUE_LAMA = jeniskendaraan.ID_JENIS_KENDARAAN + ", " + oke.NAMA_KENDARAAN;
                        audit.VALUE_BARU = jeniskendaraan.ID_JENIS_KENDARAAN + ", " + jeniskendaraan.NAMA_KENDARAAN;
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();

                        transactions.Commit();
                        return Json(new { success = true, message = "Updated Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    return Json(new { success = true, message = msgErr.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            MS_JENIS_KENDARAAN jeniskendaraan = dc.MS_JENIS_KENDARAAN.Where(x => x.ID_JENIS_KENDARAAN == id).FirstOrDefault<MS_JENIS_KENDARAAN>();
            dc.MS_JENIS_KENDARAAN.Remove(jeniskendaraan);
            dc.SaveChanges();
            return Json(new { success = true, message = "Deleted Successfully" }, JsonRequestBehavior.AllowGet);
        }
    }

    public class jeniskdrOutput
    {
        public int ID_JENIS_KENDARAAN { get; set; }
        public string NAMA_KENDARAAN { get; set; }
    }
}
