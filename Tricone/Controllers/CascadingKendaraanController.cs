﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class CascadingKendaraanController : Controller
    {
        // GET: CascadingKendaraan
        public ActionResult Index()
        {
            DB_TRICONEEntitie dc = new DB_TRICONEEntitie();
            ViewBag.MS_JENISKENDARAANLIST = new SelectList(GetMS_JENISKENDARAANList(), "ID_JENIS_KENDARAAN", "NAMA_KENDARAAN");
            return View();
        }

        public List<MS_JENIS_KENDARAAN> GetMS_JENISKENDARAANList()
        {
            DB_TRICONEEntitie dc = new DB_TRICONEEntitie();
            List<MS_JENIS_KENDARAAN> msjeniskendaraan = dc.MS_JENIS_KENDARAAN.ToList();
            return msjeniskendaraan;
        }

        public ActionResult GetMS_KENDARAANLIST(int ID_JENIS_KENDARAAN)
        {
            DB_TRICONEEntitie dc = new DB_TRICONEEntitie();
            List<MS_KENDARAAN> selectList = dc.MS_KENDARAAN.Where(x => x.ID_JENIS_KENDARAAN == ID_JENIS_KENDARAAN).ToList();
            ViewBag.Slist = new SelectList(selectList, "ID_KENDARAAN", "JUMLAH_SEAT", "JUMLAH_KENDARAAN");
            return PartialView("DisplayKendaraan");
        }
    }
}