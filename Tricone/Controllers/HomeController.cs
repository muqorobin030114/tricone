﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class HomeController : Controller
    {
        DB_TRICONEEntitie db = new DB_TRICONEEntitie();
        public ActionResult Index()
        {
            ViewBag.loggedInRoleMember = Convert.ToString(TempData["loggedInRoleMember"]);
            ViewBag.Message = "home";
            List<v_kendaraan> kendaraanList2 = db.v_kendaraan.ToList();
            ViewData["kndraan2"] = kendaraanList2;
            //List<TBL_TESTIMONI> testimonilist = db.TBL_TESTIMONI.Where(x => x.STATUS == 1).ToList();
            var testimonilist = (from testi in db.TBL_TESTIMONI where testi.STATUS == 1 select new testimoni {NAMA = testi.NAMA, MESSAGE= testi.MESSAGE}).ToList();
            ViewData["tesimoniuser"] = testimonilist;

            var qtrx = (from dtl in db.TR_DETAIL_PEMESANAN
                        join trs in db.TBL_TRANSAKSI on dtl.ID_TRANSAKSI equals trs.ID_TRANSAKSI
                        where trs.STATUS == 1 && trs.APPROVAL == 4 && !(from o in db.detail_supir select o.ID_PEMESANAN).Contains(dtl.ID_PEMESANAN)
                        group dtl by new { dtl.ID_TRANSAKSI } into grp
                        select new
                        {
                            ID_TRANSAKSI = grp.Key.ID_TRANSAKSI,
                            pcount = grp.Count()
                        });
            ViewBag.Supir = qtrx.Count();

            var tranx = db.v_Notreschedule.Where(af => af.APPROVAL!=0 && af.APPROVAL != 1).Select(uf => uf.ID_TRANSAKSI).Count();
            ViewBag.tranx = tranx;

            var resche = db.v_reschedule.Where(af => af.APPROVAL != 0 && af.APPROVAL != 1).Select(uf => uf.ID_TRANSAKSI).Count();
            ViewBag.resche = resche;

            var refun = db.v_refund.Select(uf => uf.ID_TRANSAKSI).Count();
            ViewBag.refun = refun;
            
            return View();
        }

        public ActionResult GetData()
        {
            int id_mem = Convert.ToInt32(Session["ID_MEMBER"]);
            var notifcart = db.TBL_TRANSAKSI
                    .Where(uf => uf.ID_MEMBER == id_mem && uf.APPROVAL == 0)
                    .Select(uf => uf.APPROVAL)
                    .Count();
            ViewBag.keranjang = notifcart;
            return Json(notifcart, JsonRequestBehavior.AllowGet);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        [Authorize]
        public ActionResult Index1()
        {
            return View();
        }


        //notifkasi
        [ChildActionOnly]
        public ActionResult Navig()
        {
            int id_mem = Convert.ToInt32(Session["ID_MEMBER"]);
            var notifcart = db.TBL_TRANSAKSI
                    .Where(uf => uf.ID_MEMBER == id_mem && uf.APPROVAL == 0)
                    .Select(uf => uf.APPROVAL)
                    .Count();
            ViewBag.keranjang = notifcart;

            //all notifikasi
            var countnotifi = db.notifikasi
                    .Where(uf => uf.NOTIF_FOR == id_mem.ToString())
                    .Select(uf => uf.NAMA_NOTIF)
                    .Count();
            ViewBag.jumlahnotif = countnotifi;

            //notif transaksi
            var notiftrans = db.notifikasi
                    .Where(uf => uf.NAMA_NOTIF == "transaksi" && uf.NOTIF_FOR == id_mem.ToString())
                    .Select(uf => uf.NAMA_NOTIF)
                    .Count();
            ViewBag.notiftrans = notiftrans;

            //notif transaksi
            var notifchange = db.notifikasi
                    .Where(uf => uf.NAMA_NOTIF == "ubahjadwal" && uf.NOTIF_FOR == id_mem.ToString())
                    .Select(uf => uf.NAMA_NOTIF)
                    .Count();
            ViewBag.notifchange = notifchange;

            //notif transaksi
            var notifrefun = db.notifikasi
                    .Where(uf => uf.NAMA_NOTIF == "refund" && uf.NOTIF_FOR == id_mem.ToString())
                    .Select(uf => uf.NAMA_NOTIF)
                    .Count();
            ViewBag.notifrefun = notifrefun;
            return PartialView("Nav");
        }

        [ChildActionOnly]
        public ActionResult Navigadmin()
        {
            //semua notif
            var notifall = db.notifikasi
                    .Where(uf => uf.NOTIF_FOR == "admin")
                    .Select(uf => uf.NOTIF_FOR)
                    .Count();
            ViewBag.notifall = notifall;

            //notif transaksi
            var notiftrans = db.notifikasi
                    .Where(uf => uf.NOTIF_FOR == "admin" && uf.NAMA_NOTIF=="transaksi")
                    .Select(uf => uf.NOTIF_FOR)
                    .Count();
            ViewBag.notiftrans = notiftrans;

            //notif change schedule
            var notifchange = db.notifikasi
                    .Where(uf => uf.NOTIF_FOR == "admin" && uf.NAMA_NOTIF == "ubahjadwal")
                    .Select(uf => uf.NOTIF_FOR)
                    .Count();
            ViewBag.notifchange = notifchange;

            //notif refund
            var notifrefund = db.notifikasi
                    .Where(uf => uf.NOTIF_FOR == "admin" && uf.NAMA_NOTIF == "refund")
                    .Select(uf => uf.NOTIF_FOR)
                    .Count();
            ViewBag.notifrefund = notifrefund;
            return PartialView("Nav");
        }

        //notif admin transaksi
        public ActionResult trans()
        {
            using (var transactions = db.Database.BeginTransaction())
            {
                try
                {
                    //hapus notif
                    db.notifikasi.RemoveRange(db.notifikasi.Where(x => x.NAMA_NOTIF == "transaksi" && x.NOTIF_FOR == "admin"));
                    db.SaveChanges();
                    transactions.Commit();
                    return RedirectToAction("index", "transaksi");
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    ViewBag.Error = msgErr.Message;
                }
            }
            return null;
        }

        public ActionResult Deltrans()
        {
            using (var transactions = db.Database.BeginTransaction())
            {
                try
                {
                    int id_mem = Convert.ToInt32(Session["ID_MEMBER"]);
                    //hapus notif
                    db.notifikasi.RemoveRange(db.notifikasi.Where(x => x.NAMA_NOTIF == "transaksi" && x.NOTIF_FOR == id_mem.ToString()));
                    db.SaveChanges();
                    transactions.Commit();
                    return RedirectToAction("Index", "ScheduleUser");
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    ViewBag.Error = msgErr.Message;
                }
            }
            return null;
        }

        //notif admin refund
        public ActionResult refun()
        {
            using (var transactions = db.Database.BeginTransaction())
            {
                try
                {
                    //hapus notif
                    db.notifikasi.RemoveRange(db.notifikasi.Where(x => x.NAMA_NOTIF == "refund" && x.NOTIF_FOR == "admin"));
                    db.SaveChanges();
                    transactions.Commit();
                    return RedirectToAction("index", "Refund");
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    ViewBag.Error = msgErr.Message;
                }
            }
            return null;
        }

        public ActionResult Delrefund()
        {
            using (var transactions = db.Database.BeginTransaction())
            {
                try
                {
                    int id_mem = Convert.ToInt32(Session["ID_MEMBER"]);
                    //hapus notif
                    db.notifikasi.RemoveRange(db.notifikasi.Where(x => x.NAMA_NOTIF == "refund" && x.NOTIF_FOR == id_mem.ToString()));
                    db.SaveChanges();
                    transactions.Commit();
                    return RedirectToAction("Index", "RefundUser");
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    ViewBag.Error = msgErr.Message;
                }
            }
            return null;
        }

        //notif admin reschedule
        public ActionResult ubahjdl()
        {
            using (var transactions = db.Database.BeginTransaction())
            {
                try
                {
                    //hapus notif
                    db.notifikasi.RemoveRange(db.notifikasi.Where(x => x.NAMA_NOTIF == "ubahjadwal" && x.NOTIF_FOR == "admin"));
                    db.SaveChanges();
                    transactions.Commit();
                    return RedirectToAction("index", "Reschedule");
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    ViewBag.Error = msgErr.Message;
                }
            }
            return null;
        }

        public ActionResult Delreschedule()
        {
            using (var transactions = db.Database.BeginTransaction())
            {
                try
                {
                    int id_mem = Convert.ToInt32(Session["ID_MEMBER"]);
                    //hapus notif
                    db.notifikasi.RemoveRange(db.notifikasi.Where(x => x.NAMA_NOTIF == "ubahjadwal" && x.NOTIF_FOR == id_mem.ToString()));
                    db.SaveChanges();
                    transactions.Commit();
                    return RedirectToAction("Index", "ReScheduleUser");
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    ViewBag.Error = msgErr.Message;
                }
            }
            return null;
        }
       
    }
}