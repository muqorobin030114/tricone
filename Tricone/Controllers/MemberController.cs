﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class MemberController : Controller
    {
        // GET: Member
        DB_TRICONEEntitie dc = new DB_TRICONEEntitie();

        public ActionResult Index()
        {
            return View(dc.TBL_MEMBER.ToList());
        }

        public JsonResult Getdata()
        {
            List<TBL_MEMBER> provList = dc.TBL_MEMBER.ToList<TBL_MEMBER>();
            List<MemberOutput> result = new List<MemberOutput>();
            int id_user = Convert.ToInt32(Session["ID_MEMBER"].ToString());
            foreach (var item in provList)
            {
                if (id_user == Convert.ToInt32(item.ID_MEMBER) || Session["ROLEMEMBER"].ToString() == "SUPERADMIN" || item.ROLEMEMBER=="MEMBER") {
                    result.Add(new MemberOutput
                    {
                        ID_MEMBER = Convert.ToInt32(item.ID_MEMBER),
                        NAMA_MEMBER = item.NAMA_MEMBER,
                        ALAMAT = item.ALAMAT,
                        NO_TELEPON = item.NO_TELEPON,
                        EMAIL = item.EMAIL,
                        USERNAME = item.USERNAME,
                        ISEMAILVERIFIED = Convert.ToBoolean(item.ISEMAILVERIFIED),
                        ROLEMEMBER = item.ROLEMEMBER,
                        STATUS = Convert.ToBoolean(item.ID_MEMBER)
                    });
                }
            }
            //var json = Newtonsoft.Json.JsonConvert.SerializeObject(result);
            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddorEdit(int id = 0)
        {
            if (id == 0)
            {
                return View(new TBL_MEMBER());
            }
            else
            {
                return View(dc.TBL_MEMBER.Where(x => x.ID_MEMBER == id).FirstOrDefault<TBL_MEMBER>());
            }
        }

        [HttpPost]
        public ActionResult AddorEdit([Bind(Exclude = "ISEMAILVERIFIED,ACTIVATIONCODE")] TBL_MEMBER member)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    member.MODIFIED_DATE = DateTime.Now;
                    if (member.ID_MEMBER == 0)
                    {
                        bool Status = false;
                        string msg = "";
                        //model validation
                        if (ModelState.IsValid)
                        {
                            //email is already exist
                            #region //email is already exist
                            var isExist = IsEmailExist(member.EMAIL);
                            if (isExist)
                            {
                                ModelState.AddModelError("EmailExist", "Email already exist");
                                return View(member);
                            }
                            #endregion

                            NewMethod(member);
                            #region password hashing
                            member.PASSWORD = Crypto.Hash(member.PASSWORD);
                            member.CONFIRMPASSWORD = Crypto.Hash(member.CONFIRMPASSWORD);
                            #endregion
                            member.ISEMAILVERIFIED = false;
                            member.CREATED_DATE = DateTime.Now;

                            #region save data to database
                            using (DB_TRICONEEntitie dc = new DB_TRICONEEntitie())
                            {
                                dc.TBL_MEMBER.Add(member);
                                dc.SaveChanges();

                                //audit trail
                                var qaudit = dc.TBL_MEMBER.OrderByDescending(e => e.ID_MEMBER).FirstOrDefault();
                                TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                                audit.NAMA_TABEL = "TBL_MEMBER";
                                audit.AKSI = "ADD";
                                audit.NAMA_FIELD = "All Field";
                                audit.VALUE_LAMA = "";
                                if (Session["ID_MEMBER"].ToString() != null)
                                {
                                    audit.userid = Session["ID_MEMBER"].ToString();
                                }
                                audit.VALUE_BARU = qaudit.ID_MEMBER + ", " + qaudit.NAMA_MEMBER + ", " + qaudit.ALAMAT + ", " + qaudit.NO_TELEPON + ", " + qaudit.EMAIL + ", " + qaudit.USERNAME + ", " + qaudit.STATUS + ", " + qaudit.ROLEMEMBER;
                                audit.CREATED_DATE = DateTime.Now;
                                audit.MODIFIED_DATE = DateTime.Now;
                                dc.TBL_AUDIT_TRAIL.Add(audit);
                                dc.SaveChanges();

                                transactions.Commit();
                                //sent email to user
                                SendVerificationLinkEmail(member.EMAIL, member.ACTIVATIONCODE.ToString());
                                msg = "Registration successfully done. Account activation link " +
                                    " has been sent to your email id: " + member.EMAIL;
                                Status = true;
                            }
                            #endregion
                        }
                        else
                        {
                            msg = "Invalid Request";
                        }
                        ViewBag.Status = Status;
                        return Json(new { success = true, message = msg }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var oke = dc.TBL_MEMBER.AsNoTracking().Where(x => x.ID_MEMBER == member.ID_MEMBER).FirstOrDefault();
                        dc.Entry(member).State = EntityState.Modified;
                        dc.Entry(member).Property("PASSWORD").IsModified = false;
                        dc.Entry(member).Property("ISEMAILVERIFIED").IsModified = false;
                        dc.Entry(member).Property("ACTIVATIONCODE").IsModified = false;
                        dc.Entry(member).Property("RESETPASSWORD").IsModified = false;
                        dc.Entry(member).Property("CREATED_DATE").IsModified = false;
                        dc.SaveChanges();

                        //audit trail edit
                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                        audit.NAMA_TABEL = "TBL_MEMBER";
                        audit.AKSI = "EDIT";
                        if (Session["ID_MEMBER"].ToString() != null)
                        {
                            audit.userid = Session["ID_MEMBER"].ToString();
                        }
                        audit.NAMA_FIELD = "";
                        audit.VALUE_LAMA = oke.ID_MEMBER + ", " + oke.NAMA_MEMBER + ", " + oke.ALAMAT + ", " + oke.NO_TELEPON + ", " + oke.EMAIL + ", " + oke.USERNAME + ", " + oke.STATUS + ", " + oke.ROLEMEMBER;
                        audit.VALUE_BARU = member.ID_MEMBER + ", " + member.NAMA_MEMBER + ", " + member.ALAMAT + ", " + member.NO_TELEPON + ", " + member.EMAIL + ", " + member.USERNAME + ", " + member.STATUS + ", " + member.ROLEMEMBER;
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();

                        transactions.Commit();
                        return Json(new { success = true, message = "Updated Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    return Json(new { success = true, message = msgErr.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [NonAction]
        public bool IsEmailExist(string emailID)
        {
            using (DB_TRICONEEntitie dc = new DB_TRICONEEntitie())
            {
                var v = dc.TBL_MEMBER.Where(a => a.EMAIL == emailID).FirstOrDefault();
                return v != null;
            }
        }

        private static void NewMethod(TBL_MEMBER member)
        {
            #region generate activation code
            member.ACTIVATIONCODE = Guid.NewGuid();
            #endregion
        }

        [HttpGet]
        public ActionResult VerifyAccount(string id)
        {
            bool Status = false;
            using (DB_TRICONEEntitie dc = new DB_TRICONEEntitie())
            {
                dc.Configuration.ValidateOnSaveEnabled = false; //this line I have added here to avoid
                                                                //confirm password does not match issue on save changes
                var v = dc.TBL_MEMBER.Where(a => a.ACTIVATIONCODE == new Guid(id)).FirstOrDefault();
                if (v != null)
                {
                    v.ISEMAILVERIFIED = true;
                    dc.SaveChanges();
                    Status = true;
                }
                else
                {
                    ViewBag.Message = "Invalid Request";
                }
            }
            ViewBag.Status = Status;
            return View();
        }

        [NonAction]
        public void SendVerificationLinkEmail(string emailID, string activationCode, string emailFor = "VerifyAccount")
        {
            var verifyUrl = "/Regist/" + emailFor + "/" + activationCode;
            var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyUrl);

            var fromEmail = new MailAddress("traveltricone@gmail.com", "Tricone Membership");
            var toEmail = new MailAddress(emailID);
            var fromEmailPassword = "tangerang"; //replace with actual password

            string subject = "";
            string body = "";
            if (emailFor == "VerifyAccount")
            {
                subject = "Your account is succesfully created!";

                body = "<br/><br/>We are exited to tell you that your account is" +
                " successfully created. Please click on the below link to verify your account" +
                " <br/><br/><a href='" + link + "'>" + link + "</a>";
            }
            else if (emailFor == "ResetPassword")
            {
                subject = "Reset Password";
                body = "Hi, <br/><br/>We got requst for your account password. Please click on the below link to reset your password" +
                    "<br/><br/><a href=" + link + ">Reset Password link</a>";
            }
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };

            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
                smtp.Send(message);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            TBL_MEMBER member = dc.TBL_MEMBER.Where(x => x.ID_MEMBER == id).FirstOrDefault<TBL_MEMBER>();
            dc.TBL_MEMBER.Remove(member);
            dc.SaveChanges();
            return Json(new { success = true, message = "Deleted Successfully" }, JsonRequestBehavior.AllowGet);
        }
    }
    public class MemberOutput
    {
        public int ID_MEMBER { get; set; }
        public string NAMA_MEMBER { get; set; }
        public string ALAMAT { get; set; }
        public string NO_TELEPON { get; set; }
        public string EMAIL { get; set; }
        public string USERNAME { get; set; }
        public bool STATUS { get; set; }
        public bool ISEMAILVERIFIED { get; set; }
        public string ROLEMEMBER { get; set; }
    }
}
