﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class HariLiburController : Controller
    {
        // GET: Hari Libur
        DB_TRICONEEntitie dc = new DB_TRICONEEntitie();

        public ActionResult Index()
        {
            return View(dc.MS_HARILIBUR.ToList());
        }

        /*public ActionResult Getdata()
        {
            List<MS_HARILIBUR> hariliburlist = dc.MS_HARILIBUR.ToList<MS_HARILIBUR>();
            return Json(new { data = hariliburlist }, JsonRequestBehavior.AllowGet);
        }*/
        public JsonResult Getdata()
        {
            List<MS_HARILIBUR> provList = dc.MS_HARILIBUR.ToList<MS_HARILIBUR>();
            List<HariliburOutput> result = new List<HariliburOutput>();
            foreach (var item in provList)
            {
                result.Add(new HariliburOutput
                {
                    ID_HARILIBUR = item.ID_HARILIBUR,
                    NAMA_HARILIBUR = item.NAMA_HARILIBUR,
                    STARTDATE = Convert.ToDateTime(item.STARTDATE).Date,
                    ENDDATE = Convert.ToDateTime(item.ENDDATE).Date,
                    PERSEN_LIBUR = Convert.ToInt32(item.PERSEN_LIBUR)
                });
            }

            //var json = Newtonsoft.Json.JsonConvert.SerializeObject(result);
            return Json(new { data = result.OrderByDescending(e => e.ID_HARILIBUR) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddorEdit(int id = 0)
        {
            if (id == 0)
            {
                return View(new MS_HARILIBUR());
            }
            else
            {
                return View(dc.MS_HARILIBUR.Where(x => x.ID_HARILIBUR == id).FirstOrDefault<MS_HARILIBUR>());
            }
        }

        [HttpPost]
        public ActionResult AddorEdit(MS_HARILIBUR harilibur)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    harilibur.MODIFIED_DATE = DateTime.Now;
                    if (harilibur.ID_HARILIBUR == 0)
                    {
                        harilibur.CREATED_DATE = DateTime.Now;
                        dc.MS_HARILIBUR.Add(harilibur);
                        dc.SaveChanges();

                        //audit trail add
                        var hlibur = dc.MS_HARILIBUR.OrderByDescending(x => x.ID_HARILIBUR).FirstOrDefault();
                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                        audit.NAMA_TABEL = "MS_HARILIBUR";
                        audit.AKSI = "ADD";
                        audit.NAMA_FIELD = "All Field";
                        audit.VALUE_LAMA = "";
                        audit.VALUE_BARU = hlibur.ID_HARILIBUR.ToString();
                        if (Session["ID_MEMBER"] != null)
                        {
                            audit.userid = Session["ID_MEMBER"].ToString();
                        }
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();

                        transactions.Commit();
                        return Json(new { success = true, message = "Saved Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var oke = dc.MS_HARILIBUR.AsNoTracking().Where(x => x.ID_HARILIBUR == harilibur.ID_HARILIBUR).FirstOrDefault();
                        dc.Entry(harilibur).State = EntityState.Modified;
                        dc.Entry(harilibur).Property("CREATED_DATE").IsModified = false;
                        dc.SaveChanges();


                        //audit trail

                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                        audit.NAMA_TABEL = "MS_HARILIBUR";
                        audit.AKSI = "EDIT";
                        audit.userid = Session["ID_MEMBER"].ToString();
                        audit.NAMA_FIELD = "NAMA_HARILIBUR" + ", " + "STARTDATE" + ", " + "ENDDATE" + ", " + "PERSEN_LIBUR";
                        audit.VALUE_LAMA = harilibur.ID_HARILIBUR + ", " + oke.NAMA_HARILIBUR + ", " + oke.STARTDATE + ", " + oke.ENDDATE + ", " + oke.PERSEN_LIBUR;
                        audit.VALUE_BARU = harilibur.ID_HARILIBUR + ", " + harilibur.NAMA_HARILIBUR + ", " + harilibur.STARTDATE + ", " + harilibur.ENDDATE + ", " + harilibur.PERSEN_LIBUR;
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();

                        transactions.Commit();
                        return Json(new { success = true, message = "Updated Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();
                    transactions.Rollback();
                    return Json(new { success = true, message = msgErr.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            MS_HARILIBUR harilibur = dc.MS_HARILIBUR.Where(x => x.ID_HARILIBUR == id).FirstOrDefault<MS_HARILIBUR>();
            dc.MS_HARILIBUR.Remove(harilibur);
            dc.SaveChanges();
            return Json(new { success = true, message = "Deleted Successfully" }, JsonRequestBehavior.AllowGet);
        }
    }
    public class HariliburOutput
    {
        public int ID_HARILIBUR { get; set; }
        public string NAMA_HARILIBUR { get; set; }
        public DateTime STARTDATE { get; set; }
        public DateTime ENDDATE { get; set; }
        public int PERSEN_LIBUR { get; set; }
    }
}
