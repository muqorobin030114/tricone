﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;


namespace Tricone.ControllersAdmin
{
    public class ScheduleController : Controller
    {
        DB_TRICONEEntitie dc = new DB_TRICONEEntitie();

        // GET: Schedule
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetEvents()
        {
            var events = dc.v_transaksi.Where(a=> a.STATUS_TRANSAKSI==1 && a.APPROVAL==4).ToList();
            return new JsonResult { Data = events, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }
    }
}