﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class PromoUserController : Controller
    {
        // GET: PromoUser
        DB_TRICONEEntitie db = new DB_TRICONEEntitie();
        public ActionResult Index()
        {
            List<TBL_PROMO> promolist = db.TBL_PROMO.ToList();
            ViewData["promolist"] = promolist;
            return View();
        }
    }
}