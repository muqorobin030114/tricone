﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class RescheduleController : Controller
    {
        // GET: Reschedule
        DB_TRICONEEntitie dc = new DB_TRICONEEntitie();
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult Getdata()
        {
            List<v_reschedule> transaksiList = dc.v_reschedule.ToList<v_reschedule>();
            List<transaksiOutput> result = new List<transaksiOutput>();
            foreach (var item in transaksiList)
            {
                if (item.APPROVAL != 0 && item.APPROVAL != 1)
                {
                    result.Add(new transaksiOutput
                    {
                        ID_TRANSAKSI = item.ID_TRANSAKSI,
                        ID_MEMBER = Convert.ToInt32(item.ID_MEMBER),
                        NAMA_MEMBER = item.NAMA_MEMBER,
                        ID_REFUND = Convert.ToInt32(item.ID_REFUND),
                        ID_PEMBAYARAN = Convert.ToInt32(item.ID_PEMBAYARAN),
                        TOTAL_TRANSAKSI = Convert.ToInt32(item.TOTAL_TRANSAKSI),
                        ID_PROMO = Convert.ToInt32(item.ID_PROMO),
                        FOTO_PEMBAYARAN = item.FOTO_PEMBAYARAN,
                        TOTAL_DURASI = Convert.ToInt32(item.TOTAL_DURASI),
                        STATUS_UBAH_JADWAL = Convert.ToInt32(item.STATUS_UBAH_JADWAL),
                        APPROVAL = Convert.ToInt32(item.APPROVAL),
                        KETERANGANUSER = item.KETERANGANUSER,
                        KETERANGANADMIN = item.KETERANGANADMIN,
                        STATUS = Convert.ToInt32(item.STATUS_TRANSAKSI),
                        TGL_PERGI = Convert.ToDateTime(item.TGL_PERGI),
                        TGL_PULANG = Convert.ToDateTime(item.TGL_PULANG),
                        SISA_BAYAR = Convert.ToInt32(item.SISA_BAYAR)
                    });
                }
            }
            
            return Json(new { data = result.OrderByDescending(e => e.ID_TRANSAKSI) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Approve(int id)
        {
            var oke = dc.TBL_TRANSAKSI.AsNoTracking().Where(x => x.ID_TRANSAKSI == id).FirstOrDefault();
            var trx = dc.TBL_TRANSAKSI.Where(x => x.ID_TRANSAKSI == id).FirstOrDefault();
            trx.APPROVAL = 4;
            trx.STATUS = 1;
            trx.MODIFIED_DATE = DateTime.Now;

            dc.Entry(trx).State = EntityState.Modified;
            dc.Entry(trx).Property("STATUS").IsModified = true;
            dc.Entry(trx).Property("APPROVAL").IsModified = true;
            dc.Entry(trx).Property("MODIFIED_DATE").IsModified = true;
            dc.SaveChanges();

            //audit trail
            TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
            audit.NAMA_TABEL = "TBL_TRANSAKSI";
            audit.AKSI = "EDIT, APPROVED";
            audit.userid = Session["ID_MEMBER"].ToString();
            audit.NAMA_FIELD = "";
            audit.VALUE_LAMA = oke.ID_TRANSAKSI + ", " + oke.STATUS + ", " + oke.APPROVAL;
            audit.VALUE_BARU = id + ", " + trx.STATUS + ", " + trx.APPROVAL;
            audit.CREATED_DATE = DateTime.Now;
            audit.MODIFIED_DATE = DateTime.Now;
            dc.TBL_AUDIT_TRAIL.Add(audit);
            dc.SaveChanges();

            //buat notifikasi
            var notit = dc.v_reschedule.Where(x => x.ID_TRANSAKSI == id).FirstOrDefault();
            notifikasi noti = new notifikasi();
            noti.NAMA_NOTIF = "ubahjadwal";
            noti.NOTIF_FOR = notit.ID_MEMBER.ToString();
            dc.Entry(noti).State = EntityState.Added;
            dc.SaveChanges();

            return RedirectToAction("Index", "Reschedule");
        }

        public ActionResult popreject(int id)
        {
            ViewBag.id_trans = id;
            return View();
        }



        public ActionResult Reject(FormCollection formnya)
        {
            int id = Convert.ToInt32(formnya["id_trans"]);
            var keterangan = formnya["KETERANGANADMIN"];
            var oke = dc.TBL_TRANSAKSI.AsNoTracking().Where(x => x.ID_TRANSAKSI == id).FirstOrDefault();
            var trx = dc.TBL_TRANSAKSI.Where(x => x.ID_TRANSAKSI == id).FirstOrDefault();
            trx.STATUS = 0;
            trx.APPROVAL = 3;
            trx.KETERANGANADMIN = keterangan;
            trx.MODIFIED_DATE = DateTime.Now;

            dc.Entry(trx).State = EntityState.Modified;
            dc.Entry(trx).Property("STATUS").IsModified = true;
            dc.Entry(trx).Property("KETERANGANADMIN").IsModified = true;
            dc.Entry(trx).Property("APPROVAL").IsModified = true;
            dc.Entry(trx).Property("MODIFIED_DATE").IsModified = true;
            dc.SaveChanges();

            var trx2 = dc.TBL_TRANSAKSI.Where(x => x.STATUS_UBAH_JADWAL == trx.ID_TRANSAKSI).FirstOrDefault();
            trx2.STATUS = 1;
            trx2.APPROVAL = 4;
            trx2.MODIFIED_DATE = DateTime.Now;

            dc.Entry(trx2).State = EntityState.Modified;
            dc.Entry(trx2).Property("STATUS").IsModified = true;
            dc.Entry(trx2).Property("APPROVAL").IsModified = true;
            dc.Entry(trx2).Property("MODIFIED_DATE").IsModified = true;
            dc.SaveChanges();

            //audit trail edit
            TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
            audit.NAMA_TABEL = "TBL_TRANSAKSI,TR_DETAIL_PEMESANAN";
            audit.AKSI = "EDIT, REJECTED";
            audit.userid = Session["ID_MEMBER"].ToString();
            audit.NAMA_FIELD = "";
            audit.VALUE_LAMA = oke.ID_TRANSAKSI + ", " + oke.STATUS + ", " + oke.APPROVAL;
            audit.VALUE_BARU = id + ", " + trx.STATUS + ", " + trx.APPROVAL;
            audit.CREATED_DATE = DateTime.Now;
            audit.MODIFIED_DATE = DateTime.Now;
            dc.TBL_AUDIT_TRAIL.Add(audit);
            dc.SaveChanges();

            //buat notifikasi
            var notit = dc.v_reschedule.Where(x => x.ID_TRANSAKSI == id).FirstOrDefault();
            notifikasi noti = new notifikasi();
            noti.NAMA_NOTIF = "ubahjadwal";
            noti.NOTIF_FOR = notit.ID_MEMBER.ToString();
            dc.Entry(noti).State = EntityState.Added;
            dc.SaveChanges();
            return Json(new { success = true, message = "Reject Successfully" }, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Index", "Reschedule");
        }

        [HttpGet]
        public ActionResult Details(int id)
        {
            ViewBag.id_trans = id;
            return View();
        }

        [HttpGet]
        public JsonResult GetdataDetail(int id_trans)
        {
            List<v_detail_pesan> detailtransaksiList = dc.v_detail_pesan.ToList<v_detail_pesan>();
            List<detailtransaksiOutput> result = new List<detailtransaksiOutput>();
            foreach (var item in detailtransaksiList)
            {
                if (id_trans == item.id_transaksi)
                {
                    result.Add(new detailtransaksiOutput
                    {
                        ID_TRANSAKSI = Convert.ToInt32(item.id_transaksi),
                        id_pemesanan = Convert.ToInt32(item.id_pemesanan),
                        id_member = Convert.ToInt32(item.id_member),
                        nama_member = item.nama_member,
                        telpon_member = item.telpon_member,
                        email_member = item.email_member,
                        id_harga = Convert.ToInt32(item.id_harga),
                        harga = Convert.ToInt32(item.harga),
                        id_kendaraan = Convert.ToInt32(item.id_kendaraan),
                        id_jenis_kendaraan = Convert.ToInt32(item.id_jenis_kendaraan),
                        nama_kendaraan = item.nama_kendaraan,
                        jumlah_seat = Convert.ToInt32(item.jumlah_seat),
                        jumlah_kendaraan = Convert.ToInt32(item.jumlah_kendaraan),
                        status_kendaraan = Convert.ToInt32(item.status_kendaraan),
                        durasi_jam = Convert.ToInt32(item.durasi_jam),
                        id_harilibur = Convert.ToInt32(item.id_harilibur),
                        nama_harilibur = item.nama_harilibur,
                        startdate_libur = Convert.ToDateTime(item.startdate_libur),
                        enddate_libur = Convert.ToDateTime(item.enddate_libur),
                        persen_libur = Convert.ToInt32(item.persen_libur),
                        tgl_pergi = Convert.ToDateTime(item.tgl_pergi),
                        tgl_pulang = Convert.ToDateTime(item.tgl_pulang),
                        id_kota = Convert.ToInt32(item.id_kota),
                        nama_kota = item.nama_kota,
                        id_provinsi = Convert.ToInt32(item.id_provinsi),
                        nama_provinsi = item.nama_provinsi,
                        quantity_kendaraan = Convert.ToInt32(item.quantity_kendaraan),
                        total_pembayaran = Convert.ToInt32(item.total_pembayaran),
                        status_detail_pesan = Convert.ToInt32(item.status_detail_pesan),
                        id_supir = Convert.ToInt32(item.id_supir),
                        nama_supir = item.nama_supir,
                        telpon_supir = item.telpon_supir

                    });
                }
            }
            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }
    }
}