﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class KotaController : Controller
    {
        // GET: kota
        DB_TRICONEEntitie dc = new DB_TRICONEEntitie();

        public ActionResult Index()
        {
            return View(dc.MS_KOTA.ToList());
        }

        
        [HttpGet]
        public JsonResult Getdata()
        {
            List<MS_KOTA> kotalist = dc.MS_KOTA.ToList<MS_KOTA>();
            List<KotaOutput> result = new List<KotaOutput>();
            foreach (var item in kotalist)
            {
                result.Add( new KotaOutput{
                    ID_KOTA=item.ID_KOTA,
                    NAMA_KOTA=item.NAMA_KOTA,
                    NAMA_PROVINSI = item.MS_PROVINSI.NAMA_PROVINSI,
                    ID_PROVINSI = item.MS_PROVINSI.ID_PROVINSI
                });
            }

            //var json = Newtonsoft.Json.JsonConvert.SerializeObject(result);
            return Json(new { data = result.OrderByDescending(e => e.ID_KOTA) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddorEdit(int id = 0)
        {
            MS_KOTA kotamodel = new MS_KOTA();
            kotamodel.provcollection = dc.MS_PROVINSI.ToList<MS_PROVINSI>();
            if (id == 0)
            {
                //return View(new MS_KOTA());
               return View(kotamodel);
            }
            else
            {
                kotamodel = dc.MS_KOTA.Where(x => x.ID_KOTA == id).FirstOrDefault();
                kotamodel.provcollection = dc.MS_PROVINSI.ToList<MS_PROVINSI>();
                //return View(dc.MS_KOTA.Where(x => x.ID_KOTA == id).FirstOrDefault<MS_KOTA>());
                return View(kotamodel);
            }
        }

        [HttpPost]
        public ActionResult AddorEdit(MS_KOTA kota)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    kota.MODIFIED_DATE = DateTime.Now;
                    if (kota.ID_KOTA == 0)
                    {
                        kota.CREATED_DATE = DateTime.Now;
                        dc.MS_KOTA.Add(kota);
                        dc.SaveChanges();

                        //audit trail
                        var city = dc.MS_KOTA.OrderByDescending(e => e.ID_KOTA).FirstOrDefault();
                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                        audit.NAMA_TABEL = "MS_KOTA";
                        audit.AKSI = "ADD";
                        audit.NAMA_FIELD = "All Field";
                        audit.VALUE_LAMA = "";
                        audit.userid = Session["ID_MEMBER"].ToString();
                        audit.VALUE_BARU = city.ID_KOTA + ", " + city.NAMA_KOTA + ", " + city.ID_PROVINSI+ ", "+ city.ID_PROVINSI;
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();

                        transactions.Commit();
                        return Json(new { success = true, message = "Saved Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var oke = dc.MS_KOTA.AsNoTracking().Where(x => x.ID_KOTA == kota.ID_KOTA).FirstOrDefault();
                        dc.Entry(kota).State = EntityState.Modified;
                        dc.Entry(kota).Property("CREATED_DATE").IsModified = false;
                        dc.SaveChanges();

                        //audit trail

                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                        audit.NAMA_TABEL = "MS_KOTA";
                        audit.AKSI = "EDIT";
                        audit.userid = Session["ID_MEMBER"].ToString();
                        audit.NAMA_FIELD = "NAMA_KOTA"+"NAMA_PROVINSI";
                        audit.VALUE_LAMA = kota.ID_KOTA + ", " + oke.NAMA_KOTA + ", " + oke.ID_PROVINSI;
                        audit.VALUE_BARU = kota.ID_KOTA + ", " + kota.NAMA_KOTA + ", " + kota.ID_PROVINSI; 
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();

                        transactions.Commit();
                        return Json(new { success = true, message = "Updated Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    return Json(new { success = true, message = msgErr.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            MS_KOTA kota = dc.MS_KOTA.Where(x => x.ID_KOTA == id).FirstOrDefault<MS_KOTA>();
            dc.MS_KOTA.Remove(kota);
            dc.SaveChanges();
            return Json(new { success = true, message = "Deleted Successfully" }, JsonRequestBehavior.AllowGet);
        }
    }

    public class KotaOutput
    {
        public int ID_KOTA { get; set; }
        public string NAMA_KOTA { get; set; }
        public string NAMA_PROVINSI { get; set; }
        public int ID_PROVINSI { get; set; }
    }
}
