﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class WisataUserController : Controller
    {
        // GET: WisataUser
        DB_TRICONEEntitie db = new DB_TRICONEEntitie();
        public ActionResult Index()
        {
            List<TBL_WISATA> wisataList = db.TBL_WISATA.Where(x => x.ID_PROVINSI == 11 || x.ID_PROVINSI == 12 || x.ID_PROVINSI == 13 || x.ID_PROVINSI == 14 || x.ID_PROVINSI == 15 || x.ID_PROVINSI == 16).ToList();
            ViewData["detailwisata"] = wisataList;
            return View();
        }

        public ActionResult Jabar()
        {
            List<TBL_WISATA> wisataList = db.TBL_WISATA.Where(x => x.ID_PROVINSI == 12).ToList();
            ViewData["detailwisata"] = wisataList;
            return View();
        }
        
        public ActionResult Banten()
        {
            List<TBL_WISATA> wisataList = db.TBL_WISATA.Where(x => x.ID_PROVINSI == 11).ToList();
            ViewData["detailwisata"] = wisataList;
            return View();
        }

        public ActionResult jkt()
        {
            List<TBL_WISATA> wisataList = db.TBL_WISATA.Where(x => x.ID_PROVINSI == 13).ToList();
            ViewData["detailwisata"] = wisataList;
            return View();
        }
        public ActionResult jateng()
        {
            List<TBL_WISATA> wisataList = db.TBL_WISATA.Where(x => x.ID_PROVINSI == 14).ToList();
            ViewData["detailwisata"] = wisataList;
            return View();
        }
        public ActionResult jatim()
        {
            List<TBL_WISATA> wisataList = db.TBL_WISATA.Where(x => x.ID_PROVINSI == 16).ToList();
            ViewData["detailwisata"] = wisataList;
            return View();
        }
        public ActionResult jogja()
        {
            List<TBL_WISATA> wisataList = db.TBL_WISATA.Where(x => x.ID_PROVINSI == 15).ToList();
            ViewData["detailwisata"] = wisataList;
            return View();
        }
    }
}