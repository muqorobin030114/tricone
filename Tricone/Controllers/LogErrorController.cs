﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class LogErrorController : Controller
    {
        // GET: ErrorLog
        DB_TRICONEEntitie dc = new DB_TRICONEEntitie();
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult Getdata()
        {
            List<TBL_EROR_LOG> errorlist = dc.TBL_EROR_LOG.ToList<TBL_EROR_LOG>();
            List<errorOutput> result = new List<errorOutput>();
            foreach (var item in errorlist)
            {
                result.Add(new errorOutput
                {
                    ID_LOG = item.ID_LOG,
                    ID_USER = item.ID_USER,
                    EROR_TIME = item.EROR_TIME,
                    EROR_LINE = item.EROR_LINE,
                    EROR_DESCRIPTION = item.EROR_DESCRIPTION
                });
            }
            return Json(new { data = result.OrderByDescending(e => e.ID_LOG) }, JsonRequestBehavior.AllowGet);
        }
    }
}