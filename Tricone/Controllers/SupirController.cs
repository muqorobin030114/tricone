﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class SupirController : Controller
    {
        // GET: supir
        DB_TRICONEEntitie dc = new DB_TRICONEEntitie();

        public ActionResult Index()
        {
            return View(dc.MS_SUPIR.ToList());
        }

        public ActionResult Getdata()
        {
            List<MS_SUPIR> supirList = dc.MS_SUPIR.ToList<MS_SUPIR>();
            List<SupirOutput> result = new List<SupirOutput>();
            foreach (var item in supirList)
            {
                result.Add(new SupirOutput
                {
                    ID_SUPIR = item.ID_SUPIR,
                    NAMA_SUPIR = item.NAMA_SUPIR,
                    ALAMAT = item.ALAMAT,
                    NO_TELEPON = item.NO_TELEPON
                });
            }

            //var json = Newtonsoft.Json.JsonConvert.SerializeObject(result);
            return Json(new { data = result.OrderByDescending(e => e.ID_SUPIR) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddorEdit(int id = 0)
        {
            if (id == 0)
            {
                return View(new MS_SUPIR());
            }
            else
            {
                return View(dc.MS_SUPIR.Where(x => x.ID_SUPIR == id).FirstOrDefault<MS_SUPIR>());
            }
        }

        [HttpPost]
        public ActionResult AddorEdit(MS_SUPIR supir)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    supir.MODIFIED_DATE = DateTime.Now;
                    if (supir.ID_SUPIR == 0)
                    {
                        supir.CREATED_DATE = DateTime.Now;
                        dc.MS_SUPIR.Add(supir);
                        dc.SaveChanges();

                        //audit trail
                        var sup = dc.MS_SUPIR.OrderByDescending(e => e.ID_SUPIR).FirstOrDefault();
                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                        audit.NAMA_TABEL = "MS_SUPIR";
                        audit.AKSI = "ADD";
                        audit.NAMA_FIELD = "All Field";
                        audit.VALUE_LAMA = "";
                        audit.userid = Session["ID_MEMBER"].ToString();
                        audit.VALUE_BARU = sup.ID_SUPIR + ", " + sup.NAMA_SUPIR + ", " + sup.ALAMAT + ", " + sup.NO_TELEPON;
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();

                        transactions.Commit();
                        return Json(new { success = true, message = "Saved Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var oke = dc.MS_SUPIR.AsNoTracking().Where(x => x.ID_SUPIR == supir.ID_SUPIR).FirstOrDefault();
                        dc.Entry(supir).State = EntityState.Modified;
                        dc.Entry(supir).Property("CREATED_DATE").IsModified = false;
                        dc.SaveChanges();

                        //audit trail

                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                        audit.NAMA_TABEL = "MS_SUPIR";
                        audit.AKSI = "EDIT";
                        audit.userid = Session["ID_MEMBER"].ToString();
                        audit.NAMA_FIELD = "ID_SUPIR" + ", " + "NAMA_SUPIR" + ", " + "ALAMAT" + ", " + "NO_TELEPON";
                        audit.VALUE_LAMA = supir.ID_SUPIR + ", " + oke.NAMA_SUPIR + ", " + oke.ALAMAT + ", " + oke.NO_TELEPON;
                        audit.VALUE_BARU = supir.ID_SUPIR + ", " + supir.NAMA_SUPIR + ", " + supir.ALAMAT + ", " + supir.NO_TELEPON;
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();

                        transactions.Commit();
                        return Json(new { success = true, message = "Updated Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    return Json(new { success = true, message = msgErr.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }
        // GET: Member/Details/5
        public ActionResult Details(int id)
        {
            using (DB_TRICONEEntitie dc = new DB_TRICONEEntitie())
            {
                return View(dc.MS_SUPIR.Where(x => x.ID_SUPIR == id).FirstOrDefault());

            }

        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            MS_SUPIR supir = dc.MS_SUPIR.Where(x => x.ID_SUPIR == id).FirstOrDefault<MS_SUPIR>();
            dc.MS_SUPIR.Remove(supir);
            dc.SaveChanges();
            return Json(new { success = true, message = "Deleted Successfully" }, JsonRequestBehavior.AllowGet);
        }
    }
    public class SupirOutput
    {
        public int ID_SUPIR { get; set; }
        public string NAMA_SUPIR { get; set; }
        public string ALAMAT { get; set; }
        public string NO_TELEPON { get; set; }
    }
}
