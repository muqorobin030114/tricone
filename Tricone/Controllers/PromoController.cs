﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class PromoController : Controller
    {
        // GET: Promo
        DB_TRICONEEntitie dc = new DB_TRICONEEntitie();

        public ActionResult Index()
        {
            return View(dc.TBL_PROMO.ToList());
        }

        public ActionResult Getdata()
        {
            List<TBL_PROMO> provList = dc.TBL_PROMO.ToList<TBL_PROMO>();
            List<PromoOutput> result = new List<PromoOutput>();
            foreach (var item in provList)
            {
                result.Add(new PromoOutput
                {
                    ID_PROMO = item.ID_PROMO,
                    KODE_PROMO = item.KODE_PROMO,
                    PERSEN = Convert.ToInt32(item.PERSEN),
                    FOTO_PROMO = item.FOTO_PROMO,
                    STARTDATE = Convert.ToDateTime(item.STARTDATE).Date,
                    ENDDATE = Convert.ToDateTime(item.ENDDATE).Date
                });
            }

            //var json = Newtonsoft.Json.JsonConvert.SerializeObject(result);
            return Json(new { data = result.OrderByDescending(e => e.ID_PROMO) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddorEdit(int id = 0)
        {
            if (id == 0)
            {
                return View(new TBL_PROMO());
            }
            else
            {
                return View(dc.TBL_PROMO.Where(x => x.ID_PROMO == id).FirstOrDefault<TBL_PROMO>());
            }
        }

        [HttpPost]
        public ActionResult AddorEdit(TBL_PROMO prom, HttpPostedFileBase filebase)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    prom.MODIFIED_DATE = DateTime.Now;
                    if (prom.ID_PROMO == 0)
                    {
                        string fileName = Path.GetFileNameWithoutExtension(filebase.FileName);
                        string extension = Path.GetExtension(filebase.FileName);
                        fileName = fileName + DateTime.Now.ToString("yyyyMMddHHmmssfff") + extension;
                        prom.FOTO_PROMO = fileName;
                        fileName = Path.Combine(Server.MapPath("~/images/"), fileName);
                        filebase.SaveAs(fileName);
                        prom.CREATED_DATE = DateTime.Now;
                        if (filebase.ContentLength <= 1000000)
                        {
                            dc.TBL_PROMO.Add(prom);
                            if (dc.SaveChanges() > 0)
                            {
                                filebase.SaveAs(fileName);

                                //audit trail
                                var qaudit = dc.TBL_PROMO.OrderByDescending(e => e.ID_PROMO).FirstOrDefault();
                                TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                                audit.NAMA_TABEL = "TBL_PROMO";
                                audit.AKSI = "ADD";
                                audit.NAMA_FIELD = "All Field";
                                audit.VALUE_LAMA = "";
                                audit.userid = Session["ID_MEMBER"].ToString();
                                audit.VALUE_BARU = qaudit.ID_PROMO + ", " + qaudit.KODE_PROMO + ", " + qaudit.PERSEN + ", " + qaudit.STARTDATE + ", " + qaudit.ENDDATE + ", " + qaudit.FOTO_PROMO;
                                audit.CREATED_DATE = DateTime.Now;
                                audit.MODIFIED_DATE = DateTime.Now;
                                dc.TBL_AUDIT_TRAIL.Add(audit);
                                dc.SaveChanges();

                                transactions.Commit();
                                return Json(new { success = true, message = "Saved Successfully" }, JsonRequestBehavior.AllowGet);
                                //ViewBag.msg = "Employee Added";
                                //ModelState.Clear();
                            }
                            else
                            {
                                return Json(new { success = true, message = "File Size must be Equal or less than 1mb" }, JsonRequestBehavior.AllowGet);
                                //ViewBag.msg = "File Size must be Equal or less than 1mb";
                            }
                        }
                        else
                        {
                            return Json(new { success = true, message = "Please Try Again With Short Filename" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        var qaudit = dc.TBL_PROMO.AsNoTracking().Where(x => x.ID_PROMO == prom.ID_PROMO).FirstOrDefault();
                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();

                        if (filebase != null)
                        {
                            //delete foto lama
                            string fullPath = Request.MapPath("~/images/" + prom.FOTO_PROMO);
                            if (System.IO.File.Exists(fullPath))
                            {
                                System.IO.File.Delete(fullPath);
                            }

                            //penambahan foto baru
                            string fileName = Path.GetFileNameWithoutExtension(filebase.FileName);
                            string extension = Path.GetExtension(filebase.FileName);
                            fileName = fileName + DateTime.Now.ToString("yyyyMMddHHmmssfff") + extension;
                            //if (kendaraan.FOTO_PROMO==null) { kendaraan.FOTO_PROMO = fileName; }
                            prom.FOTO_PROMO = fileName;
                            fileName = Path.Combine(Server.MapPath("~/images/"), fileName);
                            filebase.SaveAs(fileName);

                            audit.VALUE_LAMA = qaudit.ID_PROMO + ", " + qaudit.KODE_PROMO + ", " + qaudit.PERSEN + ", " + qaudit.STARTDATE + ", " + qaudit.ENDDATE + ", " + qaudit.FOTO_PROMO;
                            audit.VALUE_BARU = prom.ID_PROMO + ", " + prom.KODE_PROMO + ", " + prom.PERSEN + ", " + prom.STARTDATE + ", " + prom.ENDDATE + ", " + prom.FOTO_PROMO;
                        }

                        dc.Entry(prom).State = EntityState.Modified;
                        if (filebase == null) {
                            dc.Entry(prom).Property("FOTO_PROMO").IsModified = false;
                            
                            audit.VALUE_LAMA = qaudit.ID_PROMO + ", " + qaudit.KODE_PROMO + ", " + qaudit.PERSEN + ", " + qaudit.STARTDATE + ", " + qaudit.ENDDATE;
                            audit.VALUE_BARU = prom.ID_PROMO + ", " + prom.KODE_PROMO + ", " + prom.PERSEN + ", " + prom.STARTDATE + ", " + prom.ENDDATE;
                        }
                        dc.Entry(prom).Property("CREATED_DATE").IsModified = false;
                        dc.SaveChanges();

                        //audit trail edit
                        audit.NAMA_TABEL = "TBL_PROMO";
                        audit.AKSI = "EDIT";
                        audit.userid = Session["ID_MEMBER"].ToString();
                        audit.NAMA_FIELD = "";
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();

                        transactions.Commit();
                        return Json(new { success = true, message = "Updated Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    return Json(new { success = true, message = msgErr.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            TBL_PROMO promo = dc.TBL_PROMO.Where(x => x.ID_PROMO== id).FirstOrDefault<TBL_PROMO>();
            dc.TBL_PROMO.Remove(promo);
            dc.SaveChanges();
            return Json(new { success = true, message = "Deleted Successfully" }, JsonRequestBehavior.AllowGet);
        }
    }
    public class PromoOutput
    {
        public int ID_PROMO { get; set; }
        public string KODE_PROMO { get; set; }
        public int PERSEN { get; set; }
        public string FOTO_PROMO { get; set; }
        public Nullable<System.DateTime> STARTDATE { get; set; }
        public Nullable<System.DateTime> ENDDATE { get; set; }
    }
}
