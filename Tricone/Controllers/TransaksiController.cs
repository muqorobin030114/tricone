﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class TransaksiController : Controller
    {
        DB_TRICONEEntitie dc = new DB_TRICONEEntitie();

        // GET: Transaksi

        public ActionResult Index()
        {
            return View(dc.TBL_TRANSAKSI.ToList());
        }

        [HttpGet]
        public JsonResult Getdata()
        {
            List<v_Notreschedule> transaksiList = dc.v_Notreschedule.ToList<v_Notreschedule>();
            List<transaksiOutput> result = new List<transaksiOutput>();
            foreach (var item in transaksiList)
            {
                if (item.APPROVAL != 0 && item.APPROVAL != 1) {
                    result.Add(new transaksiOutput
                    {
                        ID_TRANSAKSI = item.ID_TRANSAKSI,
                        ID_MEMBER = Convert.ToInt32(item.ID_MEMBER),
                        NAMA_MEMBER = item.NAMA_MEMBER,
                        ID_REFUND = Convert.ToInt32(item.ID_REFUND),
                        ID_PEMBAYARAN = Convert.ToInt32(item.ID_PEMBAYARAN),
                        TOTAL_TRANSAKSI = Convert.ToInt32(item.TOTAL_TRANSAKSI),
                        ID_PROMO = Convert.ToInt32(item.ID_PROMO),
                        FOTO_PEMBAYARAN = item.FOTO_PEMBAYARAN,
                        TOTAL_DURASI = Convert.ToInt32(item.TOTAL_DURASI),
                        STATUS_UBAH_JADWAL = Convert.ToInt32(item.STATUS_UBAH_JADWAL),
                        APPROVAL = Convert.ToInt32(item.APPROVAL),
                        KETERANGANUSER = item.KETERANGANUSER,
                        KETERANGANADMIN = item.KETERANGANADMIN,
                        STATUS = Convert.ToInt32(item.STATUS_TRANSAKSI),
                        TGL_PERGI = Convert.ToDateTime(item.TGL_PERGI),
                        TGL_PULANG = Convert.ToDateTime(item.TGL_PULANG)
                    });
                }
            }

            //var json = Newtonsoft.Json.JsonConvert.SerializeObject(result);
            return Json(new { data = result.OrderByDescending(e => e.ID_TRANSAKSI) }, JsonRequestBehavior.AllowGet);
        }

        // GET: Transaksi/Details/5
        [HttpGet]
        public ActionResult Details(int? id)
        {
           
            ViewBag.id_trans = id;
            return View();
        }

        [HttpGet]
        public JsonResult GetdataDetail(int id_trans)
        {
            var a = id_trans;
            List<v_detail_pesan> detailtransaksiList = dc.v_detail_pesan.ToList<v_detail_pesan>();
            List<detailtransaksiOutput> result = new List<detailtransaksiOutput>();
            foreach (var item in detailtransaksiList)
            {
                if (id_trans == item.id_transaksi)
                {
                    result.Add(new detailtransaksiOutput
                    {
                        ID_TRANSAKSI = Convert.ToInt32(item.id_transaksi),
                        id_pemesanan = Convert.ToInt32(item.id_pemesanan),
                        id_member = Convert.ToInt32(item.id_member),
                        nama_member = item.nama_member,
                        telpon_member = item.telpon_member,
                        email_member = item.email_member,
                        id_harga = Convert.ToInt32(item.id_harga),
                        harga = Convert.ToInt32(item.harga),
                        id_kendaraan = Convert.ToInt32(item.id_kendaraan),
                        id_jenis_kendaraan = Convert.ToInt32(item.id_jenis_kendaraan),
                        nama_kendaraan = item.nama_kendaraan,
                        jumlah_seat = Convert.ToInt32(item.jumlah_seat),
                        jumlah_kendaraan = Convert.ToInt32(item.jumlah_kendaraan),
                        status_kendaraan = Convert.ToInt32(item.status_kendaraan),
                        durasi_jam = Convert.ToInt32(item.durasi_jam),
                        id_harilibur = Convert.ToInt32(item.id_harilibur),
                        nama_harilibur = item.nama_harilibur,
                        startdate_libur = Convert.ToDateTime(item.startdate_libur),
                        enddate_libur = Convert.ToDateTime(item.enddate_libur),
                        persen_libur = Convert.ToInt32(item.persen_libur),
                        tgl_pergi = Convert.ToDateTime(item.tgl_pergi),
                        tgl_pulang = Convert.ToDateTime(item.tgl_pulang),
                        id_kota = Convert.ToInt32(item.id_kota),
                        nama_kota = item.nama_kota,
                        id_provinsi = Convert.ToInt32(item.id_provinsi),
                        nama_provinsi = item.nama_provinsi,
                        quantity_kendaraan = Convert.ToInt32(item.quantity_kendaraan),
                        total_pembayaran = Convert.ToInt32(item.total_pembayaran),
                        status_detail_pesan = Convert.ToInt32(item.status_detail_pesan),
                        id_supir = Convert.ToInt32(item.id_supir),
                        nama_supir = item.nama_supir,
                        telpon_supir = item.telpon_supir

                    });
                }
            }

            //var json = Newtonsoft.Json.JsonConvert.Serializ_Object(result);
            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }

        
        public ActionResult Approve(int id)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    var oke = dc.TBL_TRANSAKSI.AsNoTracking().Where(x => x.ID_TRANSAKSI == id).FirstOrDefault();
                    var trx = dc.TBL_TRANSAKSI.Where(x => x.ID_TRANSAKSI == id).FirstOrDefault();
                    trx.STATUS = 1;
                    trx.APPROVAL = 4;
                    trx.MODIFIED_DATE = DateTime.Now;

                    dc.Entry(trx).State = EntityState.Modified;
                    dc.Entry(trx).Property("STATUS").IsModified = true;
                    dc.Entry(trx).Property("APPROVAL").IsModified = true;
                    dc.Entry(trx).Property("MODIFIED_DATE").IsModified = true;
                    dc.SaveChanges();

                    //audit trail edit
                    TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                    audit.NAMA_TABEL = "TBL_TRANSAKSI";
                    audit.AKSI = "EDIT, APPROVED";
                    audit.userid = Session["ID_MEMBER"].ToString();
                    audit.NAMA_FIELD = "";
                    audit.VALUE_LAMA = oke.ID_TRANSAKSI + ", " + oke.STATUS + ", " + oke.APPROVAL;
                    audit.VALUE_BARU = id + ", " + trx.STATUS + ", " + trx.APPROVAL;
                    audit.CREATED_DATE = DateTime.Now;
                    audit.MODIFIED_DATE = DateTime.Now;
                    dc.TBL_AUDIT_TRAIL.Add(audit);
                    dc.SaveChanges();

                    //buat notifikasi
                    notifikasi noti = new notifikasi();
                    noti.NAMA_NOTIF = "transaksi";
                    noti.NOTIF_FOR = trx.ID_MEMBER.ToString();
                    dc.Entry(noti).State = EntityState.Added;
                    dc.SaveChanges();
                    transactions.Commit();
                    return RedirectToAction("Index", "Transaksi");
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    ViewBag.Error = msgErr.Message;
                }
            }
            return null;
        }
        
        public ActionResult Reject(int id)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    var oke = dc.TBL_TRANSAKSI.AsNoTracking().Where(x => x.ID_TRANSAKSI == id).FirstOrDefault();
                    var trx = dc.TBL_TRANSAKSI.Where(x => x.ID_TRANSAKSI == id).FirstOrDefault();
                    trx.APPROVAL = 3;
                    trx.STATUS = 0;
                    trx.MODIFIED_DATE = DateTime.Now;

                    dc.Entry(trx).State = EntityState.Modified;
                    dc.Entry(trx).Property("APPROVAL").IsModified = true;
                    dc.Entry(trx).Property("STATUS").IsModified = true;
                    dc.Entry(trx).Property("MODIFIED_DATE").IsModified = true;
                    dc.SaveChanges();

                    //update status detail
                    var trxdetail = dc.TR_DETAIL_PEMESANAN.Where(x => x.ID_TRANSAKSI == id).FirstOrDefault();
                    trxdetail.STATUS = 0;
                    trxdetail.MODIFIED_DATE = DateTime.Now;
                    dc.Entry(trxdetail).State = EntityState.Modified;
                    dc.Entry(trxdetail).Property("STATUS").IsModified = true;
                    dc.Entry(trxdetail).Property("MODIFIED_DATE").IsModified = true;
                    dc.SaveChanges();

                    //audit trail edit
                    TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                    audit.NAMA_TABEL = "TBL_TRANSAKSI,TR_DETAIL_PEMESANAN";
                    audit.AKSI = "EDIT, REJECTED";
                    audit.userid = Session["ID_MEMBER"].ToString();
                    audit.NAMA_FIELD = "";
                    audit.VALUE_LAMA = oke.ID_TRANSAKSI + ", " + oke.STATUS + ", " + oke.APPROVAL;
                    audit.VALUE_BARU = id + ", " + trx.STATUS + ", " + trx.APPROVAL;
                    audit.CREATED_DATE = DateTime.Now;
                    audit.MODIFIED_DATE = DateTime.Now;
                    dc.TBL_AUDIT_TRAIL.Add(audit);
                    dc.SaveChanges();

                    //buat notifikasi
                    notifikasi noti = new notifikasi();
                    noti.NAMA_NOTIF = "transaksi";
                    noti.NOTIF_FOR = trx.ID_MEMBER.ToString();
                    dc.Entry(noti).State = EntityState.Added;
                    dc.SaveChanges();
                    transactions.Commit();
                    return RedirectToAction("Index", "Transaksi");
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    ViewBag.Error = msgErr.Message;
                }
            }
            return null;
        }
    }
}
