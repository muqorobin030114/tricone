﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class KendaraanController : Controller
    {
        // GET: kendaraan
        DB_TRICONEEntitie dc = new DB_TRICONEEntitie();

        public ActionResult Index()
        {
            return View(dc.MS_KENDARAAN.ToList());
        }

        [HttpGet]
        public JsonResult Getdata()
        {

            List<MS_KENDARAAN> kendaraanlist = dc.MS_KENDARAAN.ToList<MS_KENDARAAN>();
            List<KendaraanOutput> result = new List<KendaraanOutput>();
            foreach (var item in kendaraanlist)
            {
                var deskripsi = "";
                if (item.DESKRIPSI == null) {
                    deskripsi = item.DESKRIPSI;
                }
                else
                {
                    deskripsi = item.DESKRIPSI.Replace("\n", "<br />");
                }
                result.Add(new KendaraanOutput
                {
                    ID_KENDARAAN = item.ID_KENDARAAN,
                    NAMA_KENDARAAN = item.MS_JENIS_KENDARAAN.NAMA_KENDARAAN,
                    ID_JENIS_KENDARAAN = item.MS_JENIS_KENDARAAN.ID_JENIS_KENDARAAN,
                    JUMLAH_SEAT = Convert.ToInt32(item.JUMLAH_SEAT),
                    JUMLAH_KENDARAAN = Convert.ToInt32(item.JUMLAH_KENDARAAN),
                    DESKRIPSI = deskripsi,
                    fasilitas = item.FASILITAS,
                    pathFoto = item.pathFoto,
                    STATUS = Convert.ToInt32(item.STATUS)
                });
            }
            
            return Json(new { data = result.OrderByDescending(e => e.ID_KENDARAAN) }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult AddorEdit(int id = 0)
        {
            MS_KENDARAAN Kendaraanmodel = new MS_KENDARAAN();
            Kendaraanmodel.kendcollection = dc.MS_JENIS_KENDARAAN.ToList<MS_JENIS_KENDARAAN>();

            if (id == 0)
            {
                return View(Kendaraanmodel);
            }
            else
            {
                var kendaraan = dc.MS_KENDARAAN.Find(id);
                Session["imgPath"] = kendaraan.pathFoto;
                if (kendaraan == null)
                {
                    return HttpNotFound();
                }
                Kendaraanmodel = dc.MS_KENDARAAN.Where(x => x.ID_KENDARAAN == id).FirstOrDefault();
                Kendaraanmodel.kendcollection = dc.MS_JENIS_KENDARAAN.ToList<MS_JENIS_KENDARAAN>();
                return View(Kendaraanmodel);
            }
        }
        
        [HttpPost]
        public ActionResult AddorEdit(MS_KENDARAAN kendaraan, HttpPostedFileBase filebase)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    kendaraan.MODIFIED_DATE = DateTime.Now;
                    if (kendaraan.ID_KENDARAAN == 0)
                    {
                        string fileName = Path.GetFileNameWithoutExtension(filebase.FileName);
                        string extension = Path.GetExtension(filebase.FileName);
                        fileName = fileName + DateTime.Now.ToString("yyyyMMddHHmmssfff") + extension;
                        kendaraan.pathFoto = fileName;
                        fileName = Path.Combine(Server.MapPath("~/images/"), fileName);
                        filebase.SaveAs(fileName);
                        kendaraan.CREATED_DATE = DateTime.Now;
                        if (filebase.ContentLength <= 1000000)
                        {
                            dc.MS_KENDARAAN.Add(kendaraan);
                            if (dc.SaveChanges() > 0)
                            {
                                filebase.SaveAs(fileName);

                                //audit trail
                                var qaudit = dc.MS_KENDARAAN.OrderByDescending(e => e.ID_KENDARAAN).FirstOrDefault();
                                TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                                audit.NAMA_TABEL = "MS_KENDARAAN";
                                audit.AKSI = "ADD";
                                audit.NAMA_FIELD = "All Field";
                                audit.VALUE_LAMA = "";
                                audit.userid = Session["ID_MEMBER"].ToString();
                                audit.VALUE_BARU = qaudit.ID_KENDARAAN + ", " + qaudit.ID_JENIS_KENDARAAN + ", " + qaudit.JUMLAH_SEAT + ", " + qaudit.JUMLAH_KENDARAAN + ", " + qaudit.STATUS + ", " + qaudit.pathFoto + ", " + qaudit.FASILITAS + ", " + qaudit.DESKRIPSI;
                                audit.CREATED_DATE = DateTime.Now;
                                audit.MODIFIED_DATE = DateTime.Now;
                                dc.TBL_AUDIT_TRAIL.Add(audit);
                                dc.SaveChanges();

                                transactions.Commit();
                                return Json(new { success = true, message = "Saved Successfully" }, JsonRequestBehavior.AllowGet);
                                //ViewBag.msg = "Employee Added";
                                //ModelState.Clear();
                            }
                            else
                            {
                                return Json(new { success = true, message = "File Size must be Equal or less than 1mb" }, JsonRequestBehavior.AllowGet);
                                //ViewBag.msg = "File Size must be Equal or less than 1mb";
                            }
                        }
                        else
                        {
                            return Json(new { success = true, message = "Please Try Again With Short File Name" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        if (filebase!=null)
                        {
                            //delete foto lama
                            string fullPath = Request.MapPath("~/images/" + kendaraan.pathFoto);
                            if (System.IO.File.Exists(fullPath))
                            {
                                System.IO.File.Delete(fullPath);
                            }

                            //penambahan foto baru
                            string fileName = Path.GetFileNameWithoutExtension(filebase.FileName);
                            string extension = Path.GetExtension(filebase.FileName);
                            fileName = fileName + DateTime.Now.ToString("yyyyMMddHHmmssfff") + extension;
                            //if (kendaraan.pathFoto==null) { kendaraan.pathFoto = fileName; }
                            kendaraan.pathFoto = fileName;
                            fileName = Path.Combine(Server.MapPath("~/images/"), fileName);
                            filebase.SaveAs(fileName);
                        }
                        var oke = dc.MS_KENDARAAN.AsNoTracking().Where(x => x.ID_KENDARAAN == kendaraan.ID_KENDARAAN).FirstOrDefault();
                        dc.Entry(kendaraan).State = EntityState.Modified;
                        if (filebase == null){ dc.Entry(kendaraan).Property("pathFoto").IsModified = false; }
                        dc.Entry(kendaraan).Property("CREATED_DATE").IsModified = false;
                        dc.SaveChanges();

                        //audit trail edit
                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                        audit.NAMA_TABEL = "MS_KENDARAAN";
                        audit.AKSI = "EDIT";
                        audit.userid = Session["ID_MEMBER"].ToString();
                        audit.NAMA_FIELD = "NAMA_BANK";
                        audit.VALUE_LAMA = oke.ID_KENDARAAN + ", " + oke.ID_JENIS_KENDARAAN + ", " + oke.JUMLAH_SEAT + ", " + oke.JUMLAH_KENDARAAN + ", " + oke.STATUS + ", " + oke.pathFoto + ", " + oke.FASILITAS + ", " + oke.DESKRIPSI;
                        audit.VALUE_BARU = kendaraan.ID_KENDARAAN + ", " + kendaraan.ID_JENIS_KENDARAAN + ", " + kendaraan.JUMLAH_SEAT + ", " + kendaraan.JUMLAH_KENDARAAN + ", " + kendaraan.STATUS + ", " + kendaraan.pathFoto + ", " + kendaraan.FASILITAS + ", " + kendaraan.DESKRIPSI;
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();

                        transactions.Commit();
                        return Json(new { success = true, message = "Updated Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    return Json(new { success = true, message = msgErr.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }


        [HttpPost]
        public ActionResult Delete(int id)
        {
            MS_KENDARAAN kendaraan = dc.MS_KENDARAAN.Where(x => x.ID_KENDARAAN == id).FirstOrDefault<MS_KENDARAAN>();
            dc.MS_KENDARAAN.Remove(kendaraan);
            dc.SaveChanges();
            return Json(new { success = true, message = "Deleted Successfully" }, JsonRequestBehavior.AllowGet);
        }
    }

    public class KendaraanOutput
    {
        public int ID_KENDARAAN { get; set; }
        public string NAMA_KENDARAAN { get; set; }
        public int ID_JENIS_KENDARAAN { get; set; }
        public int JUMLAH_SEAT { get; set; }
        public int JUMLAH_KENDARAAN { get; set; }
        public int STATUS { get; set; }
        public string pathFoto { get; set; }
        [AllowHtml]
        public string DESKRIPSI { get; set; }
        [AllowHtml]
        public string fasilitas { get; set; }
    }
}
