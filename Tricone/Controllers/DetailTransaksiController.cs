﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class DetailTransaksiController : Controller
    {
        // GET: DetailTransaksi
        
        DB_TRICONEEntitie dc = new DB_TRICONEEntitie();

        public ActionResult Index()
        {
            return View(dc.TR_DETAIL_PEMESANAN.ToList());
        }

        [HttpGet]
        public JsonResult Getdata()
        {
            List<TR_DETAIL_PEMESANAN> detailList = dc.TR_DETAIL_PEMESANAN.ToList<TR_DETAIL_PEMESANAN>();
            List<DetailOutput> result = new List<DetailOutput>();
            foreach (var item in detailList)
            {
                result.Add(new DetailOutput
                {
                    ID_PEMESANAN = item.ID_PEMESANAN,
                    ID_MEMBER = Convert.ToInt32(item.ID_MEMBER),
                    ID_HARGA = Convert.ToInt32(item.ID_HARGA),
                    ID_HARILIBUR = Convert.ToInt32(item.ID_HARILIBUR),
                    NAMA_HARILIBUR = item.MS_HARILIBUR.NAMA_HARILIBUR,
                    ID_SUPIR = Convert.ToInt32(item.ID_SUPIR),
                    NAMA_SUPIR = item.MS_SUPIR.NAMA_SUPIR,
                    TGL_PERGI = Convert.ToDateTime(item.TGL_PERGI),
                    TGL_PULANG = Convert.ToDateTime(item.TGL_PULANG),
                    ID_KOTA = Convert.ToInt32(item.ID_KOTA),
                    QUANTITY_KENDARAAN = Convert.ToInt32(item.QUANTITY_KENDARAAN),
                    TOTAL_PEMBAYARAN = Convert.ToInt32(item.TOTAL_PEMBAYARAN),
                    STATUS = Convert.ToInt32(item.STATUS),
                });
            }
            
            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddorEdit(int id = 0)
        {
            if (id == 0)
            {
                return View(new TR_DETAIL_PEMESANAN());
            }
            else
            {
                return View(dc.TR_DETAIL_PEMESANAN.Where(x => x.ID_PEMESANAN == id).FirstOrDefault<TR_DETAIL_PEMESANAN>());
            }
        }

        [HttpPost]
        public ActionResult AddorEdit(TR_DETAIL_PEMESANAN detailpesan)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    detailpesan.MODIFIED_DATE = DateTime.Now;
                    if (detailpesan.ID_HARILIBUR == 0)
                    {
                        detailpesan.CREATED_DATE = DateTime.Now;
                        dc.TR_DETAIL_PEMESANAN.Add(detailpesan);
                        dc.SaveChanges();
                        transactions.Commit();
                        return Json(new { success = true, message = "Saved Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        dc.Entry(detailpesan).State = EntityState.Modified;
                        dc.Entry(detailpesan).Property("CREATED_DATE").IsModified = false;
                        dc.SaveChanges();
                        transactions.Commit();
                        return Json(new { success = true, message = "Updated Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();
                    transactions.Rollback();
                    return Json(new { success = true, message = msgErr.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            TR_DETAIL_PEMESANAN detailpesan = dc.TR_DETAIL_PEMESANAN.Where(x => x.ID_PEMESANAN == id).FirstOrDefault<TR_DETAIL_PEMESANAN>();
            dc.TR_DETAIL_PEMESANAN.Remove(detailpesan);
            dc.SaveChanges();
            return Json(new { success = true, message = "Deleted Successfully" }, JsonRequestBehavior.AllowGet);
        }
    }
    public class DetailOutput
    {
        public int ID_PEMESANAN { get; set; }
        public int ID_MEMBER { get; set; }
        public int ID_HARGA { get; set; }
        public int ID_HARILIBUR { get; set; }
        public string NAMA_HARILIBUR { get; set; }
        public DateTime TGL_PERGI { get; set; }
        public DateTime TGL_PULANG { get; set; }
        public int ID_KOTA { get; set; }
        public int QUANTITY_KENDARAAN { get; set; }
        public int TOTAL_PEMBAYARAN { get; set; }
        public int STATUS { get; set; }
        public int ID_SUPIR { get; set; }
        public string NAMA_SUPIR { get; set; }
    }
}