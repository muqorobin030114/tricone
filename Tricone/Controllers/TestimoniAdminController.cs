﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;
using System.Net;
using System.Net.Mail;

namespace Tricone.Controllers
{
    public class TestimoniAdminController : Controller
    {
        // GET: TestimoniAdmin
        DB_TRICONEEntitie dc = new DB_TRICONEEntitie();

        public ActionResult Index()
        {
            return View(dc.TBL_TESTIMONI.ToList());
        }

        [HttpGet]
        public JsonResult Getdata()
        {
            List<TBL_TESTIMONI> testimonilist = dc.TBL_TESTIMONI.ToList<TBL_TESTIMONI>();
            List<TestimoniOutput> result = new List<TestimoniOutput>();
            foreach (var item in testimonilist)
            {
                result.Add(new TestimoniOutput
                {
                    ID_TESTIMONI = item.ID_TESTIMONI,
                    EMAIL_FROM = item.EMAIL_FROM,
                    NAMA = item.NAMA,
                    EMAIL_TO = item.EMAIL_TO,
                    SUBJECT = item.SUBJECT,
                    MESSAGE = item.MESSAGE,
                    STATUS = item.STATUS
                });
            }

            //var json = Newtonsoft.Json.JsonConvert.SerializeObject(result);
            return Json(new { data = result.OrderByDescending(e => e.ID_TESTIMONI) }, JsonRequestBehavior.AllowGet);
        }
        // GET: TestimoniUser
        public ActionResult Testimoni()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Testimoni(TBL_TESTIMONI model)
        {
            try
            {
                if (model.EMAIL_FROM != null || model.MESSAGE != null || model.SUBJECT != null)
                {
                    TBL_TESTIMONI testi = new TBL_TESTIMONI();
                    testi.EMAIL_FROM = model.EMAIL_FROM;
                    testi.NAMA = model.NAMA;
                    testi.SUBJECT = model.SUBJECT;
                    testi.MESSAGE = model.MESSAGE;
                    testi.CREATED_DATE = DateTime.Now;
                    testi.MODIFIED_DATE = DateTime.Now;
                    testi.STATUS = 0;

                    dc.Entry(testi).State=EntityState.Added;
                    dc.SaveChanges();

                    MailMessage mm = new MailMessage("traveltricone@gmail.com", "traveltricone@gmail.com");
                    mm.Subject = model.SUBJECT;
                    mm.Body = " Hello I am " +model.NAMA+ " my email " + model.EMAIL_FROM + '\n' + model.MESSAGE;
                    mm.IsBodyHtml = false;

                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.EnableSsl = true;

                    NetworkCredential nc = new NetworkCredential("traveltricone@gmail.com", "tangerang");
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = nc;
                    smtp.Send(mm);
                    ViewBag.Message = "Mail Has Been Sent Succesfully!";

                    return View();
                }
                return View();
            }
            catch (Exception msgErr)
            {
                //error log
                DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                if (Session["ID_MEMBER"].ToString() != null)
                {
                    logerror.ID_USER = Session["ID_MEMBER"].ToString();
                }
                logerror.EROR_TIME = DateTime.Now;
                logerror.EROR_DESCRIPTION = msgErr.Message;
                logerror.CREATED_DATE = DateTime.Now;
                logerror.MODIFIED_DATE = DateTime.Now;
                logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                dc2.TBL_EROR_LOG.Add(logerror);
                dc2.SaveChanges();

                ViewBag.Message = msgErr.Message;
                return View();
            }
        }
        
        [HttpPost]
        public ActionResult TurnOnOff(int id)
        {
            try
            {
                var oke = dc.TBL_TESTIMONI.AsNoTracking().Where(x => x.ID_TESTIMONI == id).FirstOrDefault();
                TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                var testimon = dc.TBL_TESTIMONI.Where(a => a.ID_TESTIMONI == id).FirstOrDefault();
                if (testimon.STATUS == 0)
                {
                    testimon.STATUS = 1;
                    audit.VALUE_BARU = id + ", 1";
                }
                else
                {
                    testimon.STATUS = 0;
                    audit.VALUE_BARU = id + ", 0";
                }

                testimon.MODIFIED_DATE = DateTime.Now;
                dc.Entry(testimon).State = EntityState.Modified;
                dc.Entry(testimon).Property("STATUS").IsModified = true;
                dc.Entry(testimon).Property("MODIFIED_DATE").IsModified = true;
                dc.SaveChanges();

                //audit trail edit
                audit.NAMA_TABEL = "TBL_TESTIMONI";
                audit.AKSI = "EDIT";
                audit.userid = Session["ID_MEMBER"].ToString();
                audit.NAMA_FIELD = "";
                audit.VALUE_LAMA = oke.ID_TESTIMONI + ", " + oke.STATUS;
                audit.CREATED_DATE = DateTime.Now;
                audit.MODIFIED_DATE = DateTime.Now;
                dc.TBL_AUDIT_TRAIL.Add(audit);
                dc.SaveChanges();

                return Json(new { success = true, message = "Changed Successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception msgErr)
            {
                //error log
                DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                if (Session["ID_MEMBER"].ToString() != null)
                {
                    logerror.ID_USER = Session["ID_MEMBER"].ToString();
                }
                logerror.EROR_TIME = DateTime.Now;
                logerror.EROR_DESCRIPTION = msgErr.Message;
                logerror.CREATED_DATE = DateTime.Now;
                logerror.MODIFIED_DATE = DateTime.Now;
                logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                dc2.TBL_EROR_LOG.Add(logerror);
                dc2.SaveChanges();

                ViewBag.Message = msgErr.Message;
                return View();
            }
        }




        public class TestimoniOutput
        {
            public int ID_TESTIMONI { get; set; }
            public string EMAIL_FROM { get; set; }
            public string NAMA { get; set; }
            public string EMAIL_TO { get; set; }
            public string SUBJECT { get; set; }
            public string MESSAGE { get; set; }
            public Nullable<int> STATUS { get; set; }
        }
    }
}