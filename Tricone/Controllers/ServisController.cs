﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;
namespace Tricone.Controllers
{
    public class ServisController : Controller
    {
        DB_TRICONEEntitie dc = new DB_TRICONEEntitie();
        // GET: DetailServis
        public ActionResult Index()
        {
            return View(dc.TBL_DETAILSERVIS.ToList());
        }

        [HttpGet]
        public JsonResult Getdata()
        {

            List<TBL_DETAILSERVIS> kendaraanlist = dc.TBL_DETAILSERVIS.ToList<TBL_DETAILSERVIS>();
            List<servisOutput> result = new List<servisOutput>();
            foreach (var item in kendaraanlist)
            {
                result.Add(new servisOutput
                {
                    ID_DETAILSERVIS = item.ID_DETAILSERVIS,
                    NAMA_SERVIS = item.NAMA_SERVIS,
                    DESKRIPSI = item.DESKRIPSI.Replace("\n", "<br />"),
                    FOTO_DESKRIPSI = item.FOTO_DESKRIPSI
                });
            }

            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult AddorEdit(int id = 0)
        {
            TBL_DETAILSERVIS servismodel = new TBL_DETAILSERVIS();
            //Kendaraanmodel.kendcollection = dc.MS_JENIS_KENDARAAN.ToList<MS_JENIS_KENDARAAN>();

            if (id == 0)
            {
                return View(servismodel);
            }
            else
            {
                var servise = dc.TBL_DETAILSERVIS.Find(id);
                Session["imgPath"] = servise.FOTO_DESKRIPSI;
                if (servise == null)
                {
                    return HttpNotFound();
                }
                servismodel = dc.TBL_DETAILSERVIS.Where(x => x.ID_DETAILSERVIS== id).FirstOrDefault();
                //servismodel.kendcollection = dc.TBL_DETAILSERVIS.ToList<TBL_DETAILSERVIS>();
                return View(servismodel);
            }
        }

        [HttpPost]
        public ActionResult AddorEdit(TBL_DETAILSERVIS service, HttpPostedFileBase filebase)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    service.MODIFIED_DATE = DateTime.Now;
                    if (service.ID_DETAILSERVIS == 0)
                    {
                        string fileName = Path.GetFileNameWithoutExtension(filebase.FileName);
                        string extension = Path.GetExtension(filebase.FileName);
                        fileName = fileName + DateTime.Now.ToString("yyyyMMddHHmmssfff") + extension;
                        service.FOTO_DESKRIPSI = fileName;
                        fileName = Path.Combine(Server.MapPath("~/images/"), fileName);
                        filebase.SaveAs(fileName);
                        service.CREATED_DATE = DateTime.Now;
                        if (filebase.ContentLength <= 1000000)
                        {
                            dc.TBL_DETAILSERVIS.Add(service);
                            if (dc.SaveChanges() > 0)
                            {
                                filebase.SaveAs(fileName);
                                transactions.Commit();
                                return Json(new { success = true, message = "Saved Successfully" }, JsonRequestBehavior.AllowGet);
                                //ViewBag.msg = "Employee Added";
                                //ModelState.Clear();
                            }
                            else
                            {
                                return Json(new { success = true, message = "File Size must be Equal or less than 1mb" }, JsonRequestBehavior.AllowGet);
                                //ViewBag.msg = "File Size must be Equal or less than 1mb";
                            }
                        }
                        else
                        {
                            return Json(new { success = true, message = "Saved Successfully" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        if (filebase != null)
                        {
                            //delete foto lama
                            string fullPath = Request.MapPath("~/images/" + service.FOTO_DESKRIPSI);
                            if (System.IO.File.Exists(fullPath))
                            {
                                System.IO.File.Delete(fullPath);
                            }

                            //penambahan foto baru
                            string fileName = Path.GetFileNameWithoutExtension(filebase.FileName);
                            string extension = Path.GetExtension(filebase.FileName);
                            fileName = fileName + DateTime.Now.ToString("yyyyMMddHHmmssfff") + extension;
                            //if (kendaraan.pathFoto==null) { kendaraan.pathFoto = fileName; }
                            service.FOTO_DESKRIPSI = fileName;
                            fileName = Path.Combine(Server.MapPath("~/images/"), fileName);
                            filebase.SaveAs(fileName);
                        }

                        dc.Entry(service).State = EntityState.Modified;
                        if (filebase == null) { dc.Entry(service).Property("pathFoto").IsModified = false; }
                        dc.Entry(service).Property("CREATED_DATE").IsModified = false;
                        dc.SaveChanges();
                        transactions.Commit();
                        return Json(new { success = true, message = "Updated Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    return Json(new { success = true, message = msgErr.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }


        [HttpPost]
        public ActionResult Delete(int id)
        {
            TBL_DETAILSERVIS services = dc.TBL_DETAILSERVIS.Where(x => x.ID_DETAILSERVIS == id).FirstOrDefault<TBL_DETAILSERVIS>();
            dc.TBL_DETAILSERVIS.Remove(services);
            dc.SaveChanges();
            return Json(new { success = true, message = "Deleted Successfully" }, JsonRequestBehavior.AllowGet);
        }
    }

    public class servisOutput
    {
        public int ID_DETAILSERVIS { get; set; }
        public string NAMA_SERVIS { get; set; }
        [AllowHtml]
        public string DESKRIPSI { get; set; }
        public string FOTO_DESKRIPSI { get; set; }
    }
}