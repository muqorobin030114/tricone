﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class ProvinsiController : Controller
    {
        // GET: Provinsi
        DB_TRICONEEntitie dc = new DB_TRICONEEntitie();

        public ActionResult Index()
        {
            ViewBag.Message = "master";
            return View(dc.MS_PROVINSI.ToList());
        }

        /*public ActionResult Getdata()
        {
            List<MS_PROVINSI> provList = dc.MS_PROVINSI.ToList<MS_PROVINSI>();
            return Json(new { data = provList }, JsonRequestBehavior.AllowGet);
        }*/
        [HttpGet]
        public JsonResult Getdata()
        {
            List<MS_PROVINSI> provList = dc.MS_PROVINSI.ToList<MS_PROVINSI>();
            List<ProvOutput> result = new List<ProvOutput>();
            foreach (var item in provList)
            {
                result.Add(new ProvOutput
                {
                    ID_PROVINSI = item.ID_PROVINSI,
                    NAMA_PROVINSI = item.NAMA_PROVINSI
                });
            }

            //var json = Newtonsoft.Json.JsonConvert.SerializeObject(result);
            return Json(new { data = result.OrderByDescending(e=>e.ID_PROVINSI) }, JsonRequestBehavior.AllowGet);
            
        }

        [HttpGet]
        public ActionResult AddorEdit(int id = 0)
        {
            if (id == 0)
            {
                return View(new MS_PROVINSI());
            }
            else
            {
                return View(dc.MS_PROVINSI.Where(x => x.ID_PROVINSI == id).FirstOrDefault<MS_PROVINSI>());
            }
        }

        [HttpPost]
        public ActionResult AddorEdit(MS_PROVINSI prov)
        {
            using (var transactions= dc.Database.BeginTransaction())
            {
                try
                {
                    prov.MODIFIED_DATE = DateTime.Now;
                    if (prov.ID_PROVINSI == 0)
                    {
                        prov.CREATED_DATE = DateTime.Now;
                        dc.MS_PROVINSI.Add(prov);
                        dc.SaveChanges();

                        //audit trail
                        var provin = dc.MS_PROVINSI.OrderByDescending(e => e.ID_PROVINSI).FirstOrDefault();
                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                        audit.NAMA_TABEL = "MS_PROVINSI";
                        audit.AKSI = "ADD";
                        audit.NAMA_FIELD = "All Field";
                        audit.VALUE_LAMA = "";
                        audit.userid = Session["ID_MEMBER"].ToString();
                        audit.VALUE_BARU = provin.ID_PROVINSI+", "+ prov.NAMA_PROVINSI;
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();

                        transactions.Commit();
                        return Json(new { success = true, message = "Saved Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var ads = dc.MS_PROVINSI.AsNoTracking().Where(x => x.ID_PROVINSI == prov.ID_PROVINSI).FirstOrDefault();
                        dc.Entry(prov).State = EntityState.Modified;
                        dc.Entry(prov).Property("CREATED_DATE").IsModified = false;
                        dc.SaveChanges();
                        //audit trail
                        
                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                        audit.NAMA_TABEL = "MS_PROVINSI";
                        audit.AKSI = "EDIT";
                        audit.userid = Session["ID_MEMBER"].ToString();
                        audit.NAMA_FIELD = "NAMA_PROVINSI";
                        audit.VALUE_LAMA = prov.ID_PROVINSI + ", " + ads.NAMA_PROVINSI;
                        audit.VALUE_BARU = prov.ID_PROVINSI + ", " + prov.NAMA_PROVINSI;
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();

                        transactions.Commit();
                        return Json(new { success = true, message = "Updated Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    return Json(new { success = true, message = msgErr.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            MS_PROVINSI prov = dc.MS_PROVINSI.Where(x => x.ID_PROVINSI == id).FirstOrDefault<MS_PROVINSI>();
            dc.MS_PROVINSI.Remove(prov);
            dc.SaveChanges();
            return Json(new { success = true, message = "Deleted Successfully" }, JsonRequestBehavior.AllowGet);
        }
    }

    public class ProvOutput
    {
        public int ID_PROVINSI { get; set; }
        public string NAMA_PROVINSI { get; set; }
    }
}