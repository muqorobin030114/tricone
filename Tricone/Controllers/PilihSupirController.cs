﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;
using System.Web.Script.Serialization;

namespace Tricone.Controllers
{
    public class PilihSupirController : Controller
    {
        // GET: PilihSupir
        DB_TRICONEEntitie dc = new DB_TRICONEEntitie();
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult Getdata()
        {
            List<v_transaksi> transaksiList = dc.v_transaksi.ToList<v_transaksi>();
            List<transaksiOutput> result = new List<transaksiOutput>();
            foreach (var item in transaksiList)
            {
                var qtrx = (from dtl in dc.TR_DETAIL_PEMESANAN
                            join trs in dc.TBL_TRANSAKSI on dtl.ID_TRANSAKSI equals trs.ID_TRANSAKSI
                            where trs.STATUS == 1 && trs.APPROVAL == 4 && trs.ID_TRANSAKSI == item.ID_TRANSAKSI && !(from o in dc.detail_supir select o.ID_PEMESANAN).Contains(dtl.ID_PEMESANAN)
                            group dtl by new { dtl.ID_TRANSAKSI } into grp
                            select new
                            {
                                ID_TRANSAKSI = grp.Key.ID_TRANSAKSI,
                                pcount = grp.Count()
                            });
                if (qtrx.Count() > 0)
                {
                    result.Add(new transaksiOutput
                    {
                        ID_TRANSAKSI = item.ID_TRANSAKSI,
                        ID_MEMBER = Convert.ToInt32(item.ID_MEMBER),
                        NAMA_MEMBER = item.NAMA_MEMBER,
                        ID_REFUND = Convert.ToInt32(item.ID_REFUND),
                        ID_PEMBAYARAN = Convert.ToInt32(item.ID_PEMBAYARAN),
                        TOTAL_TRANSAKSI = Convert.ToInt32(item.TOTAL_TRANSAKSI),
                        ID_PROMO = Convert.ToInt32(item.ID_PROMO),
                        FOTO_PEMBAYARAN = item.FOTO_PEMBAYARAN,
                        TOTAL_DURASI = Convert.ToInt32(item.TOTAL_DURASI),
                        STATUS_UBAH_JADWAL = Convert.ToInt32(item.STATUS_UBAH_JADWAL),
                        APPROVAL = Convert.ToInt32(item.APPROVAL),
                        KETERANGANUSER = item.KETERANGANUSER,
                        KETERANGANADMIN = item.KETERANGANADMIN,
                        STATUS = Convert.ToInt32(item.STATUS_TRANSAKSI),
                        TGL_PERGI = Convert.ToDateTime(item.TGL_PERGI),
                        TGL_PULANG = Convert.ToDateTime(item.TGL_PULANG)
                    });
                }
            }

            //var json = Newtonsoft.Json.JsonConvert.SerializeObject(result);
            return Json(new { data = result.OrderByDescending(e => e.ID_TRANSAKSI) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Details(int? id)
        {

            ViewBag.id_trans = id;
            return View();
        }

        [HttpGet]
        public JsonResult GetdataDetail(int id_trans)
        {
            var a = id_trans;
            List<v_detail_pesan> detailtransaksiList = dc.v_detail_pesan.ToList<v_detail_pesan>();
            List<detailtransaksiOutput> result = new List<detailtransaksiOutput>();
            foreach (var item in detailtransaksiList)
            {
                if (id_trans == item.id_transaksi)
                {
                    result.Add(new detailtransaksiOutput
                    {
                        ID_TRANSAKSI = Convert.ToInt32(item.id_transaksi),
                        id_pemesanan = Convert.ToInt32(item.id_pemesanan),
                        id_member = Convert.ToInt32(item.id_member),
                        nama_member = item.nama_member,
                        telpon_member = item.telpon_member,
                        email_member = item.email_member,
                        id_harga = Convert.ToInt32(item.id_harga),
                        harga = Convert.ToInt32(item.harga),
                        id_kendaraan = Convert.ToInt32(item.id_kendaraan),
                        id_jenis_kendaraan = Convert.ToInt32(item.id_jenis_kendaraan),
                        nama_kendaraan = item.nama_kendaraan,
                        jumlah_seat = Convert.ToInt32(item.jumlah_seat),
                        jumlah_kendaraan = Convert.ToInt32(item.jumlah_kendaraan),
                        status_kendaraan = Convert.ToInt32(item.status_kendaraan),
                        durasi_jam = Convert.ToInt32(item.durasi_jam),
                        id_harilibur = Convert.ToInt32(item.id_harilibur),
                        nama_harilibur = item.nama_harilibur,
                        startdate_libur = Convert.ToDateTime(item.startdate_libur),
                        enddate_libur = Convert.ToDateTime(item.enddate_libur),
                        persen_libur = Convert.ToInt32(item.persen_libur),
                        tgl_pergi = Convert.ToDateTime(item.tgl_pergi),
                        tgl_pulang = Convert.ToDateTime(item.tgl_pulang),
                        id_kota = Convert.ToInt32(item.id_kota),
                        nama_kota = item.nama_kota,
                        id_provinsi = Convert.ToInt32(item.id_provinsi),
                        nama_provinsi = item.nama_provinsi,
                        quantity_kendaraan = Convert.ToInt32(item.quantity_kendaraan),
                        total_pembayaran = Convert.ToInt32(item.total_pembayaran),
                        status_detail_pesan = Convert.ToInt32(item.status_detail_pesan),
                        id_supir = Convert.ToInt32(item.id_supir),
                        nama_supir = item.nama_supir,
                        telpon_supir = item.telpon_supir

                    });
                }
            }
            
            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddorEdit(int id)
        {

            detail_supir detailsupirmodel = new detail_supir();
            detailsupirmodel.detailcollection = dc.v_detail_pesan.ToList<v_detail_pesan>();
            detailsupirmodel.supircollection = dc.MS_SUPIR.ToList<MS_SUPIR>();
            ViewBag.idtrans = id;
            return View(detailsupirmodel);
        }

        [HttpPost]
        public ActionResult AddorEdit(object sender, EventArgs e,FormCollection fomc, detail_supir detiiiil)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    string[] textboxValues = Request.Form.GetValues("ID_SUPIR");
                    int a = 0;

                    //validasi nama driver sama
                    foreach (string textboxValue in textboxValues)
                    {
                        int hitungin = 0;
                        foreach (string textboxValue2 in textboxValues)
                        {
                            if (textboxValue == textboxValue2)
                            {
                                hitungin++;
                                if (hitungin > 1)
                                {
                                    return Json(new { success = true, message = "Please Select Other Driver" }, JsonRequestBehavior.AllowGet);
                                }
                            }
                        }
                    }

                    var cekjumlahmobil = dc.TR_DETAIL_PEMESANAN.AsNoTracking().Where(x => x.ID_PEMESANAN == detiiiil.ID_PEMESANAN).FirstOrDefault();
                    var hitung = textboxValues.Count();
                    if (textboxValues.Count() < cekjumlahmobil.QUANTITY_KENDARAAN)
                    {
                        return Json(new { success = true, message = "Not Enough Driver" }, JsonRequestBehavior.AllowGet);
                    }
                    else {
                        var cekeditdriver = dc.detail_supir.AsNoTracking().Where(x => x.ID_PEMESANAN == detiiiil.ID_PEMESANAN).Count();
                        if (cekeditdriver > 0)
                        {
                            var oke = dc.detail_supir.AsNoTracking().Where(x => x.ID_PEMESANAN == detiiiil.ID_PEMESANAN).FirstOrDefault();
                            dc.detail_supir.RemoveRange(dc.detail_supir.Where(x => x.ID_PEMESANAN == detiiiil.ID_PEMESANAN));
                            dc.SaveChanges();

                            foreach (string textboxValue in textboxValues)
                            {
                                detail_supir detsup = new detail_supir();
                                detsup.ID_PEMESANAN = detiiiil.ID_PEMESANAN;
                                detsup.ID_SUPIR = Convert.ToInt32(textboxValue);
                                var nama = dc.MS_SUPIR.AsNoTracking().Where(x => x.ID_SUPIR == detsup.ID_SUPIR).FirstOrDefault();
                                detsup.NAMA_SUPIR = nama.NAMA_SUPIR;
                                if (a < cekeditdriver)
                                {
                                    detsup.UTAMA = 1;
                                }
                                detsup.CREATED_DATE = DateTime.Now;
                                detsup.MODIFIED_DATE = DateTime.Now;
                                detsup.TGL_PERGI = cekjumlahmobil.TGL_PERGI;
                                detsup.TGL_PULANG = cekjumlahmobil.TGL_PULANG;
                                dc.detail_supir.Add(detsup);
                                dc.SaveChanges();
                                a++;
                            }

                            //audit trail edit
                            TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                            audit.NAMA_TABEL = "detail_supir";
                            audit.AKSI = "EDIT, INSERT DRIVER";
                            audit.userid = Session["ID_MEMBER"].ToString();
                            audit.NAMA_FIELD = "";
                            audit.VALUE_LAMA = detiiiil.ID_PEMESANAN.ToString();
                            audit.VALUE_BARU = detiiiil.ID_PEMESANAN.ToString();
                            audit.CREATED_DATE = DateTime.Now;
                            audit.MODIFIED_DATE = DateTime.Now;
                            dc.TBL_AUDIT_TRAIL.Add(audit);
                            dc.SaveChanges();

                            transactions.Commit();
                            return Json(new { success = true, message = "Update Successfully" }, JsonRequestBehavior.AllowGet);
                        }
                        else {
                            foreach (string textboxValue in textboxValues)
                            {
                                detail_supir detsup = new detail_supir();
                                detsup.ID_PEMESANAN = detiiiil.ID_PEMESANAN;
                                detsup.ID_SUPIR = Convert.ToInt32(textboxValue);
                                var nama = dc.MS_SUPIR.AsNoTracking().Where(x => x.ID_SUPIR == detsup.ID_SUPIR).FirstOrDefault();
                                detsup.NAMA_SUPIR = nama.NAMA_SUPIR;
                                if (a < cekeditdriver)
                                {
                                    detsup.UTAMA = 1;
                                }
                                detsup.CREATED_DATE = DateTime.Now;
                                detsup.MODIFIED_DATE = DateTime.Now;
                                detsup.TGL_PERGI = cekjumlahmobil.TGL_PERGI;
                                detsup.TGL_PULANG = cekjumlahmobil.TGL_PULANG;
                                dc.detail_supir.Add(detsup);
                                dc.SaveChanges();
                                a++;
                            }

                            //audit trail edit
                            TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                            audit.NAMA_TABEL = "detail_supir";
                            audit.AKSI = "ADD, INSERT DRIVER";
                            audit.userid = Session["ID_MEMBER"].ToString();
                            audit.NAMA_FIELD = "";
                            audit.VALUE_LAMA = "";
                            audit.VALUE_BARU = detiiiil.ID_PEMESANAN.ToString();
                            audit.CREATED_DATE = DateTime.Now;
                            audit.MODIFIED_DATE = DateTime.Now;
                            dc.TBL_AUDIT_TRAIL.Add(audit);
                            dc.SaveChanges();

                            transactions.Commit();
                            return Json(new { success = true, message = "Insert Successfully" }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    return Json(new { success = true, message = msgErr.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpGet]
        public ActionResult SeeDriver(int id)
        {
            List<detail_supir> qtrx = dc.detail_supir.Where(x => x.ID_PEMESANAN == id).ToList();
            ViewData["supp"] = qtrx;
            return View();
        }
    }
}