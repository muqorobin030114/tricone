﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class RefundUserController : Controller
    {
        DB_TRICONEEntitie dc = new DB_TRICONEEntitie();
        // GET: RefundUser
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult GetdataRefund()
        {
            int id = Convert.ToInt32(Session["ID_MEMBER"]);

            List<v_refund> refundList = dc.v_refund.ToList<v_refund>();
            List<refundOutput> result = new List<refundOutput>();
            foreach (var item in refundList)
            {
                if (item.ID_MEMBER == id)
                {
                    result.Add(new refundOutput
                    {
                        ID_REFUND = Convert.ToInt32(item.ID_REFUND),
                        ID_TRANSAKSI = Convert.ToInt32(item.ID_TRANSAKSI),
                        TGL_PERGI = Convert.ToDateTime(item.TGL_PERGI),
                        NAMA_KOTA = item.NAMA_KOTA,
                        FOTO_REFUND = item.FOTO_REFUND,
                        STATUS = Convert.ToInt32(item.STATUS),
                        APPROVAL = Convert.ToInt32(item.APPROVAL)
                    });
                }
            }
            return Json(new { data = result.OrderByDescending(e => e.ID_REFUND) }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DeleteRefund(int id)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    var oke = dc.TBL_TRANSAKSI.AsNoTracking().Where(x => x.ID_REFUND == id).FirstOrDefault();
                    //update transaksi
                    var tansx = dc.TBL_TRANSAKSI.Where(x => x.ID_REFUND == id).FirstOrDefault();
                    tansx.ID_REFUND = null;
                    tansx.MODIFIED_DATE = DateTime.Now;
                    dc.Entry(tansx).State = EntityState.Modified;
                    dc.Entry(tansx).Property("ID_REFUND").IsModified = true;
                    dc.Entry(tansx).Property("MODIFIED_DATE").IsModified = true;
                    dc.SaveChanges();

                    //hapus table refund
                    dc.TBL_REFUND.RemoveRange(dc.TBL_REFUND.Where(c => c.ID_REFUND == id));
                    dc.SaveChanges();

                    //audit trail edit
                    TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                    audit.NAMA_TABEL = "TBL_TRANSAKSI,TBL_REFUND";
                    audit.AKSI = "DELETE REFUND REQUEST";
                    audit.userid = Session["ID_MEMBER"].ToString();
                    audit.NAMA_FIELD = "";
                    audit.VALUE_LAMA = oke.ID_REFUND + ", " + oke.ID_TRANSAKSI;
                    audit.VALUE_BARU = oke.ID_REFUND + ", " + oke.ID_TRANSAKSI;
                    audit.CREATED_DATE = DateTime.Now;
                    audit.MODIFIED_DATE = DateTime.Now;
                    dc.TBL_AUDIT_TRAIL.Add(audit);
                    dc.SaveChanges();

                    //hapus notif
                    int id_mem = Convert.ToInt32(Session["ID_MEMBER"]);
                    dc.notifikasi.RemoveRange(dc.notifikasi.Where(c => c.id_trx == id && c.NOTIF_FROM == id_mem.ToString() && c.NAMA_NOTIF == "refund"));
                    dc.SaveChanges();
                    transactions.Commit();
                    return RedirectToAction("index", "RefundUser");
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    ViewBag.Error = msgErr.Message;
                }
            }
            return null;
        }

        public ActionResult CancelRefund(int id)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    var oke = dc.TBL_TRANSAKSI.AsNoTracking().Where(x => x.ID_REFUND == id).FirstOrDefault();
                    //update transaksi
                    var tansx = dc.TBL_TRANSAKSI.Where(x => x.ID_REFUND == id).FirstOrDefault();
                    tansx.ID_REFUND = null;
                    tansx.MODIFIED_DATE = DateTime.Now;
                    dc.Entry(tansx).State = EntityState.Modified;
                    dc.Entry(tansx).Property("ID_REFUND").IsModified = true;
                    dc.Entry(tansx).Property("MODIFIED_DATE").IsModified = true;
                    dc.SaveChanges();

                    //update cancelled table refund
                    var updtrefund = dc.TBL_REFUND.Where(x => x.ID_REFUND == id).FirstOrDefault();
                    updtrefund.APPROVAL = 6;
                    updtrefund.STATUS = 2;
                    dc.Entry(updtrefund).State = EntityState.Modified;
                    dc.Entry(updtrefund).Property("MODIFIED_DATE").IsModified = true;
                    dc.Entry(updtrefund).Property("STATUS").IsModified = true;
                    dc.SaveChanges();

                    //audit trail edit
                    TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                    audit.NAMA_TABEL = "TBL_TRANSAKSI,TBL_REFUND";
                    audit.AKSI = "CANCEL REFUND REQUEST";
                    audit.userid = Session["ID_MEMBER"].ToString();
                    audit.NAMA_FIELD = "";
                    audit.VALUE_LAMA = oke.ID_REFUND + ", " + oke.ID_TRANSAKSI;
                    audit.VALUE_BARU = oke.ID_REFUND + ", " + oke.ID_TRANSAKSI;
                    audit.CREATED_DATE = DateTime.Now;
                    audit.MODIFIED_DATE = DateTime.Now;
                    dc.TBL_AUDIT_TRAIL.Add(audit);
                    dc.SaveChanges();

                    //hapus notif
                    int id_mem = Convert.ToInt32(Session["ID_MEMBER"]);
                    dc.notifikasi.RemoveRange(dc.notifikasi.Where(c => c.id_trx == id && c.NOTIF_FROM == id_mem.ToString() && c.NAMA_NOTIF == "refund"));
                    dc.SaveChanges();
                    transactions.Commit();
                    return RedirectToAction("index", "RefundUser");
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    ViewBag.Error = msgErr.Message;
                }
            }
            return null;
        }

        [HttpGet]
        public ActionResult addrefund()
        {
            TBL_REFUND transv = new TBL_REFUND();
            transv.transcollection = dc.v_transaksi.ToList<v_transaksi>();
            transv.bankcollection = dc.MS_BANK.ToList<MS_BANK>();
            
            return View(transv);
        }

        [HttpPost]
        public ActionResult addrefund(v_refund modd)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                    var tambahrec = dc.TBL_TRANSAKSI.Where(a => a.ID_TRANSAKSI == modd.ID_TRANSAKSI).FirstOrDefault();
                    var id_ref = "";
                    notifikasi noti = new notifikasi();
                    if (tambahrec.ID_REFUND != null)
                    {
                        audit.AKSI = "EDIT, UPDATE REFUND REQUEST";
                        //update refund aktif lagi
                        var updtrx = dc.TBL_REFUND.Where(a => a.ID_REFUND == tambahrec.ID_REFUND).FirstOrDefault();
                        updtrx.ID_BANK = modd.ID_BANK;
                        updtrx.NOMOR_REKENING = modd.NOMOR_REKENING;
                        updtrx.ID_TRANSAKSI = modd.ID_TRANSAKSI;
                        updtrx.APPROVAL = 0;
                        updtrx.KETERANGANUSER = modd.KETERANGANUSER;
                        updtrx.STATUS = 1;
                        updtrx.NAMA_REKENING = modd.NAMA_REKENING;
                        updtrx.MODIFIED_DATE = DateTime.Now;
                        dc.Entry(updtrx).State = EntityState.Modified;
                        dc.Entry(updtrx).Property("MODIFIED_DATE").IsModified = true;
                        dc.SaveChanges();
                        id_ref = tambahrec.ID_REFUND.ToString();
                        noti.id_trx = updtrx.ID_REFUND;
                    }
                    else
                    {
                        audit.AKSI = "ADD REFUND REQUEST";
                        //total durasi
                        DateTime dt1 = DateTime.Parse(Convert.ToDateTime(tambahrec.TGL_PERGI).AddDays(35).ToString());
                        DateTime dt2 = DateTime.Parse(Convert.ToDateTime(DateTime.Now).AddDays(35).ToString());
                        TimeSpan ts = dt1.Subtract(dt2);
                        int jumlah_durasi = ts.Days + 1;

                        TBL_REFUND transv = new TBL_REFUND();
                        if (jumlah_durasi > 7)
                        {
                            transv.TOTAL_REFUND = tambahrec.TOTAL_TRANSAKSI;
                        }
                        else
                        {
                            transv.TOTAL_REFUND = Convert.ToInt32(tambahrec.TOTAL_TRANSAKSI) / 2;
                        }
                        transv.ID_BANK = modd.ID_BANK;
                        transv.NOMOR_REKENING = modd.NOMOR_REKENING;
                        transv.ID_TRANSAKSI = modd.ID_TRANSAKSI;
                        transv.APPROVAL = 0;
                        transv.KETERANGANUSER = modd.KETERANGANUSER;
                        transv.STATUS = 1;
                        transv.NAMA_REKENING = modd.NAMA_REKENING;
                        transv.CREATED_DATE = DateTime.Now;
                        transv.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_REFUND.Add(transv);
                        dc.SaveChanges();

                        //update transaksi
                        var updtrx = dc.TBL_REFUND.Where(a => a.ID_TRANSAKSI == modd.ID_TRANSAKSI).FirstOrDefault();
                        var tansx = dc.TBL_TRANSAKSI.Where(x => x.ID_TRANSAKSI == modd.ID_TRANSAKSI).FirstOrDefault();
                        tansx.ID_REFUND = updtrx.ID_REFUND;
                        tansx.MODIFIED_DATE = DateTime.Now;
                        dc.Entry(tansx).State = EntityState.Modified;
                        dc.Entry(tansx).Property("ID_REFUND").IsModified = true;
                        dc.Entry(tansx).Property("MODIFIED_DATE").IsModified = true;
                        dc.SaveChanges();

                        id_ref = updtrx.ID_REFUND.ToString();
                        noti.id_trx = updtrx.ID_REFUND;
                    }

                    //audit trail
                    audit.NAMA_TABEL = "TBL_TRANSAKSI,TBL_REFUND";
                    audit.userid = Session["ID_MEMBER"].ToString();
                    audit.NAMA_FIELD = "";
                    audit.VALUE_LAMA = "";
                    audit.VALUE_BARU = modd.ID_TRANSAKSI + ", " + id_ref;
                    audit.CREATED_DATE = DateTime.Now;
                    audit.MODIFIED_DATE = DateTime.Now;
                    dc.TBL_AUDIT_TRAIL.Add(audit);
                    dc.SaveChanges();

                    //buat notifikasi
                    noti.NAMA_NOTIF = "refund";
                    noti.NOTIF_FROM = tambahrec.ID_MEMBER.ToString();
                    noti.NOTIF_FOR = "admin";
                    dc.Entry(noti).State = EntityState.Added;
                    dc.SaveChanges();
                    transactions.Commit();
                    return RedirectToAction("index", "RefundUser");
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    ViewBag.Error = msgErr.Message;
                }
            }
            return null;
        }

        [HttpGet]
        public JsonResult cekpengembalian(int id_trans)
        {
            var tambahrec = dc.TBL_TRANSAKSI.Where(a => a.ID_TRANSAKSI == id_trans).FirstOrDefault();
            
            //total durasi
            DateTime dt1 = DateTime.Parse(Convert.ToDateTime(tambahrec.TGL_PERGI).AddDays(35).ToString());
            DateTime dt2 = DateTime.Parse(Convert.ToDateTime(DateTime.Now).AddDays(35).ToString());
            TimeSpan ts = dt1.Subtract(dt2);
            int jumlah_durasi = ts.Days + 1;
            var hasil = "";
            if (jumlah_durasi > 7)
            {
                int jumlah = Convert.ToInt32(tambahrec.TOTAL_TRANSAKSI);
                hasil = "You will receive a refund of 100 % because it hasn't been less than 7 days, which is Rp."+ jumlah.ToString("#,##");
            }
            else
            {
                Decimal jumlah = Convert.ToDecimal(tambahrec.TOTAL_TRANSAKSI) / 2;
                hasil = "You will receive a refund of 50 % because it's been more than 7 days, which is Rp."+ jumlah.ToString("#,##");
            }
            var json = JsonConvert.SerializeObject(hasil);
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult detailbayar(int id)
        {
            ViewBag.id_trans = id;
            return View();
        }

        [HttpGet]
        public JsonResult GetdataDetail(int id_trans)
        {
            var refu = dc.TBL_REFUND.Where(g => g.ID_REFUND == id_trans).FirstOrDefault();
            List<v_detail_pesan> detailtransaksiList = dc.v_detail_pesan.ToList<v_detail_pesan>();
            List<detailtransaksiOutput> result = new List<detailtransaksiOutput>();
            foreach (var item in detailtransaksiList)
            {
                if (refu.ID_TRANSAKSI == item.id_transaksi)
                {
                    result.Add(new detailtransaksiOutput
                    {
                        ID_TRANSAKSI = Convert.ToInt32(item.id_transaksi),
                        id_pemesanan = Convert.ToInt32(item.id_pemesanan),
                        id_member = Convert.ToInt32(item.id_member),
                        nama_member = item.nama_member,
                        telpon_member = item.telpon_member,
                        email_member = item.email_member,
                        id_harga = Convert.ToInt32(item.id_harga),
                        harga = Convert.ToInt32(item.harga),
                        id_kendaraan = Convert.ToInt32(item.id_kendaraan),
                        id_jenis_kendaraan = Convert.ToInt32(item.id_jenis_kendaraan),
                        nama_kendaraan = item.nama_kendaraan,
                        jumlah_seat = Convert.ToInt32(item.jumlah_seat),
                        jumlah_kendaraan = Convert.ToInt32(item.jumlah_kendaraan),
                        status_kendaraan = Convert.ToInt32(item.status_kendaraan),
                        durasi_jam = Convert.ToInt32(item.durasi_jam),
                        id_harilibur = Convert.ToInt32(item.id_harilibur),
                        nama_harilibur = item.nama_harilibur,
                        startdate_libur = Convert.ToDateTime(item.startdate_libur),
                        enddate_libur = Convert.ToDateTime(item.enddate_libur),
                        persen_libur = Convert.ToInt32(item.persen_libur),
                        tgl_pergi = Convert.ToDateTime(item.tgl_pergi),
                        tgl_pulang = Convert.ToDateTime(item.tgl_pulang),
                        id_kota = Convert.ToInt32(item.id_kota),
                        nama_kota = item.nama_kota,
                        id_provinsi = Convert.ToInt32(item.id_provinsi),
                        nama_provinsi = item.nama_provinsi,
                        quantity_kendaraan = Convert.ToInt32(item.quantity_kendaraan),
                        total_pembayaran = Convert.ToInt32(item.total_pembayaran),
                        status_detail_pesan = Convert.ToInt32(item.status_detail_pesan),
                        id_supir = Convert.ToInt32(item.id_supir),
                        nama_supir = item.nama_supir,
                        telpon_supir = item.telpon_supir

                    });
                }
            }
            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }
    }
}