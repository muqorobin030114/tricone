﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class HargaUserController : Controller
    {
        // GET: Harga
        DB_TRICONEEntitie dc = new DB_TRICONEEntitie();

        public ActionResult Index()
        {
            return View(dc.TBL_HARGA.ToList());
        }

        [HttpGet]
        public JsonResult Getdata()
        {
            List<TBL_HARGA> hargauserlist = dc.TBL_HARGA.ToList<TBL_HARGA>();
            List<HargaUserOutput> result = new List<HargaUserOutput>();
            foreach (var item in hargauserlist)
            {
                result.Add(new HargaUserOutput
                {
                    ID_HARGA = item.ID_HARGA,
                    ID_KENDARAAN = item.MS_KENDARAAN.ID_KENDARAAN,
                    NAMA_KENDARAAN = item.MS_KENDARAAN.MS_JENIS_KENDARAAN.NAMA_KENDARAAN,
                    ID_PROVINSI = item.MS_KOTA.MS_PROVINSI.ID_PROVINSI,
                    NAMA_PROVINSI = item.MS_KOTA.MS_PROVINSI.NAMA_PROVINSI,
                    ID_KOTA = item.MS_KOTA.ID_KOTA,
                    NAMA_KOTA = item.MS_KOTA.NAMA_KOTA,
                    DURASI = Convert.ToInt32(item.DURASI),
                    HARGA = Convert.ToInt32(item.HARGA)
                });
            }
            
            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }
    }

    public class HargaUserOutput
    {
        public int ID_HARGA { get; set; }
        public int ID_KENDARAAN { get; set; }
        public string NAMA_KENDARAAN { get; set; }
        public int ID_KOTA { get; set; }
        public string NAMA_KOTA { get; set; }
        public int ID_PROVINSI { get; set; }
        public string NAMA_PROVINSI { get; set; }
        public int DURASI { get; set; }
        public int HARGA { get; set; }
    }
}
