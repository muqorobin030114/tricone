﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class DetailKendaraanController : Controller
    {
        // GET: DetailKendaraan
        DB_TRICONEEntitie db = new DB_TRICONEEntitie();
        public ActionResult Index()
        {
            List<MS_KENDARAAN> kendaraanList = db.MS_KENDARAAN.ToList();
            ViewData["detailkendaraan"] = kendaraanList;
            return View();
        }

        [HttpGet]
        public ActionResult Shuttle()
        {
            List<MS_KENDARAAN> kendaraanList = db.MS_KENDARAAN.Where(x => x.ID_KENDARAAN == 1).ToList();
            ViewData["detailkendaraan"] = kendaraanList;
            return View();
        }

        [HttpGet]
        public ActionResult MediumBus()
        {
            List<MS_KENDARAAN> kendaraanList = db.MS_KENDARAAN.Where(x => x.ID_KENDARAAN == 2).ToList();
            ViewData["detailkendaraan"] = kendaraanList;
            return View();
        }

        [HttpGet]
        public ActionResult BigBus()
        {
            List<MS_KENDARAAN> kendaraanList = db.MS_KENDARAAN.Where(x => x.ID_KENDARAAN == 3).ToList();
            ViewData["detailkendaraan"] = kendaraanList;
            return View();
        }
    }
}