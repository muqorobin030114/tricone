﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;
using System.Data;

namespace Tricone.Controllers
{
    public class BankController : Controller
    {
        // GET: Bank
        DB_TRICONEEntitie dc = new DB_TRICONEEntitie();

        public ActionResult Index()
        {
            return View(dc.MS_BANK.ToList());
        }

        [HttpGet]
        public JsonResult Getdata()
        {
            List<MS_BANK> provList = dc.MS_BANK.ToList<MS_BANK>();
            List<BankOutput> result = new List<BankOutput>();
            foreach (var item in provList)
            {
                result.Add(new BankOutput
                {
                    ID_BANK = item.ID_BANK,
                    NAMA_BANK = item.NAMA_BANK
                });
            }

            //var json = Newtonsoft.Json.JsonConvert.SerializeObject(result);
            return Json(new { data = result.OrderByDescending(e => e.ID_BANK) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult AddorEdit(int id = 0)
        {
            if (id == 0)
            {
                return View(new MS_BANK());
            }
            else
            {
                return View(dc.MS_BANK.Where(x => x.ID_BANK == id).FirstOrDefault<MS_BANK>());
            }
        }

        [HttpPost]
        public ActionResult AddorEdit(MS_BANK bank)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    bank.MODIFIED_DATE = DateTime.Now;
                    if (bank.ID_BANK == 0)
                    {
                        bank.CREATED_DATE = DateTime.Now;
                        dc.MS_BANK.Add(bank);
                        dc.SaveChanges();

                        //audit trail
                        var bnk = dc.MS_BANK.OrderByDescending(e => e.ID_BANK).FirstOrDefault();
                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                        audit.NAMA_TABEL = "MS_BANK";
                        audit.AKSI = "ADD";
                        audit.NAMA_FIELD = "All Field";
                        audit.VALUE_LAMA = "";
                        audit.userid = Session["ID_MEMBER"].ToString();
                        audit.VALUE_BARU = bank.ID_BANK + ", " + bank.NAMA_BANK;
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();

                        transactions.Commit();
                        return Json(new { success = true, message = "Saved Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var oke = dc.MS_BANK.AsNoTracking().Where(x => x.ID_BANK == bank.ID_BANK).FirstOrDefault();
                        dc.Entry(bank).State = EntityState.Modified;
                        dc.Entry(bank).Property("CREATED_DATE").IsModified = false;
                        dc.SaveChanges();

                        //audit trail edit

                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                        audit.NAMA_TABEL = "MS_BANK";
                        audit.AKSI = "EDIT";
                        audit.userid = Session["ID_MEMBER"].ToString();
                        audit.NAMA_FIELD ="NAMA_BANK";
                        audit.VALUE_LAMA = bank.ID_BANK + ", " + oke.NAMA_BANK;
                        audit.VALUE_BARU = bank.ID_BANK + ", " + bank.NAMA_BANK;
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();

                        transactions.Commit();
                        return Json(new { success = true, message = "Updated Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    return Json(new { success = true, message = msgErr.Message }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            MS_BANK bank = dc.MS_BANK.Where(x => x.ID_BANK == id).FirstOrDefault<MS_BANK>();
            dc.MS_BANK.Remove(bank);
            dc.SaveChanges();
            return Json(new { success = true, message = "Deleted Successfully" }, JsonRequestBehavior.AllowGet);
        }
    }
    public class BankOutput
    {
        public int ID_BANK { get; set; }
        public string NAMA_BANK { get; set; }
    }
}
