﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class RegistController : Controller
    {
        //Registration Action
        [HttpGet]
        public ActionResult Registration()
        {
            return View();
        }

        //Registration POST action
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Registration([Bind (Exclude = "ISEMAILVERIFIED,ACTIVATIONCODE")] TBL_MEMBER member)
        {
            bool Status = false;
            string message = "";
            //model validation

            member.ROLEMEMBER = "MEMBER";
            member.STATUS = false;
            if (ModelState.IsValid)
            {
                //email is already exist
                #region //email is already exist
                var isExist = IsEmailExist(member.EMAIL);
                if (isExist)
                {
                    ModelState.AddModelError("EmailExist", "Email already exist");
                    return View(member);
                }
                #endregion

                NewMethod(member);
                #region password hashing
                member.PASSWORD = Crypto.Hash(member.PASSWORD);
                member.CONFIRMPASSWORD = Crypto.Hash(member.CONFIRMPASSWORD);
                member.ROLEMEMBER = "MEMBER";
                #endregion
                member.ISEMAILVERIFIED = false;

                #region save data to database
                using (DB_TRICONEEntitie dc = new DB_TRICONEEntitie())
                {
                    dc.TBL_MEMBER.Add(member);
                    dc.SaveChanges();

                    //audit trail
                    var qaudit = dc.TBL_MEMBER.OrderByDescending(e => e.ID_MEMBER).FirstOrDefault();
                    TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                    audit.NAMA_TABEL = "TBL_MEMBER";
                    audit.AKSI = "ADD MEMBER";
                    audit.NAMA_FIELD = "All Field";
                    audit.VALUE_LAMA = "";

                    audit.userid = qaudit.ID_MEMBER.ToString();
                    audit.VALUE_BARU = qaudit.ID_MEMBER + ", " + member.NAMA_MEMBER + ", " + member.ALAMAT + ", " + member.NO_TELEPON + ", " + member.EMAIL + ", " + member.USERNAME + ", " + member.STATUS + ", " + member.ROLEMEMBER;
                    audit.CREATED_DATE = DateTime.Now;
                    audit.MODIFIED_DATE = DateTime.Now;
                    dc.TBL_AUDIT_TRAIL.Add(audit);
                    dc.SaveChanges();

                    //sent email to user
                    SendVerificationLinkEmail(member.EMAIL, member.ACTIVATIONCODE.ToString());
                    message = "Registration successfully done. Account activation link " +
                        " has been sent to your email id: " + member.EMAIL;
                    Status = true;
                }
                #endregion
            }
            else
            {
                message = "Invalid Request";
            }
            ViewBag.Message = message;
            ViewBag.Status = Status;
            ModelState.Clear();
            return View();
        }

        private static void NewMethod(TBL_MEMBER member)
        {
            #region generate activation code
            member.ACTIVATIONCODE = Guid.NewGuid();
            #endregion
        }

        //verify Account
        [HttpGet]
        public ActionResult VerifyAccount(string id)
        {
            bool Status = false;
            using (DB_TRICONEEntitie dc = new DB_TRICONEEntitie())
            {
                dc.Configuration.ValidateOnSaveEnabled = false; //this line I have added here to avoid
                                                                //confirm password does not match issue on save changes
                var v = dc.TBL_MEMBER.Where(a=>a.ACTIVATIONCODE == new Guid(id)).FirstOrDefault();
                if (v != null)
                {
                    v.ISEMAILVERIFIED = true;
                    dc.SaveChanges();
                    Status = true;
                }
                else
                {
                    ViewBag.Message = "Invalid Request";
                }
            }
            ViewBag.Status = Status;
            return View();
        }

        //Login
        [HttpGet]
        public ActionResult Login()
        {
            return View();
        }

        //Login POST
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(UserLogin login, string ReturnUrl = "")
        {
            string message = "";
            using (DB_TRICONEEntitie dc = new DB_TRICONEEntitie())
            {
                var v = dc.TBL_MEMBER.Where(a => a.EMAIL == login.EmailID).FirstOrDefault();
                if (v != null)
                {
                    if (string.Compare(Crypto.Hash(login.Password), v.PASSWORD) == 0)
                    {
                        if (v.ISEMAILVERIFIED == true)
                        {
                            int timeout = login.RememberMe ? 525600 : 20; // 525600 min = 1 year
                            var ticket = new FormsAuthenticationTicket(login.EmailID, login.RememberMe, timeout);
                            string encrypted = FormsAuthentication.Encrypt(ticket);
                            var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, encrypted);
                            cookie.Expires = DateTime.Now.AddMinutes(timeout);
                            cookie.HttpOnly = true;
                            Response.Cookies.Add(cookie);

                            //membuat session
                            Session["EmailID"] = login.EmailID.ToString();
                            Session["ID_MEMBER"] = v.ID_MEMBER.ToString();
                            Session["USERNAME"] = v.USERNAME.ToString();
                            Session["ROLEMEMBER"] = v.ROLEMEMBER.ToString();
                            if (Url.IsLocalUrl(ReturnUrl))
                            {
                                return Redirect(ReturnUrl);
                            }
                            else
                            {
                                TempData["loggedInRoleMember"] = "MEMBER";
                                return RedirectToAction("Index", "Home");
                            }
                        }
                        else
                        {
                            message = "Please Verify Your Email!";
                        }
                    }
                    else
                    {
                        message = "Invalid credential provided";
                    }
                }
                else
                {
                    message = "Invalid credential provided";
                }
            }
            ViewBag.Message = message;
            return View();
        }

        //lOGOUT
        public ActionResult Logout()
        {
            Session.Abandon();
            Session.RemoveAll();
            Response.Cookies.Clear();
            FormsAuthentication.SignOut();
            return RedirectToAction("Index","Home");
        }


        [NonAction]
        public bool IsEmailExist(string emailID)
        {
            using (DB_TRICONEEntitie dc = new DB_TRICONEEntitie())
            {
                var v = dc.TBL_MEMBER.Where(a=>a.EMAIL == emailID).FirstOrDefault();
                return v != null;
            }
        }

        [NonAction]
        public void SendVerificationLinkEmail(string emailID, string activationCode, string emailFor = "VerifyAccount")
        {
            var verifyUrl = "/Regist/"+emailFor+"/" + activationCode;
            var link = Request.Url.AbsoluteUri.Replace(Request.Url.PathAndQuery, verifyUrl);

            var fromEmail = new MailAddress("traveltricone@gmail.com", "Tricone Membership");
            var toEmail = new MailAddress(emailID);
            var fromEmailPassword = "tangerang"; //replace with actual password

            string subject = "";
            string body = "";
            if (emailFor == "VerifyAccount")
            {
                subject = "Your account is succesfully created!";

                body = "<br/><br/>We are exited to tell you that your account is" +
                " successfully created. Please click on the below link to verify your account" +
                " <br/><br/><a href='" + link + "'>" + link + "</a>";
            }
            else if (emailFor == "ResetPassword")
            {
                subject = "Reset Password";
                body = "Hi, <br/><br/>We got requst for your account password. Please click on the below link to reset your password" +
                    "<br/><br/><a href="+link+">Reset Password link</a>";
            }
            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
            };

            using (var message = new MailMessage(fromEmail, toEmail)
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true
            })
            smtp.Send(message);
        }

        //forgot password

        [HttpGet]
        public ActionResult ForgotPassword()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ForgotPassword(string EmailID)
        {
            //Verify Email ID
            //Generate Reset password link 
            //Send Email 
            string message = "";
            //bool status = false;

            using (DB_TRICONEEntitie dc = new DB_TRICONEEntitie())
            {
                var account = dc.TBL_MEMBER.Where(a => a.EMAIL == EmailID).FirstOrDefault();
                if (account != null)
                {
                    var oke = dc.TBL_MEMBER.AsNoTracking().Where(x => x.ID_MEMBER == account.ID_MEMBER).FirstOrDefault();
                    //Send email for reset password
                    string resetCode = Guid.NewGuid().ToString();
                    SendVerificationLinkEmail(account.EMAIL, resetCode, "ResetPassword");
                    account.RESETPASSWORD = resetCode;
                    //This line I have added here to avoid confirm password not match issue , as we had added a confirm password property 
                    //in our model class in part 1
                    dc.Configuration.ValidateOnSaveEnabled = false;
                    dc.SaveChanges();

                    //audit trail
                    TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                    audit.NAMA_TABEL = "TBL_MEMBER";
                    audit.AKSI = "EDIT, RESET PASSWORD";
                    audit.NAMA_FIELD = "";
                    audit.VALUE_LAMA = oke.EMAIL + ", " + oke.RESETPASSWORD;
                    audit.VALUE_BARU = account.EMAIL + ", " + account.RESETPASSWORD;
                    audit.CREATED_DATE = DateTime.Now;
                    audit.MODIFIED_DATE = DateTime.Now;
                    dc.TBL_AUDIT_TRAIL.Add(audit);
                    dc.SaveChanges();

                    message = "Reset password link has been sent to your email id.";
                    ModelState.Clear();
                }
                else
                {
                    message = "Account not found";
                }
            }
            ViewBag.Message = message;
            return View();
        }

        public ActionResult ResetPassword(string id)
        {
            //Verifikasi link reset password
            //menemukan akun dengan link yang terkait
            //kembali ke halaman reset password 
            if (string.IsNullOrWhiteSpace(id))
            {
                return HttpNotFound();
            }

            using (DB_TRICONEEntitie dc = new DB_TRICONEEntitie())
            {
                var user = dc.TBL_MEMBER.Where(a => a.RESETPASSWORD == id).FirstOrDefault();
                if (user != null)
                {
                    ResetPasswordModel model = new ResetPasswordModel();
                    model.ResetCode = id;
                    return View(model);
                }
                else
                {
                    return HttpNotFound();
                }
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ResetPassword(ResetPasswordModel model)
        {
            var message = "";
            if (ModelState.IsValid)
            {
                using (DB_TRICONEEntitie dc = new DB_TRICONEEntitie())
                {
                    var user = dc.TBL_MEMBER.Where(a => a.RESETPASSWORD == model.ResetCode).FirstOrDefault();
                    if (user != null)
                    {
                        var oke = dc.TBL_MEMBER.AsNoTracking().Where(x => x.ID_MEMBER == user.ID_MEMBER).FirstOrDefault();
                        user.PASSWORD = Crypto.Hash(model.NewPassword);
                        user.RESETPASSWORD = "";
                        dc.Configuration.ValidateOnSaveEnabled = false;
                        dc.SaveChanges();

                        //audit trail
                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                        audit.NAMA_TABEL = "TBL_MEMBER";
                        audit.AKSI = "EDIT, UPDATE PASSWORD";
                        audit.NAMA_FIELD = "";
                        audit.VALUE_LAMA = oke.EMAIL + ", " + oke.PASSWORD;
                        audit.VALUE_BARU = user.EMAIL + ", " + user.PASSWORD;
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();

                        message = "New password updated successfully. Click here to";
                        ModelState.Clear();
                        //return RedirectToAction("Login", "Regist");
                    }
                }
            }
            else
            {
                message = "Something invalid";
            }
            ViewBag.Message = message;
            return View(model);
        }
    }
}