﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tricone.Models;

namespace Tricone.Controllers
{
    public class ReScheduleUserController : Controller
    {
        // GET: ReSchedule
        DB_TRICONEEntitie dc = new DB_TRICONEEntitie();
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public JsonResult Getdata()
        {
            int id = Convert.ToInt32(Session["ID_MEMBER"]);

            List<v_reschedule> transaksiList = dc.v_reschedule.ToList<v_reschedule>();
            List<transaksiMemberOutput> result = new List<transaksiMemberOutput>();
            foreach (var item in transaksiList)
            {
                if (item.ID_MEMBER == id)
                {
                    {
                        result.Add(new transaksiMemberOutput
                        {
                            ID_TRANSAKSI = item.ID_TRANSAKSI,
                            TGL_PERGI = Convert.ToDateTime(item.TGL_PERGI),
                            TGL_PULANG = Convert.ToDateTime(item.TGL_PULANG),
                            NAMA_KOTA = item.NAMA_KOTA,
                            FOTO_PEMBAYARAN = item.FOTO_PEMBAYARAN,
                            STATUS = Convert.ToInt32(item.STATUS_TRANSAKSI),
                            APPROVAL = Convert.ToInt32(item.APPROVAL),
                            SISA_BAYAR = Convert.ToInt32(item.SISA_BAYAR),
                            KETERANGANADMIN = item.KETERANGANADMIN
                        });
                    }
                }
            }
            return Json(new { data = result.OrderByDescending(e => e.ID_TRANSAKSI) }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult pilihReschedule()
        {
            return View();
        }

        [HttpGet]
        public ActionResult onlydate()
        {
            TBL_TRANSAKSI ctrans = new TBL_TRANSAKSI();
            ctrans.transcollection = dc.v_transaksi.ToList<v_transaksi>();
            return View(ctrans);
        }

        [HttpGet]
        public JsonResult tanggalmu(int id_trans)
        {
            var tambahrec = dc.TBL_TRANSAKSI.Where(a => a.ID_TRANSAKSI == id_trans).FirstOrDefault();

            //total durasi
            var startdate = String.Format("{0:dd/MM/yyyy}", tambahrec.TGL_PERGI);
            var enddate = String.Format("{0:dd/MM/yyyy}", tambahrec.TGL_PULANG);

            //total durasi db
            DateTime dt1 = DateTime.Parse(Convert.ToDateTime(tambahrec.TGL_PULANG).AddDays(35).ToString());
            DateTime dt2 = DateTime.Parse(Convert.ToDateTime(tambahrec.TGL_PERGI).AddDays(35).ToString());
            TimeSpan ts1 = dt1.Subtract(dt2);
            int jumlah_durasi1 = ts1.Days + 1;

            var hasil = "your current date is "+ startdate + " To "+ enddate + "which is "+ jumlah_durasi1 + " day";
            
            var json = JsonConvert.SerializeObject(hasil);
            return Json(json, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult addonlydate(TBL_TRANSAKSI trx)
        {
            using (var transactions = dc.Database.BeginTransaction())
            {
                try
                {
                    var tambahrec = dc.TBL_TRANSAKSI.Where(a => a.ID_TRANSAKSI == trx.ID_TRANSAKSI).FirstOrDefault();

                    //total durasi db
                    DateTime dt1 = DateTime.Parse(Convert.ToDateTime(tambahrec.TGL_PULANG).AddDays(35).ToString());
                    DateTime dt2 = DateTime.Parse(Convert.ToDateTime(tambahrec.TGL_PERGI).AddDays(35).ToString());
                    TimeSpan ts1 = dt1.Subtract(dt2);
                    int jumlah_durasi1 = ts1.Days + 1;

                    //total durasi input
                    DateTime dt3 = DateTime.Parse(Convert.ToDateTime(trx.TGL_PULANG).AddDays(35).ToString());
                    DateTime dt4 = DateTime.Parse(Convert.ToDateTime(trx.TGL_PERGI).AddDays(35).ToString());
                    TimeSpan ts2 = dt3.Subtract(dt4);
                    int jumlah_durasi2 = ts2.Days + 1;

                    if (jumlah_durasi1 != jumlah_durasi2)
                    {
                        return Json(new { success = false, message = "make sure that the number of days is the same" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        TBL_TRANSAKSI trxs = new TBL_TRANSAKSI();
                        trxs.TOTAL_TRANSAKSI = tambahrec.TOTAL_TRANSAKSI;
                        trxs.ID_PROMO = tambahrec.ID_PROMO;
                        trxs.FOTO_PEMBAYARAN = tambahrec.FOTO_PEMBAYARAN;
                        trxs.TOTAL_DURASI = tambahrec.TOTAL_DURASI;
                        trxs.STATUS_UBAH_JADWAL = tambahrec.STATUS_UBAH_JADWAL;
                        trxs.APPROVAL = 2;
                        trxs.KETERANGANUSER = trx.KETERANGANUSER;
                        trxs.ID_PEMBAYARAN = tambahrec.ID_PEMBAYARAN;
                        trxs.STATUS = 1;
                        trxs.KETERANGANADMIN = tambahrec.KETERANGANADMIN;
                        trxs.ID_REFUND = tambahrec.ID_REFUND;
                        trxs.CREATED_DATE = DateTime.Now;
                        trxs.MODIFIED_DATE = DateTime.Now;
                        trxs.ID_MEMBER = tambahrec.ID_MEMBER;
                        trxs.TGL_PERGI = trx.TGL_PERGI;
                        trxs.TGL_PULANG = trx.TGL_PULANG;
                        trxs.ID_KOTA = tambahrec.ID_KOTA;
                        trxs.CATATAN = tambahrec.CATATAN;
                        trxs.JUMLAH_PROMO = tambahrec.JUMLAH_PROMO;

                        dc.Entry(trxs).State = EntityState.Added;
                        dc.SaveChanges();

                        var cekid = dc.TBL_TRANSAKSI.Where(a => a.ID_MEMBER == tambahrec.ID_MEMBER).OrderByDescending(x => x.ID_TRANSAKSI).FirstOrDefault();
                        var id_baru = cekid.ID_TRANSAKSI;
                        var detail = dc.TR_DETAIL_PEMESANAN.Where(a => a.ID_TRANSAKSI == trx.ID_TRANSAKSI).ToList();
                        foreach (var item in detail)
                        {
                            TR_DETAIL_PEMESANAN dtrans = new TR_DETAIL_PEMESANAN();
                            dtrans.ID_MEMBER = item.ID_MEMBER;
                            dtrans.ID_HARGA = item.ID_HARGA;
                            dtrans.ID_HARILIBUR = item.ID_HARILIBUR;
                            dtrans.TGL_PERGI = trx.TGL_PERGI;
                            dtrans.TGL_PULANG = trx.TGL_PULANG;
                            dtrans.ID_KOTA = item.ID_KOTA;
                            dtrans.QUANTITY_KENDARAAN = item.QUANTITY_KENDARAAN;
                            dtrans.TOTAL_PEMBAYARAN = item.TOTAL_PEMBAYARAN;
                            dtrans.STATUS = item.STATUS;
                            dtrans.ID_SUPIR = item.ID_SUPIR;
                            dtrans.CREATED_DATE = DateTime.Now;
                            dtrans.MODIFIED_DATE = DateTime.Now;
                            dtrans.ID_TRANSAKSI = cekid.ID_TRANSAKSI;

                            dc.Entry(dtrans).State = EntityState.Added;
                            dc.SaveChanges();
                        }

                        tambahrec.STATUS = 0;
                        tambahrec.APPROVAL = 7;
                        tambahrec.STATUS_UBAH_JADWAL = id_baru;
                        tambahrec.MODIFIED_DATE = DateTime.Now;
                        dc.Entry(tambahrec).State = EntityState.Modified;
                        dc.Entry(tambahrec).Property("STATUS").IsModified = true;
                        dc.Entry(tambahrec).Property("APPROVAL").IsModified = true;
                        dc.Entry(tambahrec).Property("STATUS_UBAH_JADWAL").IsModified = true;
                        dc.Entry(tambahrec).Property("MODIFIED_DATE").IsModified = true;
                        dc.SaveChanges();

                        //audit trail edit
                        TBL_AUDIT_TRAIL audit = new TBL_AUDIT_TRAIL();
                        audit.NAMA_TABEL = "TBL_TRANSAKSI,TR_DETAIL_PEMESANAN";
                        audit.AKSI = "ADD, RESCHEDULE";
                        audit.userid = Session["ID_MEMBER"].ToString();
                        audit.NAMA_FIELD = "";
                        audit.VALUE_LAMA = tambahrec.ID_TRANSAKSI.ToString();
                        audit.VALUE_BARU = id_baru.ToString();
                        audit.CREATED_DATE = DateTime.Now;
                        audit.MODIFIED_DATE = DateTime.Now;
                        dc.TBL_AUDIT_TRAIL.Add(audit);
                        dc.SaveChanges();

                        //buat notifikasi
                        notifikasi noti = new notifikasi();
                        noti.NAMA_NOTIF = "ubahjadwal";
                        noti.NOTIF_FOR = "admin";
                        dc.Entry(noti).State = EntityState.Added;
                        dc.SaveChanges();
                        transactions.Commit();

                        return Json(new { success = true, message = "nih", redirectToUrl = Url.Action("index", "ReScheduleUser") }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntitie dc2 = new DB_TRICONEEntitie();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    if (Session["ID_MEMBER"].ToString() != null)
                    {
                        logerror.ID_USER = Session["ID_MEMBER"].ToString();
                    }
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();

                    transactions.Rollback();
                    return Json(new { success = false, message = msgErr.Message }, JsonRequestBehavior.AllowGet);
                }
            }
                
            //using (var transactions = dc.Database.BeginTransaction())
            //{
            //    try
            //    {
            //kota.MODIFIED_DATE = DateTime.Now;
            //if (kota.ID_KOTA == 0)
            //{
            //    kota.CREATED_DATE = DateTime.Now;
            //    dc.MS_KOTA.Add(kota);
            //    dc.SaveChanges();
            //    transactions.Commit();
            //    return Json(new { success = true, message = "Saved Successfully" }, JsonRequestBehavior.AllowGet);
            //}
            //else
            //{
            //    dc.Entry(kota).State = EntityState.Modified;
            //    dc.Entry(kota).Property("CREATED_DATE").IsModified = false;
            //    dc.SaveChanges();
            //    transactions.Commit();
            //    return Json(new { success = true, message = "Updated Successfully" }, JsonRequestBehavior.AllowGet);
            //}
            //return Json(new { success = true, message = "a" }, JsonRequestBehavior.AllowGet);
            //}
            //    catch (Exception msgErr)
            //    {
            //        transactions.Rollback();
            //        return Json(new { success = true, message = msgErr.Message }, JsonRequestBehavior.AllowGet);
            //    }
            //}
        }

        [HttpGet]
        public ActionResult detailbayar(int id)
        {
            ViewBag.id_trans = id;
            return View();
        }

        [HttpGet]
        public JsonResult GetdataDetail(int id_trans)
        {
            List<v_detail_pesan> detailtransaksiList = dc.v_detail_pesan.ToList<v_detail_pesan>();
            List<detailtransaksiOutput> result = new List<detailtransaksiOutput>();
            foreach (var item in detailtransaksiList)
            {
                if (id_trans == item.id_transaksi)
                {
                    result.Add(new detailtransaksiOutput
                    {
                        ID_TRANSAKSI = Convert.ToInt32(item.id_transaksi),
                        id_pemesanan = Convert.ToInt32(item.id_pemesanan),
                        id_member = Convert.ToInt32(item.id_member),
                        nama_member = item.nama_member,
                        telpon_member = item.telpon_member,
                        email_member = item.email_member,
                        id_harga = Convert.ToInt32(item.id_harga),
                        harga = Convert.ToInt32(item.harga),
                        id_kendaraan = Convert.ToInt32(item.id_kendaraan),
                        id_jenis_kendaraan = Convert.ToInt32(item.id_jenis_kendaraan),
                        nama_kendaraan = item.nama_kendaraan,
                        jumlah_seat = Convert.ToInt32(item.jumlah_seat),
                        jumlah_kendaraan = Convert.ToInt32(item.jumlah_kendaraan),
                        status_kendaraan = Convert.ToInt32(item.status_kendaraan),
                        durasi_jam = Convert.ToInt32(item.durasi_jam),
                        id_harilibur = Convert.ToInt32(item.id_harilibur),
                        nama_harilibur = item.nama_harilibur,
                        startdate_libur = Convert.ToDateTime(item.startdate_libur),
                        enddate_libur = Convert.ToDateTime(item.enddate_libur),
                        persen_libur = Convert.ToInt32(item.persen_libur),
                        tgl_pergi = Convert.ToDateTime(item.tgl_pergi),
                        tgl_pulang = Convert.ToDateTime(item.tgl_pulang),
                        id_kota = Convert.ToInt32(item.id_kota),
                        nama_kota = item.nama_kota,
                        id_provinsi = Convert.ToInt32(item.id_provinsi),
                        nama_provinsi = item.nama_provinsi,
                        quantity_kendaraan = Convert.ToInt32(item.quantity_kendaraan),
                        total_pembayaran = Convert.ToInt32(item.total_pembayaran),
                        status_detail_pesan = Convert.ToInt32(item.status_detail_pesan),
                        id_supir = Convert.ToInt32(item.id_supir),
                        nama_supir = item.nama_supir,
                        telpon_supir = item.telpon_supir

                    });
                }
            }
            return Json(new { data = result }, JsonRequestBehavior.AllowGet);
        }
    }
}