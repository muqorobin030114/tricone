﻿using System.Web;
using System.Web.Optimization;

namespace Tricone
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            /*bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));*/

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            /*bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));*/
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
              "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                                    "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/fontsing/css").Include(
                     "~/fonts/icomoon/style.css"
                     ));

            bundles.Add(new StyleBundle("~/contenting/css").Include(
                     "~/Content/bootstrap.min.css",
                     "~/Content/bootstrap-datepicker.css",
                     "~/Content/jquery.fancybox.min.css",
                     "~/Content/owl.carousel.min.css",
                     "~/Content/owl.theme.default.min.css"
                     ));

            bundles.Add(new StyleBundle("~/fontsing2/css").Include(
                     "~/fonts/flaticon/font/flaticon.css"
                     ));

            bundles.Add(new StyleBundle("~/contenting2/css").Include(
                    "~/Content/aos.css",
                     "~/Content/style.css"
                     ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/jquery-3.3.1.min.js",
                      "~/Scripts/popper.min.js",
                      "~/Scripts/bootstrap.min.js",
                      "~/Scripts/owl.carousel.min.js",
                      "~/Scripts/jquery.sticky.js",
                      "~/Scripts/jquery.waypoints.min.js",
                      "~/Scripts/jquery.animateNumber.min.js",
                      "~/Scripts/jquery.fancybox.min.js",
                      "~/Scripts/jquery.easing.1.3.js",
                      "~/Scripts/bootstrap-datepicker.min.js",
                      "~/Scripts/aos.js",
                      "~/Scripts/main.js"
                      ));

            //admin
            //css
            bundles.Add(new StyleBundle("~/bootstraping1/css").Include(
                   "~/bootstrap/css/bootstrap.min.css"
                    ));

            bundles.Add(new StyleBundle("~/datatablesing/css").Include(
                   "~/plugins/datatables/dataTables.bootstrap.css"
                    ));

            bundles.Add(new StyleBundle("~/calendering/css").Include(
                   "~/plugins/fullcalendar/fullcalendar.min.css"
                    ));
            bundles.Add(new StyleBundle("~/calendering2/css").Include(
                   "~/plugins/fullcalendar/fullcalendar.print.css"
                    ));

            bundles.Add(new StyleBundle("~/bootstraping/css").Include(
                    "~/dist/css/AdminLTE.min.css",
                    "~/dist/css/skins/_all-skins.min.css"
                     ));

            //js
            bundles.Add(new ScriptBundle("~/js1ing/js").Include(
                    "~/plugins/jQuery/jQuery-2.1.4.min.js",
                    "~/bootstrap/js/bootstrap.min.js"
                    ));

            bundles.Add(new ScriptBundle("~/datatablesing/js").Include(
                    "~/plugins/datatables/jquery.dataTables.min.js",
                    "~/plugins/datatables/dataTables.bootstrap.min.js"
                    ));

            bundles.Add(new ScriptBundle("~/js2ing/js").Include(
                    "~/plugins/slimScroll/jquery.slimscroll.min.js",
                    "~/plugins/fastclick/fastclick.min.js",
                    "~/dist/js/app.min.js",
                    "~/dist/js/demo.js"
                    ));

            bundles.Add(new ScriptBundle("~/fullcalendar/js").Include(
                    "~/plugins/fullcalendar/fullcalendar.min.js"
                    ));

            /*

            bundles.Add(new ScriptBundle("~/js2ing/js").Include(
                    "~/bootstrap/js/bootstrap.min.js"
                    ));

            bundles.Add(new ScriptBundle("~/js3ing/js").Include(
                    "~/plugins/morris/morris.min.js",
                    "~/plugins/sparkline/jquery.sparkline.min.js",
                    "~/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js",
                    "~/plugins/jvectormap/jquery-jvectormap-world-mill-en.js",
                    "~/plugins/knob/jquery.knob.js"
                    ));

            bundles.Add(new ScriptBundle("~/js4ing/js").Include(
                    "~/plugins/daterangepicker/daterangepicker.js",
                    "~/plugins/datepicker/bootstrap-datepicker.js",
                    "~/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js",
                    "~/plugins/slimScroll/jquery.slimscroll.min.js",
                    "~/plugins/fastclick/fastclick.min.js",
                    "~/dist/js/app.min.js",
                    "~/dist/js/pages/dashboard.js",
                    "~/dist/js/demo.js"
                    ));*/
        }
    }
}
