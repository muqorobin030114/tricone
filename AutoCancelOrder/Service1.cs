﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace AutoCancelOrder
{
    [RunInstaller(true)]
    public partial class Service1 : ServiceBase
    {
        System.Timers.Timer tmrExecutor = new System.Timers.Timer();
        int ScheduleTime = Convert.ToInt32(ConfigurationSettings.AppSettings["ThreadTime"]);
        public Thread Worker = null;
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                tmrExecutor.Elapsed += new ElapsedEventHandler(Working); //adding Event
                tmrExecutor.Interval = 10000; //set your time here 1 sec = 1000
                tmrExecutor.Enabled = true;
                tmrExecutor.Start();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public void Working(object sender, System.Timers.ElapsedEventArgs e)
        {
            DB_TRICONEEntities dbnya = new DB_TRICONEEntities();
            using (var transactions = dbnya.Database.BeginTransaction())
            {
                try
                {
                    string path = "C:\\peringatanBayar.txt";
                    var cekpendingbayar = dbnya.v_transaksi.Where(a => a.STATUS_TRANSAKSI == 1 && a.APPROVAL == 1).ToList();
                    foreach (var item in cekpendingbayar)
                    {
                        //total durasi
                        DateTime dt1 = DateTime.Parse(Convert.ToDateTime(item.CREATED_DATE).AddDays(35).ToString());
                        DateTime dt2 = DateTime.Parse(Convert.ToDateTime(DateTime.Now).AddDays(35).ToString());
                        TimeSpan ts = dt2.Subtract(dt1);
                        int jumlah_durasi = ts.Days;
                        int hasil = 2 - jumlah_durasi;
                        var query = dbnya.TBL_MEMBER.Where(a => a.ID_MEMBER == item.ID_MEMBER).FirstOrDefault();
                        if (jumlah_durasi > 2)
                        {
                            var pembayaran = dbnya.TBL_TRANSAKSI.Where(a => a.ID_TRANSAKSI == item.ID_TRANSAKSI).FirstOrDefault();
                            pembayaran.APPROVAL = 8;
                            pembayaran.STATUS = 0;
                            pembayaran.MODIFIED_DATE = DateTime.Now;

                            dbnya.Entry(pembayaran).State = EntityState.Modified;
                            dbnya.Entry(pembayaran).Property("APPROVAL").IsModified = true;
                            dbnya.Entry(pembayaran).Property("STATUS").IsModified = true;
                            dbnya.Entry(pembayaran).Property("MODIFIED_DATE").IsModified = true;
                            dbnya.SaveChanges();

                            var detailnya = dbnya.TR_DETAIL_PEMESANAN.Where(a => a.ID_TRANSAKSI == item.ID_TRANSAKSI).ToList();
                            foreach (var det in detailnya)
                            {
                                var detil = dbnya.TR_DETAIL_PEMESANAN.Where(x => x.ID_PEMESANAN == det.ID_PEMESANAN).FirstOrDefault();
                                detil.STATUS = 0;
                                detil.MODIFIED_DATE = DateTime.Now;

                                dbnya.Entry(detil).State = EntityState.Modified;
                                dbnya.Entry(detil).Property("STATUS").IsModified = true;
                                dbnya.Entry(detil).Property("MODIFIED_DATE").IsModified = true;
                                dbnya.SaveChanges();
                            }
                        }
                        else
                        {
                            //send email
                            var fromEmail = new MailAddress("traveltricone@gmail.com", "Reminder To Payment For Tricone Travel");
                            var toEmail = new MailAddress(query.EMAIL);
                            var fromEmailPassword = "tangerang"; //replace with actual password

                            string subject = "";
                            string body = "";
                            subject = "Reminder To Payment For Tricone Travel";
                            var tanggalnya = string.Format("{0:MMM-dd-yyyy}", item.TGL_PERGI);
                            body = "<br/><br/>We want to remind you that in the next " + hasil + " days, if you haven't paid the bill from us yet, we have a heavy heart to cancel your order.";
                            var smtp = new SmtpClient
                            {
                                Host = "smtp.gmail.com",
                                Port = 587,
                                EnableSsl = true,
                                DeliveryMethod = SmtpDeliveryMethod.Network,
                                UseDefaultCredentials = false,
                                Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
                            };

                            using (var message = new MailMessage(fromEmail, toEmail)
                            {
                                Subject = subject,
                                Body = body,
                                IsBodyHtml = true
                            })
                                smtp.Send(message);
                        }

                        transactions.Commit();
                        using (StreamWriter writer = new StreamWriter(path, true))
                        {
                            writer.WriteLine(string.Format(jumlah_durasi.ToString()));
                            writer.Close();
                        }
                        Thread.Sleep(ScheduleTime * 60 * 1000);
                    }
                }
                catch (Exception msgErr)
                {
                    //error log
                    DB_TRICONEEntities dc2 = new DB_TRICONEEntities();
                    TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                    logerror.EROR_TIME = DateTime.Now;
                    logerror.EROR_DESCRIPTION = msgErr.Message;
                    logerror.CREATED_DATE = DateTime.Now;
                    logerror.MODIFIED_DATE = DateTime.Now;
                    logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                    dc2.TBL_EROR_LOG.Add(logerror);
                    dc2.SaveChanges();
                    transactions.Rollback();
                }
            }
        }

        protected override void OnStop()
        {
            try
            {
                tmrExecutor.Enabled = false;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
