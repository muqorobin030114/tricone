﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AutoCancelOrder
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class DB_TRICONEEntities : DbContext
    {
        public DB_TRICONEEntities()
            : base("name=DB_TRICONEEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<detail_supir> detail_supir { get; set; }
        public virtual DbSet<MS_ADMIN> MS_ADMIN { get; set; }
        public virtual DbSet<MS_BANK> MS_BANK { get; set; }
        public virtual DbSet<MS_HARILIBUR> MS_HARILIBUR { get; set; }
        public virtual DbSet<MS_JENIS_KENDARAAN> MS_JENIS_KENDARAAN { get; set; }
        public virtual DbSet<MS_KENDARAAN> MS_KENDARAAN { get; set; }
        public virtual DbSet<MS_KOTA> MS_KOTA { get; set; }
        public virtual DbSet<MS_KUISIONER> MS_KUISIONER { get; set; }
        public virtual DbSet<MS_PROVINSI> MS_PROVINSI { get; set; }
        public virtual DbSet<MS_SUPIR> MS_SUPIR { get; set; }
        public virtual DbSet<notifikasi> notifikasi { get; set; }
        public virtual DbSet<TBL_AUDIT_TRAIL> TBL_AUDIT_TRAIL { get; set; }
        public virtual DbSet<TBL_DETAILSERVIS> TBL_DETAILSERVIS { get; set; }
        public virtual DbSet<TBL_EROR_LOG> TBL_EROR_LOG { get; set; }
        public virtual DbSet<TBL_FAQ> TBL_FAQ { get; set; }
        public virtual DbSet<TBL_FEEDBACK> TBL_FEEDBACK { get; set; }
        public virtual DbSet<TBL_HARGA> TBL_HARGA { get; set; }
        public virtual DbSet<TBL_MEMBER> TBL_MEMBER { get; set; }
        public virtual DbSet<TBL_PEMBAYARAN> TBL_PEMBAYARAN { get; set; }
        public virtual DbSet<TBL_PROMO> TBL_PROMO { get; set; }
        public virtual DbSet<TBL_REFUND> TBL_REFUND { get; set; }
        public virtual DbSet<TBL_TESTIMONI> TBL_TESTIMONI { get; set; }
        public virtual DbSet<TBL_TRANSAKSI> TBL_TRANSAKSI { get; set; }
        public virtual DbSet<TR_DETAIL_PEMESANAN> TR_DETAIL_PEMESANAN { get; set; }
        public virtual DbSet<v_detail_pesan> v_detail_pesan { get; set; }
        public virtual DbSet<v_kendaraan> v_kendaraan { get; set; }
        public virtual DbSet<v_kota> v_kota { get; set; }
        public virtual DbSet<v_Notreschedule> v_Notreschedule { get; set; }
        public virtual DbSet<v_refund> v_refund { get; set; }
        public virtual DbSet<v_reschedule> v_reschedule { get; set; }
        public virtual DbSet<v_transaksi> v_transaksi { get; set; }
    
        public virtual ObjectResult<daftarkendaraanbytanggal_Result> daftarkendaraanbytanggal(Nullable<System.DateTime> startdate, Nullable<System.DateTime> enddate, Nullable<int> id_kota)
        {
            var startdateParameter = startdate.HasValue ?
                new ObjectParameter("startdate", startdate) :
                new ObjectParameter("startdate", typeof(System.DateTime));
    
            var enddateParameter = enddate.HasValue ?
                new ObjectParameter("enddate", enddate) :
                new ObjectParameter("enddate", typeof(System.DateTime));
    
            var id_kotaParameter = id_kota.HasValue ?
                new ObjectParameter("id_kota", id_kota) :
                new ObjectParameter("id_kota", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<daftarkendaraanbytanggal_Result>("daftarkendaraanbytanggal", startdateParameter, enddateParameter, id_kotaParameter);
        }
    }
}
