//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace AutoCancelOrder
{
    using System;
    using System.Collections.Generic;
    
    public partial class TBL_REFUND
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TBL_REFUND()
        {
            this.TBL_TRANSAKSI = new HashSet<TBL_TRANSAKSI>();
        }
    
        public int ID_REFUND { get; set; }
        public Nullable<int> ID_BANK { get; set; }
        public string NOMOR_REKENING { get; set; }
        public Nullable<int> TOTAL_REFUND { get; set; }
        public Nullable<int> APPROVAL { get; set; }
        public string KETERANGANUSER { get; set; }
        public Nullable<int> STATUS { get; set; }
        public string KETERANGANADMIN { get; set; }
        public Nullable<System.DateTime> CREATED_DATE { get; set; }
        public Nullable<System.DateTime> MODIFIED_DATE { get; set; }
        public string NAMA_REKENING { get; set; }
        public Nullable<int> ID_TRANSAKSI { get; set; }
        public string FOTO_REFUND { get; set; }
    
        public virtual MS_BANK MS_BANK { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TBL_TRANSAKSI> TBL_TRANSAKSI { get; set; }
        public virtual TBL_TRANSAKSI TBL_TRANSAKSI1 { get; set; }
    }
}
