//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ReminderEmailService
{
    using System;
    using System.Collections.Generic;
    
    public partial class TBL_TESTIMONI
    {
        public int ID_TESTIMONI { get; set; }
        public string NAMA { get; set; }
        public string EMAIL_FROM { get; set; }
        public string EMAIL_TO { get; set; }
        public string SUBJECT { get; set; }
        public string MESSAGE { get; set; }
        public Nullable<int> STATUS { get; set; }
        public Nullable<System.DateTime> CREATED_DATE { get; set; }
        public Nullable<System.DateTime> MODIFIED_DATE { get; set; }
    }
}
