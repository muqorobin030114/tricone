﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace ReminderEmailService
{
    [RunInstaller(true)]
    public partial class Service1 : ServiceBase
    {
        System.Timers.Timer tmrExecutor = new System.Timers.Timer();
        int ScheduleTime = Convert.ToInt32(ConfigurationSettings.AppSettings["ThreadTime"]);
        public Thread Worker = null;
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                /*ThreadStart start = new ThreadStart(Working);
                Worker = new Thread(start);
                Worker.Start();*/
                tmrExecutor.Elapsed += new ElapsedEventHandler(Working); //adding Event
                tmrExecutor.Interval = 10000; //set your time here 1 sec = 1000
                tmrExecutor.Enabled = true;
                tmrExecutor.Start();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public void Working(object sender, System.Timers.ElapsedEventArgs e)
        {
            try
            {
                DB_TRICONEEntities dbnya = new DB_TRICONEEntities();
                var cektransaksi = dbnya.v_transaksi.Where(a => a.STATUS_TRANSAKSI == 1 && a.APPROVAL == 4).ToList();

                string path = "C:\\agungdb.txt";
                foreach (var item in cektransaksi)
                {
                    //total durasi
                    DateTime dt1 = DateTime.Parse(Convert.ToDateTime(item.TGL_PERGI).AddDays(35).ToString());
                    DateTime dt2 = DateTime.Parse(Convert.ToDateTime(DateTime.Now).AddDays(35).ToString());
                    TimeSpan ts = dt1.Subtract(dt2);
                    int jumlah_durasi = ts.Days + 1;
                    if (jumlah_durasi == 7 || jumlah_durasi == 3 || jumlah_durasi == 2 || jumlah_durasi == 1)
                    {
                        var query = dbnya.TBL_MEMBER.Where(a => a.ID_MEMBER == item.ID_MEMBER).FirstOrDefault();
                        using (StreamWriter writer = new StreamWriter(path, true))
                        {
                            writer.WriteLine(string.Format(jumlah_durasi + " " + query.EMAIL + " " + query.NAMA_MEMBER + DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt") + ""));
                            writer.Close();
                        }

                        //send email
                        var fromEmail = new MailAddress("traveltricone@gmail.com", "Reminder Tricone Membership");
                        var toEmail = new MailAddress(query.EMAIL);
                        var fromEmailPassword = "tangerang"; //replace with actual password

                        string subject = "";
                        string body = "";
                        subject = "Reminder Tricone Membership";
                        var tanggalnya = string.Format("{0:MMM-dd-yyyy}", item.TGL_PERGI);
                        body = "<br/><br/>We want to remind you that in " + jumlah_durasi + " days, on " + tanggalnya + " your trip to " + item.NAMA_KOTA + " will be implemented. please prepare your needs.";
                        var smtp = new SmtpClient
                        {
                            Host = "smtp.gmail.com",
                            Port = 587,
                            EnableSsl = true,
                            DeliveryMethod = SmtpDeliveryMethod.Network,
                            UseDefaultCredentials = false,
                            Credentials = new NetworkCredential(fromEmail.Address, fromEmailPassword)
                        };

                        using (var message = new MailMessage(fromEmail, toEmail)
                        {
                            Subject = subject,
                            Body = body,
                            IsBodyHtml = true
                        })
                            smtp.Send(message);
                        Thread.Sleep(ScheduleTime * 60 * 1000);
                    }
                }
            }
            catch (Exception msgErr)
            {
                //error log
                DB_TRICONEEntities dc2 = new DB_TRICONEEntities();
                TBL_EROR_LOG logerror = new TBL_EROR_LOG();
                logerror.EROR_TIME = DateTime.Now;
                logerror.EROR_DESCRIPTION = msgErr.Message;
                logerror.CREATED_DATE = DateTime.Now;
                logerror.MODIFIED_DATE = DateTime.Now;
                logerror.EROR_LINE = Convert.ToString(string.Format("StackTrace: {0}", msgErr.StackTrace));
                dc2.TBL_EROR_LOG.Add(logerror);
                dc2.SaveChanges();
            }
        }

        protected override void OnStop()
        {
            try
            {
                tmrExecutor.Enabled = false;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
